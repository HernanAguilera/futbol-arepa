@extends('layouts.content')

@section('content')

<h1>All %ELEMENTO_PLURAL%</h1>

<p>{{ link_to_route('%ELEMENTO_PLURAL%.create', 'Add new %ELEMENTO_PLURAL%') }}</p>

@if ($%ELEMENTO_PLURAL%->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>id</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($%ELEMENTO_PLURAL% as $%ELEMENTO_SINGULAR%)
				<tr>
					<td>$%ELEMENTO_SINGULAR%->id</td>
                    <td>{{ link_to_route('%ELEMENTO_PLURAL%.edit', 'Editar', array($%ELEMENTO_SINGULAR%->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('%ELEMENTO_PLURAL%.destroy', $%ELEMENTO_SINGULAR%->id))) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no %ELEMENTO_PLURAL%
@endif

@stop