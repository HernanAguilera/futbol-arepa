@extends('public.base')

@section('contenido')

	{{ Form::open(array('action' => 'RemindersController@postRemind')) }}
		{{ Form::label('email', 'Email') }}
		{{ Form::email('email', '', array('class' => 'form-control' )) }}
		{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}

@stop