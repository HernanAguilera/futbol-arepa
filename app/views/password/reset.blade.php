@extends('public.base')

@section('contenido')
	{{ Form::open(array('action' => 'RemindersController@postReset')) }}
		{{ Form::hidden('token', $token) }}
	    {{ Form::label('email', 'Email') }}
	    {{ Form::email('email', '', array('class' => 'form-control')) }}
	    {{ Form::label('password', 'Contraseña') }}
	    {{ Form::password('password', array('class' => 'form-control')) }}
	    {{ Form::label('password_confirmation', 'Ingresa nuevamenente la contraseña') }}
	    {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
	    {{ Form::submit('Reset Password', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop