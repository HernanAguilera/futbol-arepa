@extends('layouts.admin')

@section('main')

<h1>Show Futbolistas_amonestado</h1>

<p>{{ link_to_route('futbolistas-amonestados.index', 'Return to all futbolistas_amonestados') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Futbolista</th>
			<th>Club</th>
			<th>Partido</th>
			<th>Tipo</th>
			<th>Minuto</th>
			<th>Tiempo</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $amonestado->futbolista()->first()->nombre }}}</td>
			<td>{{{ $amonestado->club()->first()->nombre }}}</td>
			<td>{{{ $amonestado->partido_id }}}</td>
			<td>{{{ $amonestado->tipo }}}</td>
			<td>{{{ $amonestado->minuto }}}</td>
			<td>{{{ $amonestado->tiempo }}}</td>
            <td>{{ link_to_route('futbolistas-amonestados.edit', 'Edit', array($amonestado->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('futbolistas-amonestados.destroy', $amonestado->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
