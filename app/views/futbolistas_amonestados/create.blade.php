@extends('layouts.admin')

@section('main')

<h1>Nuevo amonestado</h1>

{{ Form::open(array('route' => 'futbolistas-amonestados.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista:') }}</td>
            <td>{{ Form::input('number', 'futbolista_id', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('club_id', 'Club:') }}</td>
            <td>{{ Form::input('number', 'club_id', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('partido_id', 'Partido:') }}</td>
            <td>{{ Form::input('number', 'partido_id', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tipo', 'Tipo:') }}</td>
            <td>{{ Form::text('tipo', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('minuto', 'Minuto:') }}</td>
            <td>{{ Form::input('number', 'minuto', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tiempo', 'Tiempo:') }}</td>
            <td>{{ Form::text('tiempo', '', array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


