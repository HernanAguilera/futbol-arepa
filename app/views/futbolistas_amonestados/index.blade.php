@extends('layouts.admin')

@section('main')

<h1>Futbolistas Amonestados</h1>

<p>{{ link_to_route('futbolistas-amonestados.create', 'Add new amonestado') }}</p>

@if ($amonestados->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Futbolista_id</th>
				<th>Partido_id</th>
				<th>Tipo</th>
				<th>Minuto</th>
				<th>Tiempo</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($amonestados as $amonestado)
				<tr>
					<td>{{{ $amonestado->futbolista_id }}}</td>
					<td>{{{ $amonestado->partido_id }}}</td>
					<td>{{{ $amonestado->tipo }}}</td>
					<td>{{{ $amonestado->minuto }}}</td>
					<td>{{{ $amonestado->tiempo }}}</td>
                    <td>{{ link_to_route('futbolistas-amonestados.edit', 'Edit', array($amonestado->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('futbolistas-amonestados.destroy', $amonestado->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no amonestados
@endif

@stop
