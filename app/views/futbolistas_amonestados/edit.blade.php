@extends('layouts.admin')

@section('main')

<h1>Edit Futbolistas_amonestado</h1>
{{ Form::model($amonestado, array('method' => 'PATCH', 'route' => array('futbolistas-amonestados.update', $amonestado->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista_id:') }}</td>
            <td>{{ Form::input('number', 'futbolista_id', $amonestado->futbolista_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('club_id', 'Club:') }}</td>
            <td>{{ Form::input('number', 'club_id', $amonestado->club_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('partido_id', 'Partido_id:') }}</td>
            <td>{{ Form::input('number', 'partido_id', $amonestado->partido_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tipo', 'Tipo:') }}</td>
            <td>{{ Form::text('tipo', $amonestado->tipo, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('minuto', 'Minuto:') }}</td>
            <td>{{ Form::input('number', 'minuto', $amonestado->minuto, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tiempo', 'Tiempo:') }}</td>
            <td>{{ Form::text('tiempo', $amonestado->tiempo, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('futbolistas-amonestados.show', 'Cancel', $amonestado->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
