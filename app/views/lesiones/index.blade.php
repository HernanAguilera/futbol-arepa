@extends('layouts.admin')

@section('main')

<h1>All Lesions</h1>

<p>{{ link_to_route('lesiones.create', 'Add new lesion') }}</p>

@if ($lesiones->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Recuperacion</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($lesiones as $lesion)
				<tr>
					<td>{{{ $lesion->nombre }}}</td>
					<td>{{{ $lesion->descripcion }}}</td>
					<td>{{{ $lesion->recuperacion }}}</td>
                    <td>{{ link_to_route('lesiones.edit', 'Edit', array($lesion->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('lesiones.destroy', $lesion->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no lesiones
@endif

@stop
