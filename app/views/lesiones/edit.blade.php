@extends('layouts.admin')

@section('main')

<h1>Edit Lesion</h1>
{{ Form::model($lesion, array('method' => 'PATCH', 'route' => array('lesiones.update', $lesion->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $lesion->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::textarea('descripcion', $lesion->descripcion, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('recuperacion', 'Recuperacion:') }}</td>
            <td>{{ Form::input('number', 'recuperacion', $lesion->recuperacion, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('lesiones.show', 'Cancel', $lesion->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
