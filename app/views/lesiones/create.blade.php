@extends('layouts.admin')

@section('main')

<h1>Create Lesion</h1>

{{ Form::open(array('route' => 'lesiones.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::textarea('descripcion', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('recuperacion', 'Recuperacion:') }}</td>
            <td>{{ Form::input('number', 'recuperacion', '', array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


