@extends('layouts.admin')

@section('main')

<h1>All Arbitros</h1>

<p>{{ link_to_route('arbitros.create', 'Add new arbitro') }}</p>

@if ($arbitros->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Avatar</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Estado</th>
				<th>Fecha_nacimiento</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($arbitros as $arbitro)
				<tr>
					<td>
						@if(empty($arbitro->avatar))
							<img height="50" src="/imgs/avatar-desconocido.png">
						@else 
							<img height="50" src="{{{ $arbitro->avatar }}}">
						@endif
					</td>
					<td>{{{ $arbitro->nombre }}}</td>
					<td>{{{ $arbitro->apellido }}}</td>
					<td>{{{ $colegios[$arbitro->colegio_id] }}}</td>
					<td>{{{ $arbitro->fecha_nacimiento }}}</td>
                    <td>{{ link_to_route('arbitros.edit', 'Edit', array($arbitro->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('arbitros.destroy', $arbitro->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no arbitros
@endif

@stop
