@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop

@section('main')

<h1>Edit Arbitro</h1>
{{ Form::model($arbitro, array('method' => 'PATCH', 'files' => true, 'route' => array('arbitros.update', $arbitro->id))) }}
    <table>
        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $arbitro->nombre, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('apellido', 'Apellido:') }}</td>
            <td>{{ Form::text('apellido', $arbitro->apellido, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('fecha_nacimiento', 'Fecha_nacimiento:') }}</td>
            <td>{{ Form::text('fecha_nacimiento', $arbitro->fecha_nacimiento, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('colegio_id', 'Colegio:') }}</td>
            <td>{{ Form::select('colegio_id', $colegios, $arbitro->colegio_id, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('avatar', 'Avatar:') }}</td>
            <td>{{ Form::file('avatar') }}</td>
        </tr>
        <tr>
            <td colspan="2">{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('arbitros.show', 'Cancel', $arbitro->id, array('class' => 'btn')) }}</td>
        </tr>
    </table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
