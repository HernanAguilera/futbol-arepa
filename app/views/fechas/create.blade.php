@extends('layouts.admin')

@section('main')

<h1>Create Fecha</h1>

{{ Form::open(array('route' => 'fechas.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('jornada', 'Jornada:') }}</td>
            <td>{{ Form::text('jornada', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('torneo_id', 'Torneo:') }}</td>
            <td>{{ Form::select('torneo_id', $torneos, NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('grupos', 'Grupos:') }}</td>
            <td>{{ Form::checkbox('grupos', '1', FALSE, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('visible', 'Visible:') }}</td>
            <td>{{ Form::checkbox('visible', '1', TRUE, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('activa', 'Activa:') }}</td>
            <td>{{ Form::checkbox('activa', '1', TRUE, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('actual', 'Actual:') }}</td>
            <td>{{ Form::checkbox('actual', '1', FALSE, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('proxima', 'Proxima:') }}</td>
            <td>{{ Form::checkbox('proxima', '1', FALSE, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


