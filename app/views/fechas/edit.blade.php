@extends('layouts.admin')

@section('main')

<h1>Edit Fecha</h1>
{{ Form::model($fecha, array('method' => 'PATCH', 'route' => array('fechas.update', $fecha->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('jornada', 'Jornada:') }}</td>
            <td>{{ Form::text('jornada', $fecha->jornada, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $fecha->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('torneo_id', 'Torneo:') }}</td>
            <td>{{ Form::select('torneo_id', $torneos, $fecha->torneo_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('grupos', 'Grupos:') }}</td>
            <td>{{ Form::checkbox('grupos', '1', $grupos, array('class' => 'form-control')) }}</td>
        </tr>        

        <tr>
            <td>{{ Form::label('visible', 'Visible:') }}</td>
            <td>{{ Form::checkbox('visible', '1', $fecha->visible, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('activa', 'Activa:') }}</td>
            <td>{{ Form::checkbox('activa', '1', $fecha->visible, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('actual', 'Actual:') }}</td>
            <td>{{ Form::checkbox('actual', '1', $fecha->actual, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('proxima', 'Proxima:') }}</td>
            <td>{{ Form::checkbox('proxima', '1', $fecha->proxima, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('fechas.show', 'Cancel', $fecha->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
