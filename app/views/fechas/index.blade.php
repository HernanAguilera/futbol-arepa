@extends('layouts.admin')

@section('main')

<h1>All Fechas</h1>

<p>{{ link_to_route('fechas.create', 'Add new fecha') }}</p>

@if ($fechas->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Jornada</th>
				<th>Actual</th>
				<th>Próxima</th>
				<th>Torneo</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($fechas as $fecha)
				<tr>
					<td><a href="/fechas/{{ $fecha->id }}">{{{ $fecha->jornada }}}</a></td>
					<td>{{{ $fecha->actual }}}</td>
					<td>{{{ $fecha->proxima }}}</td>
					<td>{{{ $fecha->torneo()->first()->nombre }}}</td>
                    <td>{{ link_to_route('fechas.edit', 'Edit', array($fecha->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('fechas.destroy', $fecha->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no fechas
@endif

@stop
