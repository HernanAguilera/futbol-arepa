@extends('layouts.admin')

@section('main')

<h1>Show Fecha</h1>

<p>{{ link_to_route('fechas.index', 'Return to all fechas') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Jornada</th>
			<th>Actual</th>
			<th>Próxima</th>
			<th>Torneo</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $fecha->jornada }}}</td>
			<td>{{{ $fecha->actual }}}</td>
			<td>{{{ $fecha->proxima }}}</td>
			<td>{{{ $fecha->torneo()->first()->nombre }}}</td>
            <td>{{ link_to_route('fechas.edit', 'Edit', array($fecha->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('fechas.destroy', $fecha->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>
<ul>
	@foreach($fecha->partidos()->get() as $partido)
		<li>
			@foreach($partido->clubes()->get() as $club)
				{{ $club->nombre }}
			@endforeach
		</li>
	@endforeach
</ul>

@stop
