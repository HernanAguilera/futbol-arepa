@extends('layouts.admin')

@section('main')

<h1>Show Cargo</h1>

<p>{{ link_to_route('cargos-cuerpos-tecnicos.index', 'Return to all cargos') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
				<th>Descripcion</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $cargo->nombre }}}</td>
					<td>{{{ $cargo->descripcion }}}</td>
                    <td>{{ link_to_route('cargos-cuerpos-tecnicos.edit', 'Edit', array($cargo->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('cargos-cuerpos-tecnicos.destroy', $cargo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
