@extends('layouts.admin')

@section('main')

<h1>Edit Cargo</h1>
{{ Form::model($cargo, array('method' => 'PATCH', 'route' => array('cargos-cuerpos-tecnicos.update', $cargo->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $cargo->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::textarea('descripcion', $cargo->descripcion, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('cargos-cuerpos-tecnicos.show', 'Cancel', $cargo->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
