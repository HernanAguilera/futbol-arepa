@extends('layouts.admin')

@section('main')

<h1>Edit Rol de arbitro</h1>
{{ Form::model($rol, array('method' => 'PATCH', 'route' => array('roles-arbitros.update', $rol->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $rol->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::textarea('descripcion', $rol->descripcion, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fecha_implementado', 'Fecha_implementado:') }}</td>
            <td>{{ Form::text('fecha_implementado', $rol->fecha_implementado, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('roles-arbitros.show', 'Cancel', $rol->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
