@extends('layouts.admin')

@section('main')

<h1>All Roles de arbitros</h1>

<p>{{ link_to_route('roles-arbitros.create', 'Add new rol') }}</p>

@if ($roles->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Fecha_implementado</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($roles as $rol)
				<tr>
					<td>{{{ $rol->nombre }}}</td>
					<td>{{{ $rol->descripcion }}}</td>
					<td>{{{ $rol->fecha_implementado }}}</td>
                    <td>{{ link_to_route('roles-arbitros.edit', 'Edit', array($rol->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('roles-arbitros.destroy', $rol->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no rol
@endif

@stop
