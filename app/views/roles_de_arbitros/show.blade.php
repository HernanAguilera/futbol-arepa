@extends('layouts.admin')

@section('main')

<h1>Show Rol de arbitro</h1>

<p>{{ link_to_route('roles-arbitros.index', 'Return to all roles de arbitros') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Descripcion</th>
			<th>Fecha_implementado</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $rol->nombre }}}</td>
			<td>{{{ $rol->descripcion }}}</td>
			<td>{{{ $rol->fecha_implementado }}}</td>
            <td>{{ link_to_route('roles-arbitros.edit', 'Edit', array($rol->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('roles-arbitros.destroy', $rol->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
