@extends('layouts.admin')

@section('main')

<h1>Show Estadio</h1>

<p>{{ link_to_route('estadios.index', 'Return to all estadios') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
				<th>Capacidad</th>
				<th>Ubicacion</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $estadio->nombre }}}</td>
			<td>{{{ $estadio->capacidad }}}</td>
			<td>{{{ $estadio->ubicacion }}}</td>
            <td>{{ link_to_route('estadios.edit', 'Edit', array($estadio->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('estadios.destroy', $estadio->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

<h2>Clubes</h2>
<table class="table">
	<thead>
		<tr>
			<td>Nombre</td>
		</tr>
	</thead>
	<tbody>
		@foreach($clubes as $club)
			<tr>
				<td><a href="/clubes/{{$club->id}}">{{{$club->nombre}}}</a></td>
			</tr>
		@endforeach
	</tbody>
</table>

@stop
