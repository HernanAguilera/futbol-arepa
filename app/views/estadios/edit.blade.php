@extends('layouts.admin')

@section('main')

<h1>Edit Estadio</h1>
{{ Form::model($estadio, array('method' => 'PATCH', 'route' => array('estadios.update', $estadio->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $estadio->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('capacidad', 'Capacidad:') }}</td>
            <td>{{ Form::input('number', 'capacidad', $estadio->capacidad, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
        	<td>{{Form::label('ciudad', 'Ciudad:')}}</td>
        	<td>{{Form::select('ciudad_id', $ciudades, $estadio->ciudad_id, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('ubicacion', 'Ubicacion:') }}</td>
            <td>{{ Form::text('ubicacion', $estadio->ubicacion, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('estadios.show', 'Cancel', $estadio->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
