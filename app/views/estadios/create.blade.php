@extends('layouts.admin')

@section('main')

<h1>Create Estadio</h1>

{{ Form::open(array('route' => 'estadios.store')) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('capacidad', 'Capacidad:') }}</td>
            <td>{{ Form::input('number', 'capacidad', '', array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
        	<td>{{Form::label('ciudad', 'Ciudad:')}}</td>
        	<td>{{Form::select('ciudad_id', $ciudades, NULL, array('class' => 'form-control')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('ubicacion', 'Ubicacion:') }}</td>
            <td>{{ Form::textarea('ubicacion', '', array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


