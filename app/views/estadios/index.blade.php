@extends('layouts.admin')

@section('main')

<h1>All Estadios</h1>

<p>{{ link_to_route('estadios.create', 'Add new estadio') }}</p>

@if ($estadios->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Capacidad</th>
				<th>Ciudad</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($estadios as $estadio)
				<tr>
					<td><a href="/estadios/{{$estadio->id}}">{{{ $estadio->nombre }}}</a></td>
					<td>{{{ $estadio->capacidad }}}</td>
					<td>{{{ $ciudades[$estadio->ciudad_id] }}}</td>

                    <td>{{ link_to_route('estadios.edit', 'Edit', array($estadio->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('estadios.destroy', $estadio->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="paginate">
		{{ $estadios->links() }}
	</div>
@else
	There are no estadios
@endif

@stop
