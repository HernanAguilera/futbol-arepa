@extends('layouts.admin')

@section('main')

<h1>All grupos</h1>

<p>{{ link_to_route('grupos.create', 'Add new grupo') }}</p>

@if ($grupos->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Torneo</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($grupos as $grupo)
				<tr>
					<td>
						<a href="/grupos/{{ $grupo->id }}">{{{ $grupo->nombre }}}</a>
					</td>
					<td>{{{ $grupo->torneo()->first()->nombre }}}</td>
                    <td>{{ link_to_route('grupos.edit', 'Edit', array($grupo->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('grupos.destroy', $grupo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no grupos
@endif

@stop
