@extends('layouts.admin')

@section('main')

<h1>Edit grupo</h1>
{{ Form::model($grupo, array('method' => 'PATCH', 'route' => array('grupos.update', $grupo->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $grupo->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('torneo_id', 'Torneo_id:') }}</td>
            <td>{{ Form::select('torneo_id', $torneos, $grupo->torneo_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('clubes', 'Clubes:') }}</td>
            <td>
                <ul>
                    @foreach($clubes as $key => $club)
                        <li>
                            {{ Form::label('club-'.$key, $club) }}
                            {{ Form::checkbox('clubes[]', $key, in_array($key, $grupo->clubes), array('id' => 'club-'.$key)) }}
                        </li>
                    @endforeach
                </ul>
            </td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('grupos.show', 'Cancel', $grupo->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
