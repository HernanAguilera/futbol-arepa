@extends('layouts.admin')

@section('main')

<h1>Create Grupo</h1>

{{ Form::open(array('route' => 'grupos.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('torneo_id', 'Torneo_id:') }}</td>
            <td>{{ Form::select('torneo_id', $torneos, NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('clubes', 'Clubes:') }}</td>
            <td>
                <ul>
                    @foreach($clubes as $key => $club)
                        <li>
                            {{ Form::label('club-'.$key, $club) }}
                            {{ Form::checkbox('clubes[]', $key, NULL, array('id' => 'club-'.$key)) }}
                        </li>
                    @endforeach
                </ul>
            </td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


