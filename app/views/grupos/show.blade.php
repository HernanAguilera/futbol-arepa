@extends('layouts.admin')

@section('main')

<h1>Show Grupo</h1>

<p>{{ link_to_route('grupos.index', 'Return to all grupos') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Torneo</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $grupo->nombre }}}</td>
			<td>{{{ $grupo->torneo()->first()->nombre }}}</td>
            <td>{{ link_to_route('grupos.edit', 'Edit', array($grupo->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('grupos.destroy', $grupo->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>
<ol>
	@foreach($grupo->clubes()->orderBy('nombre')->get() as $club)
		<li>{{{$club->nombre}}}</li>
	@endforeach
</ol>

@stop
