@extends('layouts.admin')

@section('main')

<h1>Edit Posicion</h1>
{{ Form::model($posicion, array('method' => 'PATCH', 'route' => array('posiciones.update', $posicion->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::text('descripcion', $posicion->descripcion, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $posicion->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('posiciones.show', 'Cancel', $posicion->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
