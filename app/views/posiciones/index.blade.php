@extends('layouts.admin')

@section('main')

<h1>All Posiciones</h1>

<p>{{ link_to_route('posiciones.create', 'Add new posicion') }}</p>

@if ($posiciones->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Descripcion</th>
				<th>Abreviatura</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($posiciones as $posicion)
				<tr>
					<td>{{{ $posicion->descripcion }}}</td>
					<td>{{{ $posicion->abreviatura }}}</td>
                    <td>{{ link_to_route('posiciones.edit', 'Edit', array($posicion->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('posiciones.destroy', $posicion->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no posicions
@endif

@stop
