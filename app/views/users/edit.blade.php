@extends('layouts.admin')

@section('main')

<h1>Editar Usuario {{ $user->id }}</h1>
	
	{{ Form::model($user, array('method' => 'PATCH', 'route' => array('admin.users.update', $user->id))) }}
	<table>
		<tr>
			<td>{{ Form::label('username', 'Nombre:') }}</td>
			<td>{{ Form::text('username', $user->username, array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('email', 'Descripcion:') }}</td>
			<td>{{ Form::text('email', $user->email, array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('rol', 'Rol:') }}</td>
			<td>{{ Form::select('rol', $rols, $user->rol_id, array('class' => 'form-control'))}}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password', 'Ingresa tu contraseña:') }}</td>
			<td>{{ Form::password('password', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password-repeat', 'Repite tu contraseña:') }}</td>
			<td>{{ Form::password('password-repeat', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td colspan="2">
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('admin.users.show', 'Cancel', $user->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
	{{ Form::close() }}

	@if ($errors->any())
		<ul>
			{{ implode('', $errors->all('<li class="error">:message</li>')) }}
		</ul>
	@endif

@stop