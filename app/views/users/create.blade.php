@extends('layouts.admin')

@section('main')

<h1>Nuevo Usuario</h1>

	{{ Form::open(array('route' => 'admin.users.store')) }}
	<table>
		<tr>
			<td>{{ Form::label('username', 'Nombre de usuario:') }}</td>
			<td>{{ Form::text('username','', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('email', 'Correo Electrónico:') }}</td>
			<td>{{ Form::text('email','', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('rol_id', 'Rol:') }}</td>
			<td>{{ Form::select('rol_id', $rols, '', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password', 'Ingresa tu contraseña:') }}</td>
			<td>{{ Form::password('password', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td>{{ Form::label('password-repeat', 'Repite tu contraseña:') }}</td>
			<td>{{ Form::password('password-repeat', array('class' => 'form-control')) }}</td>
		</tr>
		<tr>
			<td colspan="2">{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}</td>
		</tr>
	</table>
	{{ Form::close() }}	

	@if ($errors->any())
		<ul>
			{{ implode('', $errors->all('<li class="error">:message</li>')) }}
		</ul>
	@endif

@stop