@extends('layouts.admin')

@section('main')
<h1>Usuarios</h1>

<p>{{ link_to_route('admin.users.create', 'Agregar usuario') }}</p>

@if($users->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Username</th>
				<th>Email</th>
				<td>Rol</td>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<td>
						<a href="/admin/users/{{ $user->id }}">{{ $user->username }}</a>
					</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->rol()->first()->title }}</td>
					<td>{{ link_to_route('admin.users.edit', 'Edit', array($user->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.users.destroy', $user->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<p>No se ha definido ningún usuario aún</p>
@endif
@stop