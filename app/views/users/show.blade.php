@extends('layouts.admin')

@section('main')

	<h1>{{ $user->username }}</h1>

	<p>{{ link_to_route('admin.users.index', 'Return to all usuarios') }}</p>

	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Username</th>
				<th>Email</th>
				<td>Rol</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					{{ $user->username }}
				</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->rol()->first()->title }}</td>
				<td>{{ link_to_route('admin.users.edit', 'Edit', array($user->id), array('class' => 'btn btn-info')) }}</td>
                <td>
                    {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.users.destroy', $user->id))) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                </td>
			</tr>
		</tbody>
	</table>

@stop