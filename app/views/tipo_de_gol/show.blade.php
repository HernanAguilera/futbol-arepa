@extends('layouts.admin')

@section('main')

<h1>Show Tipo de gol</h1>

<p>{{ link_to_route('tipo-de-gol.index', 'Return to all tipo de goles') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Descripcion</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $tipo->descripcion }}}</td>
                    <td>{{ link_to_route('tipo-de-gol.edit', 'Edit', array($tipo->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('tipo-de-gol.destroy', $tipo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
