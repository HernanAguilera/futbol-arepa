@extends('layouts.admin')

@section('main')

<h1>All Tipo de gol</h1>

<p>{{ link_to_route('tipo-de-gol.create', 'Add new tipo de gol') }}</p>

@if ($tipos->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Descripcion</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($tipos as $tipo)
				<tr>
					<td>{{{ $tipo->descripcion }}}</td>
                    <td>{{ link_to_route('tipo-de-gol.edit', 'Edit', array($tipo->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('tipo-de-gol.destroy', $tipo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no tipo de gol
@endif

@stop
