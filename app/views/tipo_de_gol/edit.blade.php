@extends('layouts.admin')

@section('main')

<h1>Edit Tipo de gol</h1>
{{ Form::model($tipo, array('method' => 'PATCH', 'route' => array('tipo-de-gol.update', $tipo->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::text('descripcion', $tipo->descripcion, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('tipo-de-gol.show', 'Cancel', $tipo->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
