@extends('layouts.admin')

@section('main')

<h1>Show Galeria</h1>

<p>{{ link_to_route('galerias.estadios.index', 'Return to all galerias') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Imagen</th>
			<th>Estadio</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>
				@if(!empty($galeria->imagen))
					<img height="50" src="{{{ $galeria->imagen }}}">
				@endif
			</td>
			<td>{{{ $estadios[$galeria->estadio_id] }}}</td>
            <td>{{ link_to_route('galerias.estadios.edit', 'Edit', array($galeria->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('galerias.estadios.destroy', $galeria->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
