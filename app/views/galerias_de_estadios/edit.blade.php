@extends('layouts.admin')

@section('main')

<h1>Edit Galeria</h1>
{{ Form::model($galeria, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('galerias.estadios.update', $galeria->id))) }}
	<img src="{{{ $galeria->imagen }}}">
	<table>
		<tr>
            <td>{{ Form::label('imagen', 'Imagen:') }}</td>
            <td>{{ Form::file('imagen') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('estadio_id', 'Estadio:') }}</td>
            <td>{{ Form::select('estadio_id', $estadios, $galeria->estadio, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('galerias.estadios.show', 'Cancel', $galeria->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
