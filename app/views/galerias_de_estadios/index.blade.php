@extends('layouts.admin')

@section('main')

<h1>All Galerias</h1>

<p>{{ link_to_route('galerias.estadios.create', 'Add new galeria') }}</p>

@if ($galerias->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Imagen</th>
				<th>Estadio</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($galerias as $galeria)
				<tr>
					<td>
						@if(!empty($galeria->imagen))
							<img height="50" src="{{{ $galeria->imagen }}}">
						@endif
					</td>
					<td>{{{ $estadios[$galeria->estadio_id] }}}</td>
                    <td>{{ link_to_route('galerias.estadios.edit', 'Edit', array($galeria->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('galerias.estadios.destroy', $galeria->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no galeria
@endif

@stop
