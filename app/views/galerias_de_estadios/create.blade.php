@extends('layouts.admin')

@section('main')

<h1>Create Galeria</h1>

{{ Form::open(array('route' => 'galerias.estadios.store', 'files' => TRUE)) }}
	<table>
		<tr>
            <td>{{ Form::label('imagen', 'Imagen:') }}</td>
            <td>{{ Form::file('imagen') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('estadio_id', 'Estadio_id:') }}</td>
            <td>{{ Form::select('estadio_id', $estadios, NULL, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


