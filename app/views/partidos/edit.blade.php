@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="/css/chosen/chosen.css">
<link rel="stylesheet" type="text/css" href="/css/onoff/jquery.onoff.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/lib/chosen.jquery.min.js" ></script>
<script type="text/javascript" src="/js/lib/jquery.onoff.min.js" ></script>
<script type="text/javascript" src="/js/lib/tinymce/tinymce.min.js" ></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
<script type="text/javascript" src="/js/widgets/select.widget.js"></script>
<script type="text/javascript" src="/js/widgets/check.widget.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea"
     });
</script>
@stop

@section('main')

<h1>Edit Partido</h1>
{{ Form::model($partido, array('method' => 'PATCH', 'route' => array('partidos.update', $partido->id))) }}
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-3">{{ Form::label('fecha_calendario', 'Fecha:') }}</div>
            <div class="col-md-9">{{ Form::text('fecha_calendario', $partido->fecha_calendario, array('class' => 'form-control datetimepicker')) }}</div>
            <div class="col-md-3">{{ Form::label('fecha_id', 'Jornada:') }}</div>
            <div class="col-md-9">{{ Form::select('fecha_id', $fechas, $partido->fecha_id, array('class' => 'form-control', 'id' => 'fecha')) }}</div>
            <div class="col-md-3">{{ Form::label('estadio_id', 'Estadio:') }}</div>
            <div class="col-md-9">{{ Form::select('estadio_id', $estadios, $partido->estadio_id, array('class' => 'form-control')) }}</div>
            <div class="col-md-3">{{ Form::label('local', 'Local:') }}</div>
            <div class="col-md-9">{{ Form::select('local', $clubes, isset($partido->clubes['local']) ? $partido->clubes['local'] : NULL, array('class' => 'form-control')) }}</div>
            <div class="col-md-3">{{ Form::label('visitante', 'Visitante:') }}</div>
            <div class="col-md-9">{{ Form::select('visitante', $clubes, isset($partido->clubes['visitante']) ? $partido->clubes['visitante'] : NULL, array('class' => 'form-control')) }}</div>
            <div class="col-md-3">{{ Form::label('reprogramado', 'Reprogramado:') }}</div>
            <div class="col-md-9">{{ Form::checkbox('reprogramado', '1', FALSE) }}</div>
            <div class="col-md-3">{{Form::label('estado_id', 'Jugado:')}}</div>
            <div class="col-md-9">{{ Form::select('estado_id', $estados, $partido->estado_id, array('class' => 'form-control')) }}</div>
        </div>
        <div class="col-md-6">
            @foreach($roles as $rol)
                <div class="col-md-5">{{ Form::label($rol['nombre'], ucfirst($rol['nombre']).':') }}</div>
                <div class="col-md-7">{{ Form::select('rol_'.$rol['id'], $arbitros, isset($partido->arbitros[$rol['id']]) ? $partido->arbitros[$rol['id']] : NULL, array('class' => 'form-control', 'id' => $rol['nombre'])) }}</div>
            @endforeach
            <div class="col-md-3"></div>
            <div class="col-md-9"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">{{ Form::label('nota', 'Nota:') }}</div>
        <div class="col-md-12">{{ Form::textarea('nota', $partido->nota, array('class' => 'form-control')) }}</div>
        <div class="col-md-12">{{ Form::label('video', 'Video:') }}</div>
        <div class="col-md-12">{{ Form::text('video', $partido->video, array('class' => 'form-control')) }}</div>
    </div>
    <div style="margin-top:2em" class="row">
        <div class="col-md-12">
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('partidos.show', 'Cancel', $partido->id, array('class' => 'btn')) }}
        </div>
    </div>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
