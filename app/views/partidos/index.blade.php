@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/lib/mustache.js"></script>
<script type="text/javascript" src="/js/modules/partidos/selectFilter.js"></script>
@stop

@section('main')

<h1>All Partidos</h1>

<p>{{ link_to_route('partidos.create', 'Add new partido') }}</p>

<div class="row">
	<div class="col-md-6">
		{{ Form::select('torneo', $torneos, NULL, array('class' => 'form-control', 'id' => 'torneo')) }}
	</div>
	<div class="col-md-6">
		{{ Form::select('fecha', array(), NULL, array('class' => 'form-control', 'id' => 'fecha')) }}
	</div>
</div>	

<div id="partidos">

</div>

@stop
