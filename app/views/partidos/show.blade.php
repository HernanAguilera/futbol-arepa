@extends('layouts.admin')

@section('main')

<p>{{ link_to_route('partidos.index', 'Return to all partidos') }}</p>

<div class="jumbotron">	
		<h1>{{{ $estadio->nombre }}}</h1>
		<h2>{{{ $torneo->nombre }}}</h2>
		<h3>{{{ $fecha->jornada }}} - {{{ $partido->fecha_calendario }}}</h3>
	</div>
	<div class="row clubes">
		<div class="col-md-5">
			<p class="club club-local">
				{{ mb_substr($clubes['local']->nombre, 0, 20) }}{{ strlen($clubes['local']->nombre)>20 ? '...' : '' }} 
				&nbsp; 
				<span class="gol gol-local label label-default">{{$goles['local']->cantidad}}</span>
			</p>
		</div>
		<div style="text-align:left" class="col-md-2">
			@if($partido->estado()->get()->count()>0)
				<p>{{ ucwords($partido->estado()->first()->nombre) }}</p>
			@endif
		</div>
		<div class="col-md-5">
			<p class="club club-visitante">
				<span class="gol gol-visitante label label-default">{{$goles['visitante']->cantidad}}</span>
				{{ mb_substr($clubes['visitante']->nombre,0,20) }}{{ strlen($clubes['visitante']->nombre)>20 ? '...' : '' }}
				&nbsp; 
			</p>
		</div>
	</div>
	<div class="row goles">
		<div class="col-md-6">
			<ul>
				@foreach($goles['local']->goles as $gol)
					@if(!empty($gol->futbolista_id))
						@if($gol->contrario)
							<li>{{ $futbolistas['visitante'][$gol->futbolista_id] }}
						@else
							<li>{{ $futbolistas['local'][$gol->futbolista_id] }}
						@endif
					@endif
						@foreach($gol->anotaciones as $anotacion)
							<span class="label label-primary">
								{{ $anotacion->minuto }}'
							</span>
							{{ $anotacion->tipo }}
							&nbsp;
						@endforeach
					</li>
				@endforeach
			</ul>
		</div>
		<div class="col-md-6">
			<ul>
				@foreach($goles['visitante']->goles as $gol)
					@if(!empty($gol->futbolista_id))
						@if($gol->contrario)
							<li>{{ $futbolistas['local'][$gol->futbolista_id] }}
						@else
							<li>{{ $futbolistas['visitante'][$gol->futbolista_id] }}
						@endif
					@endif
					@foreach($gol->anotaciones as $anotacion)
						<span class="label label-primary">
							{{ $anotacion->minuto }}'
						</span>
						{{ $anotacion->tipo }}
						&nbsp;
					@endforeach
					</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="row amonestados">
		<div class="col-md-6">
			<h4>Amonestados</h4>
			<ul>
				@foreach($amonestados['local'] as $amonestado)
				<li>{{ $futbolistas['local'][$amonestado->futbolista_id] }}
					@if($amonestado->tipo==='amarilla')
						<span class="label label-warning">
							{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
						</span>
					@else
						<span class="label label-danger">
							{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
						</span>
					@endif
				</li>
				@endforeach
			</ul>
		</div>
		<div class="col-md-6">
			<h4>Amonestados</h4>
			<ul>
				@foreach($amonestados['visitante'] as $amonestado)
				<li>{{ $futbolistas['visitante'][$amonestado->futbolista_id] }}
					@if($amonestado->tipo==='amarilla')
						<span class="label label-warning">
							{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
						</span>
					@else
						<span class="label label-danger">
							{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
						</span>
					@endif
				</li>
				@endforeach
			</ul>
		</div>
	</div>

<a href="/en-juego/partido/{{$partido->id}}" class="btn btn-info">Jugar</a>
<a href="/partidos/{{$partido->id}}/edit" class="btn btn-info">Editar</a>
@stop
