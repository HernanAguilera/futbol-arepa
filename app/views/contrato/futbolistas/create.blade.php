@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/chosen/chosen.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/chosen.jquery.min.js" ></script>
<script type="text/javascript" src="/js/widgets/select.widget.js"></script>
@stop

@section('main')

<h1>{{ $futbolista->nombre }} {{ $futbolista->apellido }}</h1>

{{ Form::open(array('url' => '/contrato/futbolista/store')) }}

<div class="row">
	<div class="col-md-6">
		{{ Form::label('temporada', 'Temporada:') }}
		{{ Form::select('temporada', $temporadas, NULL, array('class' => 'form-control')) }}
	</div>
	<div class="col-md-6">
		{{ Form::label('torneo', 'Torneo') }}
		{{ Form::select('torneo', $torneos, NULL, array('class' => 'form-control')) }}
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		{{ Form::label('club', 'Club:') }}
		{{ Form::select('club', $clubes, NULL, array('class' => 'form-control')) }}
	</div>
	<div class="col-md-6">
		{{ Form::label('numero', 'Numero:') }}
		{{ Form::text('numero', '', array('class' => 'form-control')) }}
	</div>
</div>
<div style="margin-top:2em" class="row">
	<div class="col-md-12">
		{{ Form::hidden('futbolista', $futbolista->id) }}
		<a href="/futbolistas/{{$futbolista->id}}" class="btn btn-default">Regresar</a>
		{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}
	</div>
</div>

{{ Form::close() }}

@stop