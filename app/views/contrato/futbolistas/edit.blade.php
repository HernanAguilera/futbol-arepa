@extends('layouts.admin')

@section('main')

<h1>{{ $futbolista->nombre }} {{ $futbolista->apellido }}</h1>

{{ Form::open(array('url' => '/contrato/futbolista/'.$futbolista->id)) }}

<div class="row">
	<div class="col-md-6">
		{{ Form::label('temporada', 'Temporada:') }}
		{{ Form::select('temporada', $temporadas, $temporada_id, array('class' => 'form-control')) }}
	</div>
	<div class="col-md-6">
		{{ Form::label('torneo', 'Torneo') }}
		{{ Form::select('torneo', $torneos, $torneo_id, array('class' => 'form-control')) }}
		{{ Form::hidden('torneo_anterior', $torneo_id) }}
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		{{ Form::label('club', 'Club:') }}
		{{ Form::select('club', $clubes, $club_id, array('class' => 'form-control')) }}
	</div>
	<div class="col-md-6">
		{{ Form::label('numero', 'Numero:') }}
		{{ Form::text('numero', $numero, array('class' => 'form-control')) }}
	</div>
</div>
<div style="margin-top:2em" class="row">
	<div class="col-md-12">
		{{ Form::hidden('futbolista', $futbolista->id) }}
		<a href="/futbolistas/{{$futbolista->id}}" class="btn btn-default">Regresar</a>
		{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}
	</div>
</div>

{{ Form::close() }}

@stop