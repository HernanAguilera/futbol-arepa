@extends('layouts.admin')

@section('main')

<h1>Futbolistas</h1>

<p>{{ link_to_route('futbolistas.create', 'Add new futbolista') }}</p>

@if ($futbolistas->count())
	<div class="paginate">{{ $futbolistas->links() }}</div>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Avatar</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Lugar de nacimiento</th>
				<th>Altura</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($futbolistas as $futbolista)
				<tr>
					<td>
						<a href="/futbolistas/{{$futbolista->id}}">
							@if(is_null($futbolista->avatar))
								<img width="80" src="{{ Config::get('images.AVATAR_DEFAULT') }}">
							@else
								<img src="{{{ dirname($futbolista->avatar).'/thumbnail/'.basename($futbolista->avatar) }}}">
							@endif
						</a>
					</td>
					<td>{{{ $futbolista->nombre }}}</td>
					<td>{{{ $futbolista->apellido }}}</td>
					<td>{{{ isset($futbolista->ciudad_id) ? $ciudades[$futbolista->ciudad_id] : "Desconocido" }}}</td>
					<td>{{{ $futbolista->altura }}}</td>
                    <td>
                    	{{ link_to_route('futbolistas.edit', 'Edit', array($futbolista->id), array('class' => 'btn btn-info')) }}
                    </td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('futbolistas.destroy', $futbolista->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="paginate">{{ $futbolistas->links() }}</div>
@else
	There are no futbolista
@endif

@stop
