@extends('layouts.admin')

@section('main')

<table class="table table-striped table-bordered">
	<thead>
		<tr>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{ link_to_route('futbolistas.index', 'regresar', array(), array('class' => 'btn btn-info')) }}</td>
            <td>{{ link_to_route('futbolistas.edit', 'Edit', array($futbolista->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('futbolistas.destroy', $futbolista->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-md-6">
		<img src="{{ $futbolista->avatar }}" width="300">
		<h1>{{{ $futbolista->nombre }}} {{{ $futbolista->apellido }}}</h1>
	</div>
	<div class="col-md-6">
		<p>Altura: {{{ $futbolista->altura }}}</p>
		<p>Fecha de Nacimiento: {{{ $futbolista->fecha_nacimiento }}}</p>
		<p>Lugar de Nacimiento: {{{ $futbolista->ciudad_nacimiento }}} </p>
	</div>
</div>

<h2>Clubes</h2>
<table class="table">
	<thead>
		<tr>
			<td>Torneo</td>
			<td>Club</td>
			<td>Numero</td>
		</tr>
	</thead>
	<tbody>
		@foreach($torneos as $torneo)
			<tr>
				<td> <?php echo $torneo->nombre ?> </td>
				<td> <a href="/clubes/{{ $torneo->pivot->club_id }}">{{ $clubes[$torneo->pivot->club_id] }}</a></td>
				<td> <?php echo $torneo->pivot->numero ?> </td>
				<td> <a href="/contrato/futbolista/{{ $futbolista->id }}/torneo/{{ $torneo->id }}/edit">Modificar</a></td>
			</tr>
		@endforeach
	</tbody>
</table>
<a href="/contrato/futbolista/{{ $futbolista->id }}" class="btn btn-primary">Agregar</a>
@stop
