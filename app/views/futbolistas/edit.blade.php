@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop

@section('main')

<h1>Edit Futbolista</h1>
{{ Form::model($futbolista, array('method' => 'PATCH', 'files' => true, 'route' => array('futbolistas.update', $futbolista->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $futbolista->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('apellido', 'Apellido:') }}</td>
            <td>{{ Form::text('apellido', $futbolista->apellido, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('nacionalidad', 'Nacionalidad:') }}</td>
            <td>{{ Form::select('nacionalidad[]', $paises, $futbolista->nacionalidad, array('multiple' => 'multiple'))}}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('ciudad_id', 'Lugar Nacimiento:') }}</td>
            <td>{{ Form::select('ciudad_id', $ciudades, $futbolista->ciudad_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('posicion', 'Posicion:') }}</td>
            <td>{{ Form::select('posicion[]', $posiciones, $futbolista->posicion, array('multiple' => 'multiple'))}}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('altura', 'Altura:') }}</td>
            <td>{{ Form::input('number', 'altura', $futbolista->altura, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fecha_nacimiento', 'Fecha_nacimiento:') }}</td>
            <td>{{ Form::text('fecha_nacimiento', $futbolista->fecha_nacimiento, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('avatar', 'Avatar:') }}</td>
            <td>{{ Form::file('avatar') }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('futbolistas.show', 'Cancel', $futbolista->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
