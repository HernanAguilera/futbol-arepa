@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop

@section('main')

<h1>Create Futbolista</h1>

{{ Form::open(array('route' => 'futbolistas.store', 'files' => TRUE)) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('apellido', 'Apellido:') }}</td>
            <td>{{ Form::text('apellido', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('nacionalidad', 'Nacionalidad:') }}</td>
            <td>{{ Form::select('nacionalidad[]', $paises, NULL, array('multiple' => 'multiple'))}}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('ciudad_id', 'Lugar Nacimiento:') }}</td>
            <td>{{ Form::select('ciudad_id', $ciudades, NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('posicion', 'Posicion:') }}</td>
            <td>{{ Form::select('posicion[]', $posiciones, NULL, array('multiple' => 'multiple'))}}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('altura', 'Altura:') }}</td>
            <td>{{ Form::input('number', 'altura', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fecha_nacimiento', 'Fecha Nacimiento:') }}</td>
            <td>{{ Form::text('fecha_nacimiento', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('avatar', 'Avatar:') }}</td>
            <td>{{ Form::file('avatar') }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


