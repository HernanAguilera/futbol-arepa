@extends('layouts.scaffold')

@section('main')

<h1>Create Convocado</h1>

{{ Form::open(array('route' => 'convocados.store')) }}
	<table>
		        <tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista_id:') }}</td>
            <td>{{ Form::input('number', 'futbolista_id', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('partido_id', 'Partido_id:') }}</td>
            <td>{{ Form::input('number', 'partido_id', '', array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


