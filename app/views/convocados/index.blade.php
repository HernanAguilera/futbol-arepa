@extends('layouts.scaffold')

@section('main')

<h1>All Convocados</h1>

<p>{{ link_to_route('convocados.create', 'Add new convocado') }}</p>

@if ($convocados->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Futbolista_id</th>
				<th>Partido_id</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($convocados as $convocado)
				<tr>
					<td>{{{ $convocado->futbolista_id }}}</td>
					<td>{{{ $convocado->partido_id }}}</td>
                    <td>{{ link_to_route('convocados.edit', 'Edit', array($convocado->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('convocados.destroy', $convocado->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no convocados
@endif

@stop
