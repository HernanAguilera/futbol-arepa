@extends('layouts.scaffold')

@section('main')

<h1>Show Convocado</h1>

<p>{{ link_to_route('convocados.index', 'Return to all convocados') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Futbolista_id</th>
				<th>Partido_id</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $convocado->futbolista_id }}}</td>
					<td>{{{ $convocado->partido_id }}}</td>
                    <td>{{ link_to_route('convocados.edit', 'Edit', array($convocado->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('convocados.destroy', $convocado->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
