@extends('layouts.scaffold')

@section('main')

<h1>Edit Convocado</h1>
{{ Form::model($convocado, array('method' => 'PATCH', 'route' => array('convocados.update', $convocado->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista_id:') }}</td>
            <td>{{ Form::input('number', 'futbolista_id', $convocado->futbolista_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('partido_id', 'Partido_id:') }}</td>
            <td>{{ Form::input('number', 'partido_id', $convocado->partido_id, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('convocados.show', 'Cancel', $convocado->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
