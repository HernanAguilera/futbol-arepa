@extends('layouts.admin')

@section('main')

<h1>All Torneos</h1>

<p>{{ link_to_route('torneos.create', 'Add new torneo') }}</p>

@if ($torneos->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Tipo</th>
				<th>Inicio</th>
				<th>Fin</th>
				<th>Temporada</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($torneos as $torneo)
				<tr>
					<td><a href="/torneos/{{$torneo->id}}">{{{ $torneo->nombre }}}</a></td>
					<td>{{{ ucfirst($torneo->tipo) }}}</td>
					<td>{{{ $torneo->inicio }}}</td>
					<td>{{{ $torneo->fin }}}</td>
					<td>{{{ $temporadas[$torneo->temporada_id] }}}</td>
                    <td>{{ link_to_route('torneos.edit', 'Edit', array($torneo->id), array('class' => 'btn btn-info')) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no torneos
@endif

@stop
