@extends('layouts.admin')

@section('main')

<h1>Show Torneo</h1>

<p>{{ link_to_route('torneos.index', 'Return to all torneos') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Inicio</th>
			<th>Fin</th>
			<th>Temporada</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $torneo->nombre }}}</td>
			<td>{{{ $torneo->inicio }}}</td>
			<td>{{{ $torneo->fin }}}</td>
			<td>{{{ $torneo->temporada()->first()->nombre}}}</td>
            <td>{{ link_to_route('torneos.edit', 'Edit', array($torneo->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('torneos.destroy', $torneo->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

<ul class="nav nav-tabs" role="tablist">
  <li class="active"><a href="#tabla" role="tab" data-toggle="tab">Tabla</a></li>
  <li><a href="#fechas" role="tab" data-toggle="tab">Fechas</a></li>
  <li><a href="#clubes" role="tab" data-toggle="tab">Clubes</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane fade  in active" id="tabla">
    @foreach($tabla as $nombre => $grupo)
      <h3>{{{ $nombre }}}</h3>
      <table class="table">
        <thead>
          <tr>
            <th></th>
            <th>PJ</th>
            <th>PG</th>
            <th>PE</th>
            <th>PP</th>
            <th>GF</th>
            <th>GC</th>
            <th>Dif.</th>
            <th>Pts</th>
          </tr>
        </thead>
        <tbody>
          @foreach($grupo as $club)
            <tr>
              <td>
                <img height="30" src="{{ $club['escudo'] }}">
                {{ $club['nombre'] }}
              </td>
              <td>{{ $club['PJ'] }}</td>
              <td>{{ $club['PG'] }}</td>
              <td>{{ $club['PE'] }}</td>
              <td>{{ $club['PP'] }}</td>
              <td>{{ $club['GF'] }}</td>
              <td>{{ $club['GC'] }}</td>
              <td>{{ $club['Dif'] }}</td>
              <td>{{ $club['Pts'] }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endforeach
  </div>
  <div class="tab-pane fade" id="clubes">
  	<table class="table">
  		<thead>
  			<tr>
  				<th></th>
  				<th>Nombre</th>
  				<th>Twitter</th>
  			</tr>
  		</thead>
  		<tbody>
  			@foreach($clubes as $club)
  				<tr>
  					<td><a href="/clubes/{{$club->id}}"><img height="25" src="{{{$club->escudo}}}"></a></td>
  					<td><a href="/clubes/{{$club->id}}">{{{$club->nombre}}}</a></td>
  					<td><a target="__blank" href="http://twitter.com/{{{$club->twitter}}}">@<?php echo $club->twitter ?></a></td>
  				</tr>
  			@endforeach
  		</tbody>
  	</table>
  </div>
  <div class="tab-pane fade" id="fechas">
  	<div class="panel-group" id="accordion">
  		@foreach($fechas as $fecha)
  			<div class="panel panel-default">
  				<div class="panel-heading">
  					<h4 class="panel-title">
  						<a data-toggle="collapse" data-parent="#accordion" href="#fecha{{$fecha->id}}">{{{ $fecha->jornada }}}</a>
  					</h4>
  				</div>
  				<div id="fecha{{$fecha->id}}" class="panel-collapse collapse">
      				<div class="panel-body">
      					<ul>
      						@foreach ($fecha->partidos()->get() as $key => $partido)
      							<?php $clubes = $partido->clubes()->get() ?>
      							<li>
      								<a href="/partidos/{{$partido->id}}">
      								@if(count($clubes)==2)
      									@if($clubes[0]->pivot->condicion == 'local')
      										{{{ $clubes[0]->nombre }}} - 
      										{{{ $clubes[1]->nombre }}}
      									@else
      										{{{ $clubes[1]->nombre }}} - 
      										{{{ $clubes[0]->nombre }}}
      									@endif
      								@endif :: {{ $partido->fecha_calendario }}
      								</a>
      							</li>
      						@endforeach
      					</ul>
      				</div>
      			</div>
  			</div>
  		@endforeach
  	</div>
  </div>
</div>
@stop
