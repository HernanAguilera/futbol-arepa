@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop
@section('main')

<h1>Edit Torneo</h1>
{{ Form::model($torneo, array('method' => 'PATCH', 'route' => array('torneos.update', $torneo->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $torneo->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('categoria_id', 'Categoria:') }}</td>
            <td>{{ Form::select('categoria_id', $categorias, $torneo->categoria_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('inicio', 'Inicio:') }}</td>
            <td>{{ Form::text('inicio', $torneo->inicio, array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fin', 'Fin:') }}</td>
            <td>{{ Form::text('fin', $torneo->fin, array('class' => 'form-control datepicker')) }}</td>
        </tr>
        <tr>
            <td>{{ Form::label('temporada_id', 'temporada:') }}</td>
            <td>{{ Form::select('temporada_id', $temporadas, $torneo->temporada_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('clubes', 'Clubes:') }}</td>
            <td>
                <ul>
                    @foreach($clubes as $key => $club)
                        <li>
                            {{ Form::label('club-'.$key, $club) }}
                            {{ Form::checkbox('clubes[]', $key, in_array($key, $torneo->clubes), array('id' => 'club-'.$key)) }}
                        </li>
                    @endforeach
                </ul>
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('descripcion', 'Descripcion:') }}</td>
            <td>{{ Form::textarea('descripcion', $torneo->descripcion, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('torneos.show', 'Cancel', $torneo->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
