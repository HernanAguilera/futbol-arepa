@extends('layouts.base')

@section('base')

	<div class="row">
        <div class="col-md-5 col-md-offset-3 login">
	        <h2>Identificarse</h2>
	        @if(Session::get('msg'))
	            <p>{{ Session::get('msg') }}</p>
	        @endif
            {{ Form::open(array('url' => '/login', 'method' => 'POST')) }}
	            {{Form::label('username','Nombre de Usuario')}}
	            {{Form::text('username', '', array('class' => 'form-control', 'placeholder' => 'ingresa tu usuario'))}} <br />
	            {{Form::label('password', 'Contraseña')}}
	            {{Form::password('password', array('class' => 'form-control', 'placeholder' => 'ingresa tu contraseña'))}}<br />
	            {{Form::label('rememberme', 'Recordar mi cuenta')}}
	            {{Form::checkbox('rememberme', 'true', FALSE, array('style' => 'vertical-align:middle')) }}<br>
	            {{ Form::captcha() }}
	            {{Form::submit('Ingresar', array('class'=>'btn btn-default'))}}
            {{ Form::close() }}
            <a href="/password/remind">¿Olvidaste tu contraseña?</a>
            @if ($errors->any())
				<ul>
					{{ implode('', $errors->all('<li class="error">:message</li>')) }}
				</ul>
			@endif
        </div>
    </div>

@stop