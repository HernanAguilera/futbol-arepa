@extends('layouts.admin')

@section('main')

	<h1>Nuevo Rol</h1>
	{{ Form::open(array('route' => 'admin.roles.store')) }}
	<div class="col-md-3">
		{{ Form::label('title', 'Titulo') }}
	</div>
	<div class="col-md-9">
		{{ Form::text('title', '', array('class' => 'form-control')) }}
	</div>
	{{ Form::submit('Guardar', array('class' => 'btn btn-info')) }}
	{{ Form::close() }}

@stop