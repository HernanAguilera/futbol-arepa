@extends('layouts.admin')

@section('main')
<h1>Roles</h1>
<p>{{ link_to_route('admin.roles.create', 'Agregar rol') }}</p>
@if($rols->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<td>Nombre</td>
			</tr>
		</thead>
		<tbody>
			@foreach($rols as $rol)
				<tr>
					<td>{{ $rol->title }}</td>
					<td>{{ link_to_route('admin.roles.edit', 'Edit', array($rol->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.roles.destroy', $rol->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<p>No se ha definido ningún rol aún</p>
@endif
@stop