@extends('layouts.admin')

@section('main')

	<h1>Editar Rol</h1>
	{{ Form::model($rol, array('method' => 'PATCH', 'route' => array('admin.roles.update', $rol->id))) }}
	<div class="col-md-3">
		{{ Form::label('title', 'Titulo') }}
	</div>
	<div class="col-md-9">
		{{ Form::text('title', $rol->title, array('class' => 'form-control')) }}
	</div>
	{{ Form::submit('Guardar', array('class' => 'btn btn-info')) }}
	{{ Form::close() }}

@stop