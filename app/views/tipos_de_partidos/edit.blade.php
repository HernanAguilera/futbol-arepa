@extends('layouts.scaffold')

@section('main')

<h1>Edit Tipos_de_partido</h1>
{{ Form::model($tipos_de_partido, array('method' => 'PATCH', 'route' => array('tipos_de_partidos.update', $tipos_de_partido->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $tipos_de_partido->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $tipos_de_partido->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('tipos_de_partidos.show', 'Cancel', $tipos_de_partido->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
