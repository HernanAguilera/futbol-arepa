@extends('layouts.scaffold')

@section('main')

<h1>All Tipos_de_partidos</h1>

<p>{{ link_to_route('tipos_de_partidos.create', 'Add new tipos_de_partido') }}</p>

@if ($tipos_de_partidos->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Abreviatura</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($tipos_de_partidos as $tipos_de_partido)
				<tr>
					<td>{{{ $tipos_de_partido->nombre }}}</td>
					<td>{{{ $tipos_de_partido->abreviatura }}}</td>
                    <td>{{ link_to_route('tipos_de_partidos.edit', 'Edit', array($tipos_de_partido->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('tipos_de_partidos.destroy', $tipos_de_partido->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no tipos_de_partidos
@endif

@stop
