@extends('layouts.scaffold')

@section('main')

<h1>Show Tipos_de_partido</h1>

<p>{{ link_to_route('tipos_de_partidos.index', 'Return to all tipos_de_partidos') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
				<th>Abreviatura</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $tipos_de_partido->nombre }}}</td>
					<td>{{{ $tipos_de_partido->abreviatura }}}</td>
                    <td>{{ link_to_route('tipos_de_partidos.edit', 'Edit', array($tipos_de_partido->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('tipos_de_partidos.destroy', $tipos_de_partido->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
