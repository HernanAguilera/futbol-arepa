@extends('layouts.admin')

@section('main')

<h1>Estados</h1>

<p>{{ link_to_route('estados-partidos.create', 'Add new estado') }}</p>

@if ($estados->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Abreviatura</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($estados as $estado)
				<tr>
					<td><a href="/estados-partidos/{{{$estado->id}}}">{{{ $estado->nombre }}}</a>
					</td>
					<td>{{{ $estado->abreviatura }}}</td>
                    <td>{{ link_to_route('estados-partidos.edit', 'Edit', array($estado->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('estados-partidos.destroy', $estado->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no estados_de_partidos
@endif

@stop
