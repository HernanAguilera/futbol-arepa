@extends('layouts.admin')

@section('main')

<h1>Show Ciudad</h1>

<p>{{ link_to_route('ciudades.index', 'Return to all ciudads') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Abreviatura</th>
			<th>Visible</th>
			<th>Estado</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $ciudad->nombre }}}</td>
			<td>{{{ $ciudad->abreviatura }}}</td>
			<td>{{{ $ciudad->visible }}}</td>
			<td>{{{ $estados[$ciudad->estado_id] }}}</td>
            <td>{{ link_to_route('ciudades.edit', 'Edit', array($ciudad->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('ciudades.destroy', $ciudad->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
