@extends('layouts.admin')

@section('main')

<h1>All Ciudades</h1>

<p>{{ link_to_route('ciudades.create', 'Add new ciudad') }}</p>

@if ($ciudades->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Abreviatura</th>
				<th>Visible</th>
				<th>Estado</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($ciudades as $ciudad)
				<tr>
					<td>{{{ $ciudad->nombre }}}</td>
					<td>{{{ $ciudad->abreviatura }}}</td>
					<td>{{{ $ciudad->visible }}}</td>
					<td>{{{ $estados[$ciudad->estado_id] }}}</td>
                    <td>{{ link_to_route('ciudades.edit', 'Edit', array($ciudad->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('ciudades.destroy', $ciudad->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no ciudads
@endif

@stop
