@extends('layouts.admin')

@section('main')

<h1>Edit Ciudad</h1>
{{ Form::model($ciudad, array('method' => 'PATCH', 'route' => array('ciudades.update', $ciudad->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $ciudad->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $ciudad->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('visible', 'Visible:') }}</td>
            <td>
                {{ Form::label('si-visible', 'Si') }}
                {{ Form::radio('visible', 'si', $ciudad->visible==='si', array('id' => 'si-visible')) }}
                {{ Form::label('no-visible', 'No') }}
                {{ Form::radio('visible', 'no', $ciudad->visible==='no', array('id' => 'no-visible')) }}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('estado_id', 'Estado_id:') }}</td>
            <td>{{ Form::select('estado_id', $estados, $ciudad->estado_id, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('ciudades.show', 'Cancel', $ciudad->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
