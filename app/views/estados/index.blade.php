@extends('layouts.admin')

@section('main')

<h1>Estados <span class="badge">{{{ $estados->count }}}</span></h1>

<p>{{ link_to_route('estados.create', 'Add new estado') }}</p>

@if ($estados->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Bandera</th>
				<th>Nombre</th>
				<th>Abreviatura</th>
				<th>Pais</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($estados as $estado)
				<tr>
					<td><img height="60" src="{{ $estado->bandera }}"></td>
					<td>{{{ $estado->nombre }}}</td>
					<td>{{{ $estado->abreviatura }}}</td>
					<td>{{{ $paises[$estado->pais_id] }}}</td>
                    <td>{{ link_to_route('estados.edit', 'Edit', array($estado->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('estados.destroy', $estado->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="paginate">{{ $estados->links() }}</div>
@else
	There are no estados
@endif

@stop
