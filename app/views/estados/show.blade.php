@extends('layouts.admin')

@section('main')

<h1>Show Estado</h1>

<p>{{ link_to_route('estados.index', 'Return to all estados') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
				<th>Abreviatura</th>
				<th>Visible</th>
				<th>Pais_id</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $estado->nombre }}}</td>
					<td>{{{ $estado->abreviatura }}}</td>
					<td>{{{ $estado->visible }}}</td>
					<td>{{{ $estado->pais_id }}}</td>
                    <td>{{ link_to_route('estados.edit', 'Edit', array($estado->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('estados.destroy', $estado->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
