@extends('layouts.admin')

@section('main')

<h1>Edit Estado</h1>
{{ Form::model($estado, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('estados.update', $estado->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $estado->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $estado->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td> {{ Form::label('bandera', 'Bandera:') }} </td>
            <td>{{ Form::file('bandera') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('visible', 'Visible:') }}</td>
            <td>
                {{ Form::label('si-visible', 'Si') }}
                {{ Form::radio('visible', 'si', $estado->visible==='si', array('id' => 'si-visible')) }}
                {{ Form::label('no-visible', 'No') }}
                {{ Form::radio('visible', 'no', $estado->visible==='no', array('id' => 'no-visible')) }}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('pais_id', 'Pais_id:') }}</td>
            <td>{{ Form::select('pais_id', $paises, NULL, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('estados.show', 'Cancel', $estado->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
