@extends('layouts.admin')

@section('main')

<h1>All Clubes</h1>

<p>{{ link_to_route('clubes.create', 'Add new club') }}</p>

@if ($clubes->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Fundacion</th>
				<th>Escudo</th>
				<th>Estado</th>
				<th>Estadio</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($clubes as $club)
				<tr>
					<td>{{{ $club->nombre }}}</td>
					<td>{{{ $club->fundacion }}}</td>
					<td><a href="clubes/{{{ $club->id }}}" class="thumbnail"><img src="{{{ $club->escudo_thumb }}}"></a></td>
					<td>{{{ $estados[$club->estado_id] }}}</td>
					<td>{{{ $estadios[$club->estadio_id] }}}</td>
					<td>{{ link_to_route('clubes.show', 'Show', array($club->id), array('class' => 'btn btn-info')) }}</td>
                    <td>{{ link_to_route('clubes.edit', 'Edit', array($club->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('clubes.destroy', $club->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="paginate">
		{{ $clubes->links() }}
	</div>
@else
	There are no club
@endif

@stop
