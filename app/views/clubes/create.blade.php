@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="/css/chosen/chosen.css">
<link rel="stylesheet" type="text/css" href="/css/onoff/jquery.onoff.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/lib/chosen.jquery.min.js" ></script>
<script type="text/javascript" src="/js/lib/jquery.onoff.min.js" ></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
<script type="text/javascript" src="/js/widgets/select.widget.js"></script>
<script type="text/javascript" src="/js/widgets/check.widget.js"></script>
@stop

@section('main')

<h1>Nuevo Club</h1>

{{ Form::open(array('route' => 'clubes.store', 'files' => TRUE)) }}
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('nombre', 'Nombre:') }}
        </div>
        <div class="col-md-12">
            {{ Form::text('nombre', '', array('class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('twitter', 'Twitter:') }}
        </div>
        <div class="col-md-12">
            {{ Form::text('twitter', '', array('class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('fundacion', 'Fecha de fundación:') }}
        </div>
        <div class="col-md-12">
            {{ Form::text('fundacion', '', array('class' => 'form-control datepicker')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('activo', 'Activo:') }}
        </div>
        <div class="col-md-12">
            {{ Form::checkbox('activo', 'si', TRUE) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('torneos', 'Torneos:') }}
        </div>
        <div class="col-md-12">
            {{ Form::select('torneos[]', $torneos, NULL, array('multiple' => 'multiple', 'class' => 'multiselect form-control'))}}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('grupos', 'Grupos:') }}
        </div>
        <div class="col-md-12">
            {{ Form::select('grupos[]', $grupos, NULL, array('multiple' => 'multiple', 'class' => 'multiselect form-control'))}}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('escudo', 'Escudo:') }}
        </div>
        <div class="col-md-12">
            {{ Form::file('escudo') }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('estado_id', 'Estado:') }}
        </div>
        <div class="col-md-12">
            {{ Form::select('estado_id', $estados, NULL, array('class' => 'form-control multiselect')) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            {{ Form::label('estadio_id', 'Estadio:') }}
        </div>
        <div class="col-md-12">
            {{ Form::select('estadio_id', $estadios, NULL, array('class' => 'form-control')) }}
        </div>
    </div>
    <div style="margin-top:3em; margin-left:1em" class="col-md-12">
        
        {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
    </div>

{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


