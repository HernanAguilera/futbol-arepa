@extends('layouts.admin')

@section('more_js')
<script type="text/javascript" src="/js/lib/mustache.js"></script>
<script type="text/javascript" src="/js/lib/typeahead.bundle.js"></script>
<script type="text/javascript" src="/js/widgets/livesearch.widget.js"></script>
<script type="text/javascript" src="/js/modules/futbolistas/livesearch.js"></script>
<script type="text/javascript" src="/js/modules/futbolistas/selectByClubs.js"></script>

@stop

@section('main')

<div class="row">
  <div class="col-md-6">
    <h1>{{ $club->nombre }}</h1>
    <h3>{{{ date('d/m/Y', strtotime($club->fundacion)) }}}</h3>
    <img src="{{{ $club->escudo }}}">
    <p>Estadio: <strong>{{{ $club->estadio()->first()->nombre }}}</strong></p>
  </div>
  <div class="col-md-6">
    <img height="250" src="{{{ $club->estado()->first()->bandera }}}">
  </div>
</div>

<div class="col-md-4">{{ link_to_route('clubes.index', 'Regresar', array(), array('class'=>'btn btn-default')) }}</div>

<div class="col-md-6"> {{ link_to_route('clubes.edit', 'Edit', array($club->id), array('class' => 'btn btn-info')) }}</div>

<div class="col-md-2">  
  <button class="btn btn-danger" data-toggle="modal" data-target="#modalDelete">
    delete
  </button>
</div>

{{ Form::select('torneos', $torneos_del_club, NULL, array('class' => 'form-control', 'id' => 'torneos')) }}

<h2>Futbolistas</h2>
<div id="futbolistas">
</div>
<div>
  {{ Form::text('agregar', '', array('class' => 'form-control typeahead', 'placeholder' => 'Escriba el nombre de un jugador...', 'id' => 'agregar')) }}
</div>

<!-- Confirmación para eliminar -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" id="myModalLabel">Confimación</h4>
      </div>
      <div class="modal-body">
        ¿Esta seguro que desea borrar este registro?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        {{ Form::open(array('method' => 'DELETE', 'style' =>'display:inline', 'route' => array('clubes.destroy', $club->id))) }}
		      {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
		    {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

<!-- Confirmacion agregar futbolista -->
<div class="modal fade" id="ConfirmacionFutbolista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
        <h4 class="modal-title" id="myModalLabel">Confimación</h4>
      </div>
      <div class="modal-body">
        <h3>¿Son correctos los datos?</h3>
        <div class="row">
          <div class="col-md-3">
            <img height="150" id="avatar" src="">
          </div>
          <div class="col-md-9">
            <table id="table-futbolista" class="table">
              <tbody>
                <tr>
                  <th>Nombre:</th>
                  <td id="nombre"></td>
                </tr>
                <tr>
                  <th>Apellido:</th>
                  <td id="apellido"></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="agregar-futbolista" type="button" class="btn btn-primary">Agregar</button>
      </div>
    </div>
  </div>
</div>


@stop
