@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="/css/chosen/chosen.css">
<link rel="stylesheet" type="text/css" href="/css/onoff/jquery.onoff.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/lib/chosen.jquery.min.js" ></script>
<script type="text/javascript" src="/js/lib/jquery.onoff.min.js" ></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
<script type="text/javascript" src="/js/widgets/select.widget.js"></script>
<script type="text/javascript" src="/js/widgets/check.widget.js"></script>
@stop

@section('main')

<h1>Edit Club</h1>
{{ Form::model($club, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('clubes.update', $club->id))) }}
	<table>
		<tr>
           <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $club->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('twitter', 'Twitter:') }}</td>
            <td>{{ Form::text('twitter', $club->twitter, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fundacion', 'Fundacion:') }}</td>
            <td>{{ Form::text('fundacion', $club->fundacion, array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('activo', 'Activo:') }}</td>
            <td>{{ Form::checkbox('activo', 'si', $club->activo === 'si') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('torneos', 'Torneos:') }}</td>
            <td>{{ Form::select('torneos[]', $torneos, $club->torneos, array('multiple' => 'multiple'))}}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('grupos', 'Grupos:') }}</td>
            <td>{{ Form::select('grupos[]', $grupos, $club->grupos, array('multiple' => 'multiple'))}}
            </td>
        </tr>

        <tr>
            <td>{{ Form::label('escudo', 'Escudo:') }}</td>
            <td>{{ Form::file('escudo') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('estado_id', 'Estado:') }}</td>
            <td>{{ Form::select('estado_id', $estados, $club->estado_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('estadio_id', 'Estadio:') }}</td>
            <td>{{ Form::select('estadio_id', $estadios, $club->estadio_id, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('clubes.show', 'Cancel', $club->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
