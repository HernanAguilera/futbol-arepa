@foreach($fecha as $nombre => $grupo)
	@if($nombre != 'default')
		<h3>{{ $nombre }}</h3>
	@endif
<table class="table table-condensed table-hover">
		@foreach($grupo as $partido)
		<tr> 
			<td style="text-align:center">
				<p style="font-size:.8em">Lugar: {{ $partido->estadio }}</p>
				<span style="{{$style}}">
					<div class="col-sm-1">
						<img height="50" src="{{ $partido->clubes['local']->escudo_thumb }}">
					</div>
					<div class="col-sm-3">
						{{ $partido->clubes['local']->nombre }}
						{{-- conteo de goles --}}
						@if(!$partido->goles['local']->anotacion_nula && !$partido->goles['visitante']->anotacion_nula)
							<br>
							@if($partido->finalizado)
								<span style="font-size:.7em">
								@foreach($partido->goles['local']->goles as $gol)
									{{ $gol->futbolista }}
									@foreach($gol->anotaciones as $anotacion)
										<span class="text-info">{{ $anotacion->minuto }}'</span>
										{{ $anotacion->tipo }}
									@endforeach<br>
								@endforeach
								</span>
							@endif
						@endif
					</div>
					<div class="col-sm-4">
						@if($partido->finalizado)
							<span style="font-size:1em" class="label label-default">
								{{ $partido->goles['local']->cantidad }}
								@if($partido->penales['local']>0 && $partido->penales['visitante']>0)
									({{ $partido->penales['local'] }})
								@endif
							</span>
						@endif
						&nbsp;-
						@if($partido->finalizado)
							<span style="font-size:1em" class="label label-default">
								@if($partido->penales['local']>0 && $partido->penales['visitante']>0)
									({{ $partido->penales['visitante'] }})
								@endif
								{{ $partido->goles['visitante']->cantidad }}
							</span>
						@endif
						<br>
						<span style="font-size:.8em">
							{{ strftime("%A %e de %B <br> %l:%M%p", strtotime($partido->horario)) }}
						</span>
						@if($partido->reprogramado)
							<br>
							<span style="font-size:.8em" class="label label-danger">
								REPROGRAMADO
							</span>
						@endif
						@if($partido->suspendido)
							<br>
							<span style="font-size:.8em" class="label label-info">
								SUSPENDIDO
							</span>
						@endif
					</div>
					<div class="col-sm-3">
						{{ $partido->clubes['visitante']->nombre }}
						{{-- conteo de goles --}}
						@if(!$partido->goles['local']->anotacion_nula && !$partido->goles['visitante']->anotacion_nula)
							<br>
							@if($partido->finalizado)
								<span style="font-size:.7em">
								@foreach($partido->goles['visitante']->goles as $gol)
									{{ $gol->futbolista }}
									@foreach($gol->anotaciones as $anotacion)
										<span class="text-info">{{ $anotacion->minuto }}'</span>
										{{ $anotacion->tipo }}
									@endforeach<br>
								@endforeach
								</span>
							@endif
						@endif
					</div>
					<div class="col-sm-1">
						<img height="50" src="{{ $partido->clubes['visitante']->escudo_thumb }}">
					</div>
				</span>
				@if(!empty($partido->nota))
					<div class="col-sm-12">						
						<div class="bg-danger nota" style="font-size:.8em; padding:2px 8px;">
							{{ $partido->nota }}
						</div>
					</div>
				@endif
				@if(!empty($partido->video) && $partido->finalizado)
					<div class="col-sm-12">						
						<div >
							<a class="video btn btn-primary btn-xs" 
							   href="{{ $partido->video }}"
							   title="{{$partido->clubes['local']->nombre.' - '.$partido->clubes['visitante']->nombre}}">
								vídeo resumen
							</a>
						</div>
					</div>
				@endif
			</td> 
		</tr>
		@endforeach
	</table>
	@endforeach