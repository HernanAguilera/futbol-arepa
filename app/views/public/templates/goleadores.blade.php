@if($goleadores->count()>0)
	<h3>Goleadores</h3>
	<table class="table">
		<tbody>
			@foreach($goleadores as $goleador)
				<tr>
					<td><img src="{{$goleador->escudo}}"></td>
					<td>{{ $goleador->nombre }} {{$goleador->apellido}}</td>
					<td>{{ $goleador->anotaciones }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif