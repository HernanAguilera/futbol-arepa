@extends('layouts.base')

@section('title_header')
	{{ $titleHeader }}
@stop

@section('before_body')
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-40276396-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'es'}
	</script>
@stop

@section('base')

	<div class="header row">
		<div class="col-md-4">
			<a 	href="/" 
			title="FutbolArepa">
			<img 
				class="logo" 
				src="/imgs/logo-fa_01.1_100x100.png" 
				alt="FutbolArepa"
			>
			<h1 class="title">FutbolArepa.</h1>
		</a>
		</div>
		<div style="margin-top:2em" class="col-md-4 col-md-offset-4">
			<div style="margin-top:3em" class="col-md-6">
				<a href="https://twitter.com/FutbolArepa" class="twitter-follow-button" data-show-count="false">Follow @FutbolArepa</a>
			</div>
			<div class="col-md-3">
				<div class="fb-follow" data-href="https://www.facebook.com/futbolarepa" data-colorscheme="light" data-layout="box_count" data-show-faces="true"></div>
			</div>
			<div class="col-md-3">
				<div class="g-plusone" data-size="tall"></div>
			</div>
		</div>
	</div>
	<div class="row">
		@yield('completo')
	</div>
	<div class="row">
		<div class="col-md-8">
			@yield('contenido')
			<div class="col-md-4">
				<a href="https://twitter.com/share" class="twitter-share-button" data-via="FutbolArepa">Tweet</a>
			</div>
			<div class="col-md-4">
				<div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
			</div>
			<div class="col-md-4">
				<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
			</div>
		</div>
		<div class="col-md-4">
			@yield('b-lateral')
		</div>
	</div>


@stop