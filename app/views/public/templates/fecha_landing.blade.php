@foreach($fecha as $dia => $fecha_calendario)
	<h3>{{ ucfirst(strftime("%A %e", strtotime($dia)))}} de {{ ucfirst(strftime("%B", strtotime($dia))) }}</h3>
	@foreach($fecha_calendario as $nombre => $grupo)
		@if($nombre != 'default')
			<h4 style="margin: 0px; text-align: center; padding: 6px 0px;" class="bg-primary">{{ $nombre }}</h4>
		@endif
		<table class="table table-condensed table-hover">
			@foreach($grupo as $partido)
			<tr> 
				<td style="text-align:center">
					<span style="{{$style}}">
						<div class="col-md-2">
							<div class="col-md-6">
								@if($partido->suspendido)
									<span 
										class="label label-info" 
										style="font-size:.8em"
										title="Suspendido"
									>
										<li class="fa fa-times fa-2x"></li>
									</span>
								@endif
								@if($partido->reprogramado)
									<span 
										class="text-danger" 
										style="font-size:.8em"
										data-toggle="tooltip" 
									   	data-placement="top"
										title="Reprogramado"
										>
										<li class="fa fa-calendar fa-2x"></li>
									</span>
								@endif
								@if(!empty($partido->video) && $partido->finalizado)
									<span
										data-toggle="tooltip" 
									   	data-placement="top"
									   	title="Resumen del partido"
									>
										
									<a class="video " 
									   href="{{ $partido->video }}"
									   title="{{$partido->clubes['local']->nombre.' - '.$partido->clubes['visitante']->nombre}}">
									   	<li class="fa fa-youtube-play fa-2x"></li>
									</a>
									</span>
								@endif
							</div>
							<div class="col-md-6">
								<span style="font-size:.8em">
									{{ ucfirst(strftime("%l:%M%p", strtotime($partido->horario))) }}
								</span>
							</div>
						</div>
						<div class="col-md-7">
							<div class="col-xs-1">
								<img  src="{{ $partido->clubes['local']->escudo_icon }}">
							</div>
							<div class="col-xs-4">
								{{ $partido->clubes['local']->nombre }}
							</div>
							<div class="col-xs-2">
								<span style="font-size:1em" class="label label-default">{{ $partido->finalizado ? $partido->goles['local']->cantidad : '' }}</span>
								-
								<span style="font-size:1em" class="label label-default">{{ $partido->finalizado ? $partido->goles['visitante']->cantidad : '' }}</span>
								<br>
							</div>
							<div class="col-xs-4">
								{{ $partido->clubes['visitante']->nombre }}
							</div>
							<div class="col-xs-1">
								<img src="{{ $partido->clubes['visitante']->escudo_icon }}">
							</div>
						</div>
						<div class="col-md-3">
							<span 
									style="font-size:.8em"
									data-toggle="tooltip" 
									data-placement="top"
									title="Estadio"
								>
								{{ $partido->estadio }}
							</span>
						</div>
					</span>
				</td> 
			</tr>
			@endforeach
		</table>
	@endforeach
@endforeach