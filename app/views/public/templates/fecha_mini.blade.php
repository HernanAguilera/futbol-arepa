@foreach($fecha as $nombre => $grupo)
@if($nombre != 'default')
	<h4>{{ $nombre }}</h4>
@endif
<table class="table table-condensed table-hover">
		@foreach($grupo as $partido)
		<tr> 
			<td style="text-align:center">
				<span style="{{$style}}">
					<div class="col-xs-4">
						{{ $partido->clubes['local']->nombre }}
					</div>
					<div class="col-xs-4">
						<span style="font-size:1em" class="label label-default">{{ $partido->finalizado ? $partido->goles['local']->cantidad : '' }}</span>
						-
						<span style="font-size:1em" class="label label-default">{{ $partido->finalizado ? $partido->goles['visitante']->cantidad : '' }}</span>
						<br>
						<span style="font-size:.8em">
							{{ strftime("%a %e de %b <br> %l:%M%p", strtotime($partido->horario)) }}
						</span>
						@if($partido->reprogramado)
							<br>
							<span class="label label-danger" style="font-size:.8em">
								REPROGRAMADO
							</span>
						@endif
						@if($partido->suspendido)
							<br>
							<span class="label label-info" style="font-size:.8em">
								SUSPENDIDO
							</span>
						@endif
					</div>
					<div class="col-xs-4">
						{{ $partido->clubes['visitante']->nombre }}
					</div>
				</span>
			</td> 
		</tr>
		@endforeach
	</table>
@endforeach