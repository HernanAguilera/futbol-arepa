@foreach($tabla as $nombre => $grupo)
  <?php $i = 1; ?>
  <h3>{{{ $nombre }}}</h3>
  <div class="table-responsive">
    <table class="table table-bordered table-hover table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th></th>
          <th title="Partidos Jugados">PJ</th>
          <th title="Partidos Ganados">PG</th>
          <th title="Partidos Empatados">PE</th>
          <th title="Partidos Perdidos">PP</th>
          <th title="Goles a favor">GF</th>
          <th title="Goles en contra">GC</th>
          <th title="Diferencia">Dif.</th>
          <th title="Puntos">Pts</th>
        </tr>
      </thead>
      <tbody>
        @foreach($grupo as $club)
          <tr>
            <td>{{$i++}}</td>
            <td>
              <img height="20" src="{{ $club['escudo'] }}">
              {{ $club['nombre'] }}
            </td>
            <td>{{ $club['PJ'] }}</td>
            <td>{{ $club['PG'] }}</td>
            <td>{{ $club['PE'] }}</td>
            <td>{{ $club['PP'] }}</td>
            <td>{{ $club['GF'] }}</td>
            <td>{{ $club['GC'] }}</td>
            <td>{{ $club['Dif'] }}</td>
            <td>{{ $club['Pts'] }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endforeach
    <p>
      PJ: <span class="text-muted">Partidos jugados</span>, 
      PG: <span class="text-muted">Partidos ganados</span>, 
      PE: <span class="text-muted">Partidos empatados</span>, 
      PP: <span class="text-muted">Partidos perdidos</span>,
      GF: <span class="text-muted">Goles a favor</span>,
      GC: <span class="text-muted">Goles en contra</span>,
      Dif.: <span class="text-muted">Diferencia de goles</span>,
      Pts: <span class="text-muted">Puntos</span>
    </p>