@extends('public.templates.base')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/colorbox/colorbox.css">
@stop

@section('more_js')
<script type="text/javascript">
	$('[data-toggle="tooltip"]').tooltip({
	    'placement': 'right'
	});
</script>
<script type="text/javascript" src="/js/lib/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/js/widgets/colorbox.widget.js" ></script>
@stop

@section('completo')
	<h2 style="text-align:center">Últimos Resultados</h2>
	@include('public.templates.fecha_landing', array('fecha' => $ultimosResultados, 'style' => 'font-size:1em'))	
	<h2 style="text-align:center">Próximos Partidos</h2>
	@include('public.templates.fecha_landing', array('fecha' => $proximosPartidos, 'style' => 'font-size:1em'))
@stop