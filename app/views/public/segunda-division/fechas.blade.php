@extends('public.templates.base')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/colorbox/colorbox.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/js/widgets/colorbox.widget.js" ></script>
@stop

@section('contenido')
	<h2>Segunda División</h2>
	<ul class="nav nav-tabs" role="tablist">
	  <li><a href="/segunda-division">Tabla</a></li>
	  <li class="active"><a href="#">Fechas</a></li>
	</ul>
	<ul style="margin-top:1em" class="nav nav-tabs" role="tablist">
		@foreach($fechas as $key => $jornada)
			@if($key == $fechaActiva_id)
				<li class="active" >
					<a href="#">
					{{ $jornada }}
					</a>
				</li>
			@elseif($key == $fechaActual_id)
				<li>
					<a href="/segunda-division/fechas">
						{{ $jornada }}
					</a>
				</li>
			@else
				<li>
					<a href="/segunda-division/fechas/{{$key}}">
						{{ $jornada }}
					</a>
				</li>
			@endif
		@endforeach
	</ul>
	@include('public.templates.fecha', array('fecha' => $fecha, 'style' => 'font-size:1em'))

@stop

@section('b-lateral')

	@include('public.templates.goleadores', compact($goleadores))

@stop