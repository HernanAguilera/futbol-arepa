@extends('public.templates.base')

@section('contenido')

	<h2>Segunda División</h2>
	<ul class="nav nav-tabs" role="tablist">
	  <li class="active"><a href="#">Tabla</a></li>
	  <li><a href="/segunda-division/fechas">Fechas</a></li>
	</ul>
	@include('public.templates.tabla', array('tabla' => $tabla))

@stop

@section('b-lateral')
	<h3>Resultados</h3>
	@include('public.templates.fecha_mini', array('fecha' => $fecha, 'style' => 'font-size:.9em'))
	@include('public.templates.goleadores', compact($goleadores))
@stop