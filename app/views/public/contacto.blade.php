@extends('public.templates.base')

@section('contenido')

<h2>Contacto</h2>
@foreach($errors->all(':message') as $message) 
	{{ $message }} 
@endforeach 
{{ Form::open() }}
<div class="row">
	<div class="col-md-3">
		{{ Form::label('nombre', 'Nombre') }}
	</div>
	<div class="col-md-9">
		{{ Form::text('nombre','', array('class' => 'form-control')) }}
	</div>
	<div class="col-md-3">
		{{ Form::label('correo', 'Correo') }}
	</div>
	<div class="col-md-9">
		{{ Form::text('correo','', array('class' => 'form-control')) }}
	</div>
	<div class="col-md-3">
		{{ Form::label('asunto', 'Asunto') }}
	</div>
	<div class="col-md-9">
		{{ Form::text('asunto','', array('class' => 'form-control')) }}
	</div>
	<div class="col-md-3">
		{{ Form::label('mensaje', 'Mensaje') }}
	</div>
	<div class="col-md-9">
		{{ Form::textarea('mensaje', '', array('class' => 'form-control')) }}
	</div>
	@if(!Session::has('username'))
		<div class="col-md-9 col-md-offset-3">
			{{ Form::captcha() }}
		</div>
	@endif
	<div class="col-md-3">
		{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
	</div>
</div>
{{ Form::close() }}
@stop