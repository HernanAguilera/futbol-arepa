@extends('public.templates.base')

@section('contenido')

	<h3>Resultados</h3>
	@include('public.templates.fecha', array('fecha' => $fechas['actual'], 'style' => 'font-size:1em'))

@stop

@section('b-lateral')
	<h3>Próxima Fecha</h3>
	@include('public.templates.fecha_mini', array('fecha' => $fechas['proxima'], 'style' => 'font-size:.9em'))
	
@stop