@extends('layouts.admin')

@section('main')

<h1>Edit Categoria</h1>
{{ Form::model($categoria, array('method' => 'PATCH', 'route' => array('categorias.update', $categoria->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $categoria->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $categoria->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('pais_id', 'Pais:') }}</td>
            <td>{{ Form::select('pais_id', $paises, $categoria->pais_id, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('categorias.show', 'Cancel', $categoria->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
