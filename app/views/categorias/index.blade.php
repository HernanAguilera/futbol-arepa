@extends('layouts.admin')

@section('main')

<h1>All Categorias</h1>

<p>{{ link_to_route('categorias.create', 'Add new categoria') }}</p>

@if ($categorias->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Abreviatura</th>
				<th>Pais</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($categorias as $categoria)
				<tr>
					<td><a href="/categorias/{{$categoria->id}}">{{{ $categoria->nombre }}}</a></td>
					<td>{{{ $categoria->abreviatura }}}</td>
					<td>{{{ $categoria->pais()->first()->nombre }}}</td>
                    <td>{{ link_to_route('categorias.edit', 'Edit', array($categoria->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('categorias.destroy', $categoria->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no categorias
@endif

@stop
