@extends('layouts.admin')

@section('main')

<h1>Show Categoria</h1>

<p>{{ link_to_route('categorias.index', 'Return to all categorias') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Abreviatura</th>
			<th>Pais</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $categoria->nombre }}}</td>
			<td>{{{ $categoria->abreviatura }}}</td>
			<td>{{{ $categoria->pais()->first()->nombre }}}</td>
            <td>{{ link_to_route('categorias.edit', 'Edit', array($categoria->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('categorias.destroy', $categoria->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>
<h2>Torneos</h2>
<table class="table">
	<thead>
		<th>nombre</th>
		<th>inicio</th>
		<th>fin</th>
	</thead>
	<tbody>
		@foreach($torneos as $torneo)
			<tr>
				<td><a href="/torneos/{{$torneo->id}}">{{{$torneo->nombre}}}</a></td>
				<td>{{{$torneo->inicio}}}</td>
				<td>{{{$torneo->fin}}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

@stop
