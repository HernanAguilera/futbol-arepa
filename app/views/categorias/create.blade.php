@extends('layouts.admin')

@section('main')

<h1>Create Categoria</h1>

{{ Form::open(array('route' => 'categorias.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('pais_id', 'Pais:') }}</td>
            <td>{{ Form::select('pais_id', $paises, NULL, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


