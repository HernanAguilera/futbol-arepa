@extends('layouts.admin')

@section('main')

<h1>Create Pais</h1>

{{ Form::open(array('route' => 'paises.store', 'files' => TRUE)) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
        	<td>{{ Form::label('gentilicio', 'Gentilicio:') }}</td>
        	<td>{{ Form::text('gentilicio', '', array('class' => 'form-control')) }}</
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('bandera', 'Bandera:') }}</td>
            <td>{{ Form::file('bandera') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('visible', 'Visible:') }}</td>
            <td>
                {{ Form::label('si-visible', 'Si') }}
                {{ Form::radio('visible', 'si', NULL, array('id' => 'si-visible')) }}
                {{ Form::label('no-visible', 'No') }}
                {{ Form::radio('visible', 'no', NULL, array('id' => 'no-visible')) }}
            </td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


