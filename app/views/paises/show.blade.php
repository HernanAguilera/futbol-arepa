@extends('layouts.admin')

@section('main')

<p>{{ link_to_route('paises.index', 'Return to all paises') }}</p>

<h1>{{{ $pais->nombre }}}</h1>
<p><img src="{{{ $pais->bandera }}}"></p>


<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Abreviatura</th>
			<th>Visible</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{{ $pais->abreviatura }}}</td>
			<td>{{{ $pais->visible }}}</td>
            <td>{{ link_to_route('paises.edit', 'Edit', array($pais->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('paises.destroy', $pais->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
