@extends('layouts.admin')

@section('main')

<h1>Edit Pais</h1>
{{ Form::model($pais, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('paises.update', $pais->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $pais->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
        	<td>{{ Form::label('gentilicio', 'Gentilicio:') }}</td>
        	<td>{{ Form::text('gentilicio', $pais->gentilicio, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('abreviatura', 'Abreviatura:') }}</td>
            <td>{{ Form::text('abreviatura', $pais->abreviatura, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('bandera', 'Bandera:') }}</td>
            <td>{{ Form::file('bandera') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('visible', 'Visible:') }}</td>
            <td>
                {{ Form::label('si-visible', 'Si') }}
                {{ Form::radio('visible', 'si', $pais->visible==='si', array('id' => 'si-visible')) }}
                {{ Form::label('no-visible', 'No') }}
                {{ Form::radio('visible', 'no', $pais->visible==='no', array('id' => 'no-visible')) }}
            </td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('paises.show', 'Cancel', $pais->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
