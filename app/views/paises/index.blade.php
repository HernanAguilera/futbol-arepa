@extends('layouts.admin')

@section('main')

<h1>All Pais</h1>

<p>{{ link_to_route('paises.create', 'Add new pais') }}</p>

@if ($paises->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Bandera</th>
				<th>Nombre</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($paises as $pais)
				<tr>
					<td><img height="50" src="{{{ $pais->bandera }}}"></td>
					<td>{{{ $pais->nombre }}}</td>
					<td>{{ link_to_route('paises.show', 'Show', array($pais->id), array('class' => 'btn btn-info')) }}</td>
                    <td>{{ link_to_route('paises.edit', 'Edit', array($pais->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('paises.destroy', $pais->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no pais
@endif

@stop
