@extends('layouts.admin')

@section('main')

<h1>Show Tiros_penale</h1>

<p>{{ link_to_route('tiros-penales.index', 'Return to all penals') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Partido_id</th>
				<th>Futbolista_id</th>
				<th>Club_id</th>
				<th>Anotado</th>
				<th>Orden</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $penal->partido_id }}}</td>
					<td>{{{ $penal->futbolista_id }}}</td>
					<td>{{{ $penal->club_id }}}</td>
					<td>{{{ $penal->anotado }}}</td>
					<td>{{{ $penal->orden }}}</td>
                    <td>{{ link_to_route('tiros-penales.edit', 'Edit', array($penal->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('tiros-penales.destroy', $penal->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
