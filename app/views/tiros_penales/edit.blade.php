@extends('layouts.admin')

@section('main')

<h1>Edit Tiros_penale</h1>
{{ Form::model($penal, array('method' => 'PATCH', 'route' => array('tiros-penales.update', $penal->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('partido_id', 'Partido_id:') }}</td>
            <td>{{ Form::input('number', 'partido_id', $penal->partido_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista_id:') }}</td>
            <td>{{ Form::input('number', 'futbolista_id', $penal->futbolista_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('club_id', 'Club_id:') }}</td>
            <td>{{ Form::input('number', 'club_id', $penal->club_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('anotado', 'Anotado:') }}</td>
            <td>{{ Form::checkbox('anotado', $penal->anotado, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('orden', 'Orden:') }}</td>
            <td>{{ Form::text('orden', $penal->orden, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('tiros-penales.show', 'Cancel', $penal->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
