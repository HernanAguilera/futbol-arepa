@extends('layouts.admin')

@section('main')

<img class="avatar" src="{{{ $integrante->avatar }}}">
<h1>{{{ $integrante->nombre }}} {{{ $integrante->apellido }}}</h1>

<p>{{ link_to_route('integrantes-cuerpos-tecnicos.index', 'Return to all integrantes de cuerpos tecnicos') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Fecha de nacimiento</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $integrante->fecha_nacimiento }}}</td>
            <td>{{ link_to_route('integrantes-cuerpos-tecnicos.edit', 'Edit', array($integrante->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('integrantes-cuerpos-tecnicos.destroy', $integrante->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
