@extends('layouts.admin')

@section('main')

<h1>Edit Integrante de cuerpo técnico</h1>
{{ Form::model($integrante, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('integrantes-cuerpos-tecnicos.update', $integrante->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $integrante->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('apellido', 'Apellido:') }}</td>
            <td>{{ Form::text('apellido', $integrante->apellido, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fecha_nacimiento', 'Fecha_nacimiento:') }}</td>
            <td>{{ Form::text('fecha_nacimiento', $integrante->fecha_nacimiento, array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('avatar', 'Avatar:') }}</td>
            <td>{{ Form::file('avatar') }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('integrantes-cuerpos-tecnicos.show', 'Cancel', $integrante->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
