@extends('layouts.admin')

@section('main')

<h1>All Integrantes de cuerpos técnicos</h1>

<p>{{ link_to_route('integrantes-cuerpos-tecnicos.create', 'Add new integrante') }}</p>

@if ($integrantes->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Avatar</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Fecha de nacimiento</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($integrantes as $integrante)
				<tr>
					<td style="text-align:center">
						<a href="integrantes-cuerpos-tecnicos/{{$integrante->id}}"><img src="{{{ $integrante->avatar }}}"></a>
					</td>
					<td>{{{ $integrante->nombre }}}</td>
					<td>{{{ $integrante->apellido }}}</td>
					<td>{{{ $integrante->fecha_nacimiento }}}</td>
                    <td>{{ link_to_route('integrantes-cuerpos-tecnicos.edit', 'Edit', array($integrante->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('integrantes-cuerpos-tecnicos.destroy', $integrante->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="paginate">{{ $integrantes->links() }}</div>
@else
	There are no integrantes
@endif

@stop
