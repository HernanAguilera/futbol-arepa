@extends('layouts.admin')

@section('main')

<h1>Create Integrante de cuerpo técnico</h1>

{{ Form::open(array('route' => 'integrantes-cuerpos-tecnicos.store', 'files' => TRUE)) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('apellido', 'Apellido:') }}</td>
            <td>{{ Form::text('apellido', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fecha_nacimiento', 'Fecha de nacimiento:') }}</td>
            <td>{{ Form::text('fecha_nacimiento', '', array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('avatar', 'Avatar:') }}</td>
            <td>{{ Form::file('avatar') }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


