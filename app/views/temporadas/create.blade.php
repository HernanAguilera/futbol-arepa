@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop
@section('main')

@section('main')

<h1>Create Temporada</h1>

{{ Form::open(array('route' => 'temporadas.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('inicio', 'Inicio:') }}</td>
            <td>{{ Form::text('inicio', '', array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fin', 'Fin:') }}</td>
            <td>{{ Form::text('fin', '', array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
        	<td>{{ Form::label('activa', 'Activa:') }}</td>
        	<td>
        		{{ Form::label('si-activa', 'Si') }}
        		{{ Form::radio('activa', 'si', NULL, array('id' => 'si-activa')) }}
        		{{ Form::label('no-activa', 'No') }}
        		{{ Form::radio('activa', 'no', NULL, array('id' => 'no-activa')) }}
        	</td>
        </tr>

        <tr>
        	<td>{{ Form::label('actual', 'Actual:') }}</td>
        	<td>
        		{{ Form::label('si-actual', 'Si') }}
        		{{ Form::radio('actual', 'si', NULL, array('id' => 'si-actual')) }}
        		{{ Form::label('no-actual', 'No') }}
        		{{ Form::radio('actual', 'no', NULL, array('id' => 'no-actual')) }}
        	</td>
        </tr>

        <tr>
        	<td>{{ Form::label('pais_id', 'Pais:') }}</td>
        	<td>
        		{{ Form::select('pais_id', $paises, NULL, array('class' => 'form-control')) }}
        	</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


