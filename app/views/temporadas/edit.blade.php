@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop
@section('main')

<h1>Edit Temporada</h1>
{{ Form::model($temporada, array('method' => 'PATCH', 'route' => array('temporadas.update', $temporada->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $temporada->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('inicio', 'Inicio:') }}</td>
            <td>{{ Form::text('inicio', $temporada->inicio, array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fin', 'Fin:') }}</td>
            <td>{{ Form::text('fin', $temporada->fin, array('class' => 'form-control datepicker')) }}</td>
        </tr>

        <tr>
        	<td>{{ Form::label('activa', 'Activa:') }}</td>
        	<td>
        		{{ Form::label('si-activa', 'Si') }}
        		{{ Form::radio('activa', 'si', $temporada->activa==='si', array('id' => 'si-activa')) }}
        		{{ Form::label('no-activa', 'No') }}
        		{{ Form::radio('activa', 'no', $temporada->activa==='no', array('id' => 'no-activa')) }}
        	</td>
        </tr>

        <tr>
        	<td>{{ Form::label('actual', 'Actual:') }}</td>
        	<td>
        		{{ Form::label('si-actual', 'Si') }}
        		{{ Form::radio('actual', 'si', $temporada->actual==='si', array('id' => 'si-actual')) }}
        		{{ Form::label('no-actual', 'No') }}
        		{{ Form::radio('actual', 'no', $temporada->actual==='no', array('id' => 'no-actual')) }}
        	</td>
        </tr>

        <tr>
        	<td>{{ Form::label('pais_id', 'Pais:') }}</td>
        	<td>
        		{{ Form::select('pais_id', $paises, $temporada->pais_id, array('class' => 'form-control')) }}
        	</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('temporadas.show', 'Cancel', $temporada->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
