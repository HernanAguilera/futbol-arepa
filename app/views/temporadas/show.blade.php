@extends('layouts.admin')

@section('main')

<h1>Show Temporada</h1>

<p>{{ link_to_route('temporadas.index', 'Return to all temporadas') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Nombre</th>
				<th>Inicio</th>
				<th>Fin</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $temporada->nombre }}}</td>
					<td>{{{ $temporada->inicio }}}</td>
					<td>{{{ $temporada->fin }}}</td>
                    <td>{{ link_to_route('temporadas.edit', 'Edit', array($temporada->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('temporadas.destroy', $temporada->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
