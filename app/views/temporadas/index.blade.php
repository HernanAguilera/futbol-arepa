@extends('layouts.admin')

@section('main')

<h1>All Temporadas</h1>

<p>{{ link_to_route('temporadas.create', 'Add new temporada') }}</p>

@if ($temporadas->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Inicio</th>
				<th>Fin</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($temporadas as $temporada)
				<tr>
					<td>{{{ $temporada->nombre }}}</td>
					<td>{{{ $temporada->inicio }}}</td>
					<td>{{{ $temporada->fin }}}</td>
                    <td>{{ link_to_route('temporadas.edit', 'Edit', array($temporada->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('temporadas.destroy', $temporada->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no temporadas
@endif

@stop
