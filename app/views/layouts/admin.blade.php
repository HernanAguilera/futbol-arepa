@extends('layouts.base')

@section('base')

	<div class="col-md-3">
		<div class="panel panel-default">
			<div class="panel-heading">Administración</div>
			<div class="panel-body list-group" style="padding:0">
					<!-- <a class="list-group-item" href="/admin">Inicio</a>
					<a class="list-group-item" href="/admin/roles">Roles</a>
					<a class="list-group-item" href="/admin/users">Usuarios</a>
					<a class="list-group-item" href="/paises">Pais</a>
					<a class="list-group-item" href="/estados">Estados</a>
					<a class="list-group-item" href="/ciudades">Ciudades</a>
					<a class="list-group-item" href="#"></a>
					<a class="list-group-item" href="/futbolistas-amonestados">Amonestados</a>
					<a class="list-group-item" href="/arbitros">Arbitros</a>
					<a class="list-group-item" href="/cargos-cuerpos-tecnicos">Cargos cuerpos técnicos</a>
					<a class="list-group-item" href="/categorias">Categorias</a>
					<a class="list-group-item" href="/clubes">Clubes</a>
					<a class="list-group-item" href="/colegios">Colegios</a>
					<a class="list-group-item" href="/estadios">Estadios</a>
					<a class="list-group-item" href="/fechas">Fechas</a>
					<a class="list-group-item" href="/futbolistas">Futbolistas</a>
					<a class="list-group-item" href="/galerias/estadios">Galerias</a>
					<a class="list-group-item" href="/goles">Goles</a>
					<a class="list-group-item" href="/grupos">Grupos</a>
					<a class="list-group-item" href="/integrantes-cuerpos-tecnicos">Integrantes de cuerpos técnicos</a>
					<a class="list-group-item" href="/lesiones">Lesiones</a>
					<a class="list-group-item" href="/partidos">Partidos</a>
					<a class="list-group-item" href="/posiciones">Posiciones</a>
					<a class="list-group-item" href="/temporadas">Temporadas</a>
					<a class="list-group-item" href="/torneos">Torneos</a>
					<a class="list-group-item" href="/roles-arbitros">Roles de arbitros</a> -->
					@if(Session::has('menu'))
						@foreach(Session::get('menu', array()) as $item)
							<a class="list-group-item" href="{{ $item['url'] }}">{{ $item['title'] }}</a>
						@endforeach
					@endif
			</div>
		</div>
	</div>
	<div class="col-md-9">	
		@if (Session::has('message'))
			<div class="{{ Session::has('class') ? 'alert alert-'.Session::get('class') : 'flash alert' }}">
				<p>{{ Session::get('message') }}</p>
			</div>
		@endif
		@yield('main')
	</div>

@stop