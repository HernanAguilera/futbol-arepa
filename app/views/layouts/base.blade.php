<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Resultados del Fútbol Venezolano, Primera, Segunda, y Tercera División" >
		<meta name="keywords" content="Fútbol Venezolano, Primera División, Segunda División, Tercera División, Tabla de Clasificación, Proximos Encuentros, Torneo Apertura, Torneos Clausura, Torneo de Promoción y Permanencia, Torneo de Ascenso, Copa Sudamericana, Copa Libertadores, Pre Libertados, Pre Sudamericana" >
		<meta name="author" content="Hernán Aguilera" >
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="/imgs/fa_favicon.ico">
		<title>
			@yield('title_header')
		</title>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-theme-futbolarepa.min.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		@yield('more_css')
		<link rel="stylesheet" type="text/css" href="/css/style.css">
	</head>
	<body>
		@yield('before_body')
		<div class="main-menu">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a style="padding: 2px 6px" class="navbar-brand" href="#">
				      		<img
								src="/imgs/logo-fa_01.1_50x50.png" 
								alt="FutbolArepa"
							>
					  </a>
				    </div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="/">Inicio</a></li>
							<li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Primera División
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/primera-division">Tabla</a></li>
									<li><a href="/primera-division/fechas">Fechas</a></li>
								</ul>
							</li>
							<li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Segunda División
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/segunda-division">Tabla</a></li>
									<li><a href="/segunda-division/fechas">Fechas</a></li>
								</ul>
							</li>
							<li><a href="/copa-venezuela">Copa Venezuela</a></li>
							<li>
								<a href="#" 
								   class="dropdown-toggle" 
								   data-toggle="dropdown">
									Tercera División
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/tercera-division">Tabla</a></li>
									<li><a href="/tercera-division/fechas">Fechas</a></li>
								</ul>
							</li>
							<li><a href="/contacto">Contacto</a></li>
							<li>
								<a href="#">{{ date('g:ia') }}</a>
							</li>
						</ul>
						@if(Session::has('username'))
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										{{ Session::get('username'); }}
									<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="/logout">Salir</a></li>
									</ul>
								</li>
								<li>
									<a href="#">{{ Locale::getDefault() }}</a>
								</li>
							</ul>
						@endif
					</div>
				</div>
			</nav>
		</div>
		<div class="container">
			@if(Auth::user())
				<p>{{ Auth::user()->rol->title }}</p>
			@endif
			@yield('base')
		</div>
		<div class="container">
			<footer>
			  <p class="text-muted">Created by: <a target="__blank" href="http://hernanaguilera.com.ve">Hernán Aguilera</a></p>
			  <p class="text-muted">Contact information: <a href="mailto:contacto@hernanaguilera.com.ve">contacto@hernanaguilera.com.ve</a>.</p>
			</footer> 
		</div>
		<script type="text/javascript" src="/js/lib/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="/js/lib/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/init.js"></script>
		@yield('more_js')
		<script type="text/javascript" src="/js/ready.js"></script>
	</body>
</html>