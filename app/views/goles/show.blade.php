@extends('layouts.admin')

@section('main')

<h1>Show Gol</h1>

<p>{{ link_to_route('goles.index', 'Return to all goles') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Minuto</th>
			<th>Partido</th>
			<th>Club</th>
			<th>Futbolista</th>
			<th>Caracteristica</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $gol->minuto }}}</td>
			<td>{{{ $gol->partido_id }}}</td>
			<td>{{{ $gol->club_id }}}</td>
			<td>{{{ $gol->futbolista_id }}}</td>
			<td>{{{ $gol->tipo()->first()->descripcion }}}</td>
            <td>{{ link_to_route('goles.edit', 'Edit', array($gol->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('goles.destroy', $gol->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop
