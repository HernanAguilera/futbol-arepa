@extends('layouts.admin')

@section('main')

<h1>Create Gol</h1>

{{ Form::open(array('route' => 'goles.store')) }}
	<table>
		<tr>
            <td>{{ Form::label('minuto', 'Minuto:') }}</td>
            <td>{{ Form::input('number', 'minuto', '', array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tiempo', 'Tiempo:') }}</td>
            <td>{{ Form::select('tiempo', array('1T', '2T', '1TEX', '2TEX'), NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('partido_id', 'Partido:') }}</td>
            <td>{{ Form::select('partido_id', $partidos, NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('club_id', 'Club:') }}</td>
            <td>{{ Form::select('club_id', $clubes, NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista:') }}</td>
            <td>{{ Form::select('futbolista_id', $futbolistas, NULL, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tipo_id', 'Caracteristicas del gol:') }}</td>
            <td>{{ Form::select('tipo_id', $tipos, NULL, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td colspan=2>
				{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


