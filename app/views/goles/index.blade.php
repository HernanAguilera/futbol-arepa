@extends('layouts.admin')

@section('main')

<h1>All Goles</h1>

<p>{{ link_to_route('goles.create', 'Add new gol') }}</p>
<div class="paginate">{{ $goles->links() }}</div>
@if ($goles->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Tiempo</th>
				<th>Minuto</th>
				<th>Partido</th>
				<th>Club</th>
				<th>Futbolista</th>
				<th>Caracteristicas</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($goles as $gol)
				<tr>
					<td>{{{ $gol->tiempo }}}</td>
					<td>{{{ $gol->minuto }}}</td>
					<td>{{{ $gol->partido_id }}}</td>
					<td>{{{ $gol->club_id }}}</td>
					<td>{{{ $gol->futbolista_id }}}</td>
					<td>{{{ $gol->tipo()->first()->descripcion }}}</td>
                    <td>{{ link_to_route('goles.edit', 'Edit', array($gol->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('goles.destroy', $gol->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
<div class="paginate">{{ $goles->links() }}</div>
@else
	There are no goles
@endif

@stop
