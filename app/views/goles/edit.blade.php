@extends('layouts.admin')

@section('main')

<h1>Edit Gol</h1>
{{ Form::model($gol, array('method' => 'PATCH', 'route' => array('goles.update', $gol->id))) }}
	<table>
		<tr>
            <td>{{ Form::label('minuto', 'Minuto:') }}</td>
            <td>{{ Form::input('number', 'minuto', $gol->minuto, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tiempo', 'Tiempo:') }}</td>
            <td>{{ Form::select('tiempo', array('1T' => '1T', '2T' => '2T', '1TEX' => '1TEX', '2TEX' => '2TEX'), $gol->tiempo, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('partido_id', 'Partido_id:') }}</td>
            <td>{{ Form::input('number', 'partido_id', $gol->partido_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('club_id', 'Club_id:') }}</td>
            <td>{{ Form::input('number', 'club_id', $gol->club_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('futbolista_id', 'Futbolista_id:') }}</td>
            <td>{{ Form::input('number', 'futbolista_id', $gol->futbolista_id, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('tipo_id', 'Caracteristicas del gol:') }}</td>
            <td>{{ Form::select('tipo_id', $tipos, $gol->tipo_id, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('goles.show', 'Cancel', $gol->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
