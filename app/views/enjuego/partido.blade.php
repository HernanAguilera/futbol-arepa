@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/partido.css">
@stop

@section('more_js')
<script type="text/javascript" src="/js/modules/partidos/anotarGol.js"></script>
<script type="text/javascript" src="/js/modules/partidos/amonestar.js"></script>
<script type="text/javascript" src="/js/modules/partidos/estado.js"></script>
<script type="text/javascript" src="/js/modules/partidos/periodo.js"></script>
<script type="text/javascript" src="/js/modules/partidos/penales.js"></script>
<script type="text/javascript" src="/js/modules/partidos/haypenales.js"></script>
@stop

@section('title_header')
	{{ $clubes['local']->nombre }} - {{ $clubes['visitante']->nombre }}
@stop

@section('main')
	<div class="jumbotron">	
		<h1>{{{ $estadio->nombre }}}</h1>
		<h2>{{{ $torneo->nombre }}}</h2>
		<h3>{{{ $fecha->jornada }}} - {{{ $partido->fecha_calendario }}}</h3>
	</div>
	<div style="margin-bottom:3em" class="row">
		{{ Form::label('penales', '¿Hay penales?') }}
		{{ Form::select('haypenales', array('1' => 'Penales', '0' => 'Accion de juego'), '0', array('class' => 'form-control', 'id' => 'haypenales')) }}
	</div>
	<div class="row">
		<div style="text-align:center" class="col-md-3">
			<img src="{{ $clubes['local']->escudo }}" title="{{ $clubes['local']->nombre }}">
			<input id="local" type="hidden" value="{{ $clubes['local']->id }}" >
		</div>
		<div style="text-align:center" class="col-md-1">
			<p class="anotaciones">
				<span class="gol gol-local label label-default">
					{{$goles['local']->cantidad}}
					@if($penales['local']->count()>0)
						({{ $penales['local']->anotados }})
					@endif
				</span>				
			</p>
		</div>
		<div class="col-md-2">
			{{ Form::label('minuto', 'Minuto:') }}
			{{ Form::input('number', 'minuto', '', array('class' => 'form-control', 'id' => 'minuto')) }}
		</div>
		<div class="col-md-2">
			{{ Form::label('tiempo', 'Tiempo') }}
			{{ Form::select('tiempo', array('1T' => '1T', '2T' => '2T'), '1', array('class' => 'form-control', 'id' => 'tiempo')) }}
		</div>
		<div style="text-align:center" class="col-md-1">
			<p class="anotaciones">
				<span class="gol gol-visitante label label-default">
					@if($penales['visitante']->count()>0)
						({{ $penales['visitante']->anotados }})
					@endif
					{{$goles['visitante']->cantidad}}
				</span>
			</p>
		</div>
		<div style="text-align:center" class="col-md-3">
			<img src="{{ $clubes['visitante']->escudo }}" title="{{ $clubes['visitante']->nombre }}">
			<input id="visitante" type="hidden" value="{{ $clubes['visitante']->id }}" >
		</div>
	</div>

	<div class="row goles estadisticas">
		<div class="col-md-12 title-section">
			<h4>Goles</h4>
		</div>
		<div style="text-align:right" class="col-md-6">
			<ul>
				@foreach($goles['local']->goles as $gol)
				<li>
					@foreach($gol->anotaciones as $anotacion)
						<a class="label label-primary" 
						   target="__blank" 
						   href="/goles/{{$anotacion->id}}">
							{{ $anotacion->minuto }}'
						</a>
						{{ $anotacion->tipo }}
						&nbsp;
					@endforeach
					{{ $gol->futbolista }}
				</li>
				@endforeach
			</ul>
		</div>
		<div class="col-md-6">
			<ul>
				@foreach($goles['visitante']->goles as $gol)
				<li>
					{{ $gol->futbolista }}
					@foreach($gol->anotaciones as $anotacion)
						<a class="label label-primary" 
						   target="__blank" 
						   href="/goles/{{$anotacion->id}}">
							{{ $anotacion->minuto }}'
						</a>
						{{ $anotacion->tipo }}
						&nbsp;
					@endforeach
				</li>
				@endforeach
			</ul>
		</div>
	</div>
	<div class="row amonestados">
		<div class="col-md-12 title-section">
			<h4>Amonestados</h4>
		</div>
		<div style="text-align:right" class="col-md-6">
			@if($amonestados['local']->count()>0)
				<ul>
					@foreach($amonestados['local'] as $amonestado)
					<li>
						@if($amonestado->tipo==='amarilla')
							<a class="label label-warning" 
							   target="__blank" 
							   href="/futbolistas-amonestados/{{$amonestado->id}}">
								{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
							</a>
						@else
							<a class="label label-danger"
							   target="__blank" 
							   href="/futbolistas-amonestados/{{$amonestado->id}}">
								{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
							</a>
						@endif
						{{ $futbolistas['local'][$amonestado->futbolista_id] }}
					</li>
					@endforeach
				</ul>
			@endif
		</div>
		<div class="col-md-6">
			@if($amonestados['visitante']->count()>0)
				<ul>
					@foreach($amonestados['visitante'] as $amonestado)
					<li>{{ $futbolistas['visitante'][$amonestado->futbolista_id] }}
						@if($amonestado->tipo==='amarilla')
							<a class="label label-warning" 
							   target="__blank" 
							   href="/futbolistas-amonestados/{{$amonestado->id}}">
								{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
							</a>
						@else
							<a class="label label-danger"
							   target="__blank" 
							   href="/futbolistas-amonestados/{{$amonestado->id}}">
								{{ $amonestado->tiempo == '2T' ? ($amonestado->minuto+45) : $amonestado->minuto }}'
							</a>
						@endif
					</li>
					@endforeach
				</ul>
			@endif
		</div>
	</div>
	@if($penales['local']->count()>0 || $penales['visitante']->count()>0)
	<div class="row">
		<div class="col-md-12 title-section">
			<h4>Penales</h4>
		</div>
		<div style="text-align:right;" class="col-md-6">
			@if($penales['local']->count()>0)
				<ol>
					@foreach($penales['local'] as $penal)
						<li style="margin-top:.2em">
							@if($penal->anotado)
								<a href="/tiros-penales/{{ $penal->id }}" target="__blank" class="label label-success">
									{{ $futbolistas['local'][$penal->futbolista_id] }}
								</a>
							@else
								<a href="/tiros-penales/{{ $penal->id }}" target="__blank" class="label label-info">
									{{ $futbolistas['local'][$penal->futbolista_id] }}
								</a>
							@endif
						</li>
					@endforeach
				</ol>
			@endif
		</div>
		<div class="col-md-6">
			@if($penales['visitante']->count()>0)
				<ol>
					@foreach($penales['visitante'] as $penal)
						<li style="margin-top:.2em">
							@if($penal->anotado)
								<a href="/tiros-penales/{{ $penal->id }}" target="__blank" class="label label-success">
									{{ $futbolistas['visitante'][$penal->futbolista_id] }}
								</a>
							@else
								<a href="/tiros-penales/{{ $penal->id }}" target="__blank" class="label label-info">
									{{ $futbolistas['visitante'][$penal->futbolista_id] }}
								</a>
							@endif
						</li>
					@endforeach
				</ol>
			@endif
		</div>
	</div>
	@endif
	<div class="row" id="en-juego">
		
		<div class="row controles">	
			<div class="col-md-6">
				<div class="row goles">
					<div class="col-md-12">
						{{ Form::label('goles', 'Goles:') }}
					</div>	
					<div class="col-md-12">
						{{ Form::select('futbolista', $futbolistas['local'], NULL, array('class' => 'form-control', 'id' => 'anotador-local')) }}
					</div>
					<div class="col-md-6">
						{{ Form::select('tipo', $tipos, NULL, array('class' => 'form-control', 'id' => 'tipo-gol-local')) }}
					</div>
					<div class="col-md-6">
						{{ Form::button('Gol', array('class' => 'btn btn-block btn-primary', 'id' => 'gol-local')) }}
					</div>
				</div>
				<div class="row amonestados">
					<div class="col-md-12">
						{{ Form::label('amonestados', 'Amonestados:') }}
					</div>	
					<div class="col-md-8">
						{{ Form::select('futbolista', $futbolistas['local'], NULL, array('class' => 'form-control', 'id' => 'amonestado-local')) }}
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-6">
								{{ Form::button('A', array('class' => 'btn btn-warning btn-block', 'id' => 'amarilla-local')) }}
							</div>
							<div class="col-md-6">
								{{ Form::button('R', array('class' => 'btn btn-danger btn-block', 'id' => 'roja-local')) }}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row goles">
					<div class="col-md-12">
						{{ Form::label('goles', 'Goles:') }}
					</div>	
					<div class="col-md-12">
						{{ Form::select('anotador', $futbolistas['visitante'], NULL, array('class' => 'form-control', 'id' => 'anotador-visitante')) }}
					</div>
					<div class="col-md-6">
						{{ Form::select('tipo', $tipos, NULL, array('class' => 'form-control', 'id' => 'tipo-gol-visitante')) }}
					</div>
					<div class="col-md-6">
						{{ Form::button('Gol', array('class' => 'btn btn-block btn-primary', 'id' => 'gol-visitante')) }}
					</div>
				</div>
				<div class="row amonestados">
					<div class="col-md-12">
						{{ Form::label('amonestados', 'Amonestados:') }}
					</div>	
					<div class="col-md-8">
						{{ Form::select('amonestado', $futbolistas['visitante'], NULL, array('class' => 'form-control', 'id' => 'amonestado-visitante')) }}
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-6">
								{{ Form::button('A', array('class' => 'btn btn-warning btn-block', 'id' => 'amarilla-visitante')) }}
							</div>
							<div class="col-md-6">
								{{ Form::button('R', array('class' => 'btn btn-danger btn-block', 'id' => 'roja-visitante')) }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row penales" id="tanda-penales">
		<div class="col-md-6">
			<div class="col-md-12">
				{{ Form::label('penales', 'Penales:') }}
			</div>	
			<div class="col-md-12">
				{{ Form::select('anotador', $futbolistas['local'], NULL, array('class' => 'form-control', 'id' => 'anotador-penal-local')) }}
			</div>
			<div class="col-md-3">
				{{ Form::checkbox('anotado-local', '1', TRUE, array('class' => 'form-control', 'id' => 'anotado-local')) }}
			</div>
			<div class="col-md-3">
				{{ Form::input('number', 'orden-local', 0, array('class' => 'form-control', 'id' => 'orden-local')) }}
			</div>
			<div class="col-md-6">
				{{ Form::button('Cobrar', array('class' => 'btn btn-block btn-primary', 'id' => 'penal-local')) }}
			</div>
		</div>
		<div class="col-md-6">
			<div class="col-md-12">
				{{ Form::label('penales', 'Penales:') }}
			</div>	
			<div class="col-md-12">
				{{ Form::select('anotador', $futbolistas['visitante'], NULL, array('class' => 'form-control', 'id' => 'anotador-penal-visitante')) }}
			</div>
			<div class="col-md-3">
				{{ Form::checkbox('anotado-visitante', '1', TRUE, array('class' => 'form-control', 'id' => 'anotado-visitante')) }}
			</div>
			<div class="col-md-3">
				{{ Form::input('number', 'orden-visitante', 0, array('class' => 'form-control', 'id' => 'orden-visitante')) }}
			</div>
			<div class="col-md-6">
				{{ Form::button('Cobrar', array('class' => 'btn btn-block btn-primary', 'id' => 'penal-visitante')) }}
			</div>
		</div>
	</div>


	<!-- boton para especificar que se desea enviar tweet -->
	<div style="margin-top:5em" class="row">
		<div class="col-md-3">
			{{ Form::select('periodo', array('I' => 'Inicio', 'MT' => 'Mediotiempo', 'I2' => 'Inicia 2do tiempo', 'F' => 'Final', 'A' => 'Ayer'), NULL, array('class' => 'form-control', 'id' => 'periodo')) }}
		</div>
		<div class="col-md-2">
			{{ Form::button('Periodo', array('class' => 'btn btn-info', 'id' => 'btn-periodo')) }}
		</div>
		<div class="col-md-5 col-md-offset-2">
			{{ Form::label('tweet', 'Publicar en @FutbolArepa') }}
			{{ Form::checkbox('tweet', 'si', FALSE, array('class' => 'check-control', 'id' => 'tweet')) }}
			{{ Form::button('Enviar status', array('id' => 'status', 'class' => 'btn btn-info')) }}
		</div>
	</div>
	<!-- Ventana de confirmacion de envio de tweet -->
	<div class="modal fade" id="modalTweet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
	        <h4 class="modal-title" id="myModalLabel">Enviar tweet</h4>
	      </div>
	      <div class="modal-body">
	        ¿Esta seguro que desea borrar este registro?
	      </div>
	      <div class="modal-footer">
	        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cerrar">
	        <input type="button" class="btn btn-primary" value="Enviar">
	      </div>
	    </div>
	  </div>
	</div>

@stop