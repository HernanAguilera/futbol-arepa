@extends('layouts.admin')

@section('more_css')
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
@stop

@section('more_js')
<script type="text/javascript" src="/js/lib/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/js/widgets/date.widget.js"></script>
@stop

@section('main')

<h1>Edit Colegio</h1>
{{ Form::model($colegio, array('method' => 'PATCH', 'route' => array('colegios.update', $colegio->id))) }}
	<table>
		        <tr>
            <td>{{ Form::label('nombre', 'Nombre:') }}</td>
            <td>{{ Form::text('nombre', $colegio->nombre, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('fecha_fundacion', 'Fecha_fundacion:') }}</td>
            <td>{{ Form::text('fecha_fundacion', $colegio->fecha_fundacion, array('class' => 'form-control')) }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('estado_id', 'Estado_id:') }}</td>
            <td>{{ Form::select('estado_id', $estados, $colegio->estado_id, array('class' => 'form-control')) }}</td>
        </tr>

		<tr>
			<td>
				{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
				{{ link_to_route('colegios.show', 'Cancel', $colegio->id, array('class' => 'btn')) }}
			</td>
		</tr>
	</table>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
