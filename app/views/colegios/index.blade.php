@extends('layouts.admin')

@section('main')

<h1>Colegios</h1>

<p>{{ link_to_route('colegios.create', 'Add new colegio') }}</p>

@if ($colegios->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Fecha de fundacion</th>
				<th>Estado</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($colegios as $colegio)
				<tr>
					<td><a href="/colegios/{{$colegio->id}}">{{{ $colegio->nombre }}}</a></td>
					<td>{{{ $colegio->fecha_fundacion }}}</td>
					<td>{{{ $colegio->estado()->first()->nombre }}}</td>
                    <td>{{ link_to_route('colegios.edit', 'Edit', array($colegio->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('colegios.destroy', $colegio->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no colegios
@endif

@stop
