@extends('layouts.admin')

@section('main')

<h1>Colegio: {{{ $colegio->nombre }}}</h1>

<p>{{ link_to_route('colegios.index', 'Return to all colegios') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Fecha de fundacion</th>
			<th>Estado</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $colegio->fecha_fundacion }}}</td>
			<td>{{{ $colegio->estado()->first()->nombre }}}</td>
            <td>{{ link_to_route('colegios.edit', 'Edit', array($colegio->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('colegios.destroy', $colegio->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

<h2>Arbitros</h2>
<table class="table">
	<thead>
		<th>Nombre</th>
		<th>Apellido</th>
	</thead>
	<tbody>
		@foreach($arbitros as $arbitro)
			<tr>
				<td><a href="/arbitros/{{$arbitro->id}}">{{{$arbitro->nombre}}}</a></td>
				<td><a href="/arbitros/{{$arbitro->id}}">{{{$arbitro->apellido}}}</a></td>
			</tr>
		@endforeach
	</tbody>
</table>

@stop
