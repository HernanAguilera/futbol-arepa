<?php

class PosicionesController extends BaseController {

	/**
	 * Posicion Repository
	 *
	 * @var Posicion
	 */
	protected $posicion;

	public function __construct(Posicion $posicion)
	{
		$this->posicion = $posicion;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posiciones = $this->posicion->all();

		return View::make('posiciones.index', compact('posiciones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('posiciones.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Posicion::$rules);

		if ($validation->passes())
		{
			$this->posicion->create($input);

			return Redirect::route('posiciones.index');
		}

		return Redirect::route('posiciones.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$posicion = $this->posicion->findOrFail($id);

		return View::make('posiciones.show', compact('posicion'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$posicion = $this->posicion->find($id);

		if (is_null($posicion))
		{
			return Redirect::route('posiciones.index');
		}

		return View::make('posiciones.edit', compact('posicion'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Posicion::$rules);

		if ($validation->passes())
		{
			$posicion = $this->posicion->find($id);
			$posicion->update($input);

			return Redirect::route('posiciones.show', $id);
		}

		return Redirect::route('posicionse.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->posicion->find($id)->delete();

		return Redirect::route('posiciones.index');
	}

}
