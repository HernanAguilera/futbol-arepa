<?php

class AuthController extends \BaseController {

	
	public function login(){
		
		$input = Input::all();

		$rules = array(
	        'username'					=> 'required',
	        'password'					=> 'required',
	        'recaptcha_response_field' 	=> 'required|recaptcha'
	    );

	    $validation = Validator::make($input, $rules);

	    if ($validation->passes()){

	    	$username = mb_strtolower(trim($input['username']));
			$password = $input['password'];
			$remenberme = isset($input['rememberme']) ? TRUE : FALSE;

			if (Auth::attempt(array('username' => $username, 'password' => $password), $remenberme))
	        {
	        	Session::put('username', $username);
	            return Redirect::to('/admin');
	        }

	        return Redirect::back()->with('msg', 'Datos incorrectos, vuelve a intentarlo.');
	    }
	    return Redirect::back()
	    	->withInput()
			->withErrors($validation)
			->with('msg', 'Campos rellenados de forma incorrecta.');
	}

	public function logout(){
		Session::flush();
		Auth::logout();
		return Redirect::to('/')->with('msg', 'Gracias por visitarnos!.');
	}
}
