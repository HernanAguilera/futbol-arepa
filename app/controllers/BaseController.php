<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected function upload_file(&$input, $field, $path_storage, $path_public, $thumbnail = FALSE, $width = NULL, $height = NULL){
		if(isset($input[$field]) && !is_null($input[$field])){
			if(Input::hasFile($field)){
					$input[$field]->move($path_storage, $input[$field]->getClientOriginalName());
					if($thumbnail && (!is_null($width) || !is_null($height))){
						// crea la carpeta para las miniaturas si no existe
						if (!file_exists($path_storage.'thumbnail')) {
						    mkdir($path_storage.'thumbnail', 0755, TRUE);
						}
						// genera la minuatura y la almacena en la carpeta thumbnail
						$imagen = Image::make($path_storage.$input[$field]->getClientOriginalName());
						if(!is_null($width) && !is_null($height)){
						}elseif (is_null($width) && !is_null($height)) {
							$imagen = $imagen->heighten($height);
						}else{
							$imagen = $imagen->widen($width);
						}
							$imagen->save($path_storage.'thumbnail/'.$input[$field]->getClientOriginalName());
					}
					
					$input[$field] = $path_public.$input[$field]->getClientOriginalName();
				}else
					unset($input[$field]);
		}elseif(is_null($input[$field]))
			unset($input[$field]);
	}

	protected function upload_file2(&$input, $field, $path_storage, $path_public){
		if(isset($input[$field]) && !is_null($input[$field])){
			if(Input::hasFile($field)){
				#	almacenar la imagen
				$input[$field]->move($path_storage, $input[$field]->getClientOriginalName());

				$path_image = $path_storage.$input[$field]->getClientOriginalName();
				$this->generateFormats(basename($path_image), $path_storage);

				$input[$field] = $path_public.$input[$field]->getClientOriginalName();
			}else
				unset($input[$field]);
		}elseif(is_null($input[$field]))
			unset($input[$field]);
	}

	protected function generateFormats($path_image, $path_storage){
		#	obtener los formatos de imagen
		$formatos_de_imagen = Config::get('images.formats');
		#	obtener el recurso de la imagen
		
		
		foreach ($formatos_de_imagen as $key => $formato) {
			Log::info('path de la imagen => '.$path_storage.$path_image);
			$imagen = Image::make($path_storage.$path_image);
			if (!file_exists($path_storage.$key)) {
			    mkdir($path_storage.$key, 0755, TRUE);
			}
			if(	   $formato['width']!='auto' 
				&& $formato['height']!='auto'
				&& $formato['width']<= $imagen->width()
				&& $formato['height']<= $imagen->height())

				$new_image = $imagen->resize($formato['width'], $formato['height']);

			elseif($formato['width']=='auto' 
				&& $formato['height']!='auto'
				&& $formato['height']<= $imagen->height())

				$new_image = $imagen->heighten($formato['height']);

			elseif($formato['width']!='auto' 
				&& $formato['height']=='auto'
				&& $formato['width']<= $imagen->width())

				$new_image = $imagen->widen($formato['width']);

			else
				$new_image = clone $imagen;

			$new_image->save($path_storage.$key.'/'.$path_image);
		}
	}

	protected function change_to_thumb(&$colection, $attribute){
		foreach ($colection as $key => $element) {
			$file_thumbnail = dirname($element[$attribute]).'/thumbnail/'.basename($element[$attribute]);
			if(is_file(public_path().$file_thumbnail))
				$element[$attribute] = $file_thumbnail;
		}
	}

	protected function addFormats(&$colection, $attribute){
		foreach ($colection as $element) {
			foreach (Config::get('images.formats') as $formatName => $format) {
				$file_format = dirname($element[$attribute]).'/'.$formatName.'/'.basename($element[$attribute]);
				if(is_file(public_path().$file_format))
					$element[$attribute.'_'.$formatName] = $file_format;
				else
					$element[$attribute.'_'.$formatName] = '';
			}
		}
	}

	protected static function startsWith($haystack, $needle){
	    return $needle === "" || strpos($haystack, $needle) === 0;
	}

	protected static function endsWith($haystack, $needle){
	    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}

	protected static function repeat_values($array){
		if(is_array($array) && count($array)>0){
			$values = array_values($array);
			for ($i=0; $i < count($values)-1; $i++) { 
				if(in_array($values[$i], array_slice($values, $i+1)))
					return TRUE;
			}
		}
		return FALSE;
	}

	protected static function to_view_datetime($datetime){
		$explode = explode(' ', $datetime);
		$date = $explode[0]; $time = explode(':', $explode[1]);
		return self::to_view_date($date)." $time[0]:$time[1]";
	}

	protected static function to_model_datetime($datetime){
		$explode = explode(' ', $datetime);
		$date = $explode[0]; $time = $explode[1];
		return self::to_model_date($date).' '.$time;
	}

	protected static function to_view_date($date){
		return implode('/', array_reverse(explode('-', $date)));
	}


	protected static function to_model_date($date){
		return implode('-', array_reverse(explode('/', $date)));
	}

}
