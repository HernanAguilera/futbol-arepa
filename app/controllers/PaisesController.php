<?php

class PaisesController extends BaseController {

	/**
	 * Pais Repository
	 *
	 * @var Pais
	 */
	protected $pais;

	public function __construct(Pais $pais)
	{
		$this->pais = $pais;

		$this->path_for_publicate_imagen = '/files/banderas/paises/';
		$this->path_for_storage_imagen = public_path().$this->path_for_publicate_imagen;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$paises = $this->pais->orderBy('nombre')->get();

		return View::make('paises.index', compact('paises'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('paises.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Pais::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('bandera')){
				$input['bandera']->move($this->path_for_storage_imagen, $input['bandera']->getClientOriginalName());
				$input['bandera'] = $this->path_for_publicate_imagen.$input['bandera']->getClientOriginalName();
			}else
				unset($input['bandera']);

			$this->pais->create($input);

			return Redirect::route('paises.index');
		}

		return Redirect::route('paises.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$pais = $this->pais->findOrFail($id);

		return View::make('paises.show', compact('pais'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pais = $this->pais->find($id);

		if (is_null($pais))
		{
			return Redirect::route('paises.index');
		}

		return View::make('paises.edit', compact('pais'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Pais::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('bandera')){
				$input['bandera']->move($this->path_for_storage_imagen, $input['bandera']->getClientOriginalName());
				$input['bandera'] = $this->path_for_publicate_imagen.$input['bandera']->getClientOriginalName();
			}else
				unset($input['bandera']);

			$pais = $this->pais->find($id);
			$pais->update($input);

			return Redirect::route('paises.show', $id);
		}

		return Redirect::route('paises.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->pais->find($id)->delete();

		return Redirect::route('paises.index');
	}

}
