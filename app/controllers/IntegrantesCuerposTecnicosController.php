<?php

class IntegrantesCuerposTecnicosController extends BaseController {

	/**
	 * IntegranteCuerpoTecnico Repository
	 *
	 * @var IntegranteCuerpoTecnico
	 */
	protected $integrante;
	protected $cargo;

	public function __construct(IntegranteCuerpoTecnico $integrante, 
								CargoCuerpoTecnico $cargo)
	{
		$this->integrante = $integrante;
		$this->cargo = $cargo;

		// CONFIGURACION DE PATHS PARA IMAGENES DE TRABJADORES DEL CUERPO TECNICO
		$this->path_for_publicate_avatar = '/files/integrantes-cuerpos-tecnicos/avatar/';
		$this->path_for_storage_avatar = public_path().$this->path_for_publicate_avatar;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$integrantes = $this->integrante->paginate(10);
		$this->change_to_thumb($integrantes, 'avatar');

		return View::make('integrantes_cuerpos_tecnicos.index', 
						  compact('integrantes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cargos = $this->cargo->lists('nombre', 'id');

		return View::make('integrantes_cuerpos_tecnicos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, IntegranteCuerpoTecnico::$rules);

		if ($validation->passes())
		{
			$this->upload_file($input,
							   'avatar', 
							   $this->path_for_storage_avatar,
							   $this->path_for_publicate_avatar,
							   TRUE, 80, NULL);

			$this->integrante->create($input);

			return Redirect::route('integrantes-cuerpos-tecnicos.index');
		}

		return Redirect::route('integrantes-cuerpos-tecnicos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$integrante = $this->integrante->findOrFail($id);

		return View::make('integrantes_cuerpos_tecnicos.show', compact('integrante'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$integrante = $this->integrante->find($id);

		if (is_null($integrante))
		{
			return Redirect::route('integrantes-cuerpos-tecnicos.index');
		}

		return View::make('integrantes_cuerpos_tecnicos.edit', compact('integrante'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, IntegranteCuerpoTecnico::$rules);

		if ($validation->passes())
		{
			Log::info('after', array('input => ' => $input));

			$this->upload_file($input,
							   'avatar', 
							   $this->path_for_storage_avatar,
							   $this->path_for_publicate_avatar,
							   TRUE, 80, NULL);

			Log::info('before', array('input => ' => $input));

			$integrante = $this->integrante->find($id);
			$integrante->update($input);

			return Redirect::route('integrantes-cuerpos-tecnicos.show', $id);
		}

		return Redirect::route('integrantes-cuerpos-tecnicos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->integrante->find($id)->delete();

		return Redirect::route('integrantes-cuerpos-tecnicos.index');
	}

}
