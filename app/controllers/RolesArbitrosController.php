<?php

class RolesArbitrosController extends BaseController {

	/**
	 * RolArbitro Repository
	 *
	 * @var RolArbitro
	 */
	protected $rol;

	public function __construct(RolArbitro $rol)
	{
		$this->rol = $rol;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = $this->rol->all();

		return View::make('roles_de_arbitros.index', compact('roles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('roles_de_arbitros.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, RolArbitro::$rules);

		if ($validation->passes())
		{
			$this->rol->create($input);

			return Redirect::route('roles-arbitros.index');
		}

		return Redirect::route('roles-arbitros.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rol = $this->rol->findOrFail($id);

		return View::make('roles_de_arbitros.show', compact('rol'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rol = $this->rol->find($id);

		if (is_null($rol))
		{
			return Redirect::route('roles-arbitros.index');
		}

		return View::make('roles_de_arbitros.edit', compact('rol'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, RolArbitro::$rules);

		if ($validation->passes())
		{
			$rol = $this->rol->find($id);
			$rol->update($input);

			return Redirect::route('roles-arbitros.show', $id);
		}

		return Redirect::route('roles-arbitros.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->rol->find($id)->delete();

		return Redirect::route('roles-arbitros.index');
	}

}
