<?php

class ConvocadosController extends BaseController {

	/**
	 * Convocado Repository
	 *
	 * @var Convocado
	 */
	protected $convocado;

	public function __construct(Convocado $convocado)
	{
		$this->convocado = $convocado;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$convocados = $this->convocado->all();

		return View::make('convocados.index', compact('convocados'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('convocados.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Convocado::$rules);

		if ($validation->passes())
		{
			$this->convocado->create($input);

			return Redirect::route('convocados.index');
		}

		return Redirect::route('convocados.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$convocado = $this->convocado->findOrFail($id);

		return View::make('convocados.show', compact('convocado'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$convocado = $this->convocado->find($id);

		if (is_null($convocado))
		{
			return Redirect::route('convocados.index');
		}

		return View::make('convocados.edit', compact('convocado'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Convocado::$rules);

		if ($validation->passes())
		{
			$convocado = $this->convocado->find($id);
			$convocado->update($input);

			return Redirect::route('convocados.show', $id);
		}

		return Redirect::route('convocados.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->convocado->find($id)->delete();

		return Redirect::route('convocados.index');
	}

}
