<?php

class EstadosController extends BaseController {

	/**
	 * Estado Repository
	 *
	 * @var Estado
	 */
	protected $estado;

	public function __construct(Estado $estado, Pais $pais)
	{
		$this->estado = $estado;
		$this->pais   = $pais;

		$this->path_for_publicate_imagen = '/files/banderas/estados/';
		$this->path_for_storage_imagen = public_path().$this->path_for_publicate_imagen;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$estados = $this->estado->orderBy('nombre')->paginate(10);
		$paises = $this->pais->lists('nombre','id');
		$estados->count = Cache::rememberForever('count_estados', 
							function(){
					    		return $this->estado->orderBy('nombre')->get()->count();
							});
		$this->change_to_thumb($estados, 'bandera');
		
		return View::make('estados.index', compact('estados', 'paises'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$paises = $this->pais->lists('nombre','id');
		return View::make('estados.create', compact('paises'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Estado::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('bandera')){
				// mueve la imagen al directorio destinado para su almacenamiento
				$input['bandera']->move($this->path_for_storage_imagen, $input['bandera']->getClientOriginalName());
				// crea la carpeta para las miniaturas si no existe
				if (!file_exists($this->path_for_storage_imagen.'thumbnail')) {
				    mkdir($this->path_for_storage_imagen.'thumbnail', 0755);
				}
				// genera la minuatura y la almacena en la carpeta thumbnail
				Image::make($this->path_for_storage_imagen.$input['bandera']->getClientOriginalName())
						->heighten(80)
						->save($this->path_for_storage_imagen.'thumbnail/'.$input['bandera']->getClientOriginalName());
				// asigna la ruta para la imagen para que sea almacenada en la bd						
				$input['bandera'] = $this->path_for_publicate_imagen.$input['bandera']->getClientOriginalName();
			}else
				unset($input['bandera']);

			$this->estado->create($input);

			Cache::forever('count_estados', $this->estado->orderBy('nombre')->get()->count());

			return Redirect::route('estados.index');
		}

		return Redirect::route('estados.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$estado = $this->estado->findOrFail($id);

		return View::make('estados.show', compact('estado'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$estado = $this->estado->find($id);

		if (is_null($estado))
		{
			return Redirect::route('estados.index');
		}
		$paises = $this->pais->lists('nombre','id');
		return View::make('estados.edit', compact('estado', 'paises'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Estado::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('bandera')){
				// mueve la imagen al directorio destinado para su almacenamiento
				$input['bandera']->move($this->path_for_storage_imagen, $input['bandera']->getClientOriginalName());
				// crea la carpeta para las miniaturas si no existe
				if (!file_exists($this->path_for_storage_imagen.'thumbnail')) {
				    mkdir($this->path_for_storage_imagen.'thumbnail', 0755);
				}
				// genera la minuatura y la almacena en la carpeta thumbnail
				Image::make($this->path_for_storage_imagen.$input['bandera']->getClientOriginalName())
						->heighten(80)
						->save($this->path_for_storage_imagen.'thumbnail/'.$input['bandera']->getClientOriginalName());
				// asigna la ruta para la imagen para que sea almacenada en la bd						
				$input['bandera'] = $this->path_for_publicate_imagen.$input['bandera']->getClientOriginalName();
			}else
				unset($input['bandera']);

			$estado = $this->estado->find($id);
			$estado->update($input);

			return Redirect::route('estados.show', $id);
		}

		return Redirect::route('estados.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->estado->find($id)->delete();

		Cache::forever('count_estados', $this->estado->orderBy('nombre')->get()->count());

		return Redirect::route('estados.index');
	}

}
