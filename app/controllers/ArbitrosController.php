<?php

class ArbitrosController extends BaseController {

	/**
	 * Arbitro Repository
	 *
	 * @var Arbitro
	 */
	protected $arbitro;
	protected $colegio;

	public function __construct(Arbitro $arbitro, Colegio $colegio)
	{
		$this->arbitro = $arbitro;
		$this->colegio = $colegio;
		
		$this->path_for_publicate_avatar = '/files/arbitros/avatar/';
		$this->path_for_storage_avatar = public_path().$this->path_for_publicate_avatar;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$arbitros = $this->arbitro->orderBy('nombre')
									->orderBy('apellido')
									->get();
		$colegios = $this->colegio->lists('nombre', 'id');

		return View::make('arbitros.index', compact('arbitros', 'colegios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$colegios = $this->colegio->orderBy('nombre')->lists('nombre', 'id');

		return View::make('arbitros.create', compact('colegios'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Arbitro::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('avatar')){
				$input['avatar']->move($this->path_for_storage_avatar, $input['avatar']->getClientOriginalName());
				$input['avatar'] = $this->path_for_publicate_avatar.$input['avatar']->getClientOriginalName();
			}else
				unset($input['avatar']);

			$this->arbitro->create($input);

			return Redirect::route('arbitros.index');
		}

		return Redirect::route('arbitros.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$arbitro = $this->arbitro->findOrFail($id);
		$colegios = $this->colegio->lists('nombre', 'id');

		return View::make('arbitros.show', compact('arbitro', 'colegios'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$arbitro = $this->arbitro->find($id);

		if (is_null($arbitro))
		{
			return Redirect::route('arbitros.index');
		}
		$colegios = $this->colegio->orderBy('nombre')->lists('nombre', 'id');

		return View::make('arbitros.edit', compact('arbitro', 'colegios'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Arbitro::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('avatar')){
				$input['avatar']->move($this->path_for_storage_avatar, $input['avatar']->getClientOriginalName());
				$input['avatar'] = $this->path_for_publicate_avatar.$input['avatar']->getClientOriginalName();
			}else
				unset($input['avatar']);

			$arbitro = $this->arbitro->find($id);
			$arbitro->update($input);

			return Redirect::route('arbitros.show', $id);
		}

		return Redirect::route('arbitros.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->arbitro->find($id)->delete();

		return Redirect::route('arbitros.index');
	}

}
