<?php

class CiudadesController extends BaseController {

	/**
	 * Ciudad Repository
	 *
	 * @var Ciudad
	 */
	protected $ciudad;

	public function __construct(Ciudad $ciudad, Estado $estado)
	{
		$this->ciudad = $ciudad;
		$this->estado = $estado;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ciudades = $this->ciudad->orderBy('nombre')->get();
		$estados = $this->estado->lists('nombre', 'id');

		return View::make('ciudades.index', compact('ciudades', 'estados'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$estados = $this->estado->lists('nombre', 'id');

		return View::make('ciudades.create', compact('estados'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Ciudad::$rules);

		if ($validation->passes())
		{
			$this->ciudad->create($input);

			return Redirect::route('ciudades.index');
		}

		return Redirect::route('ciudades.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ciudad = $this->ciudad->findOrFail($id);
		$estados = $this->estado->lists('nombre', 'id');

		return View::make('ciudades.show', compact('ciudad', 'estados'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ciudad = $this->ciudad->find($id);

		if (is_null($ciudad))
		{
			return Redirect::route('ciudades.index');
		}
		$estados = $this->estado->lists('nombre', 'id');
		return View::make('ciudades.edit', compact('ciudad', 'estados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Ciudad::$rules);

		if ($validation->passes())
		{
			$ciudad = $this->ciudad->find($id);
			$ciudad->update($input);

			return Redirect::route('ciudades.show', $id);
		}

		return Redirect::route('ciudades.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ciudad->find($id)->delete();

		return Redirect::route('ciudades.index');
	}

}
