<?php

class GaleriasEstadiosController extends BaseController {

	/**
	 * GaleriaEstadio Repository
	 *
	 * @var GaleriaEstadio
	 */
	protected $galeria;

	public function __construct(GaleriaEstadio $galeria, Estadio $estadio)
	{
		$this->galeria = $galeria;
		$this->estadio = $estadio;

		$this->path_for_publicate_imagen = '/files/galerias/estadios/';
		$this->path_for_storage_imagen = public_path().$this->path_for_publicate_imagen;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$galerias = $this->galeria->all();
		$estadios = $this->estadio->lists('nombre', 'id');

		return View::make('galerias_de_estadios.index', compact('galerias', 'estadios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$estadios = $this->estadio->lists('nombre', 'id');

		return View::make('galerias_de_estadios.create', compact('estadios'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, GaleriaEstadio::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('imagen')){
				$input['imagen']->move($this->path_for_storage_imagen, $input['imagen']->getClientOriginalName());
				$input['imagen'] = $this->path_for_publicate_imagen.$input['imagen']->getClientOriginalName();
			}else
				unset($input['imagen']);

			$this->galeria->create($input);

			return Redirect::route('galerias.estadios.index');
		}

		return Redirect::route('galerias.estadios.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$galeria = $this->galeria->findOrFail($id);
		$estadios = $this->estadio->lists('nombre', 'id');

		return View::make('galerias_de_estadios.show', compact('galeria', 'estadios'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$galeria = $this->galeria->find($id);

		if (is_null($galeria))
		{
			return Redirect::route('galerias_de_estadios.index');
		}
		$estadios = $this->estadio->lists('nombre', 'id');

		return View::make('galerias_de_estadios.edit', compact('galeria', 'estadios'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, GaleriaEstadio::$rules);

		if ($validation->passes())
		{
			if(Input::hasFile('imagen')){
				$input['imagen']->move($this->path_for_storage_imagen, $input['imagen']->getClientOriginalName());
				$input['imagen'] = $this->path_for_publicate_imagen.$input['imagen']->getClientOriginalName();
			}else
				unset($input['imagen']);

			$galeria = $this->galeria->find($id);
			$galeria->update($input);

			return Redirect::route('galerias_de_estadios.show', $id);
		}

		return Redirect::route('galerias_de_estadios.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->galeria->find($id)->delete();

		return Redirect::route('galerias_de_estadios.index');
	}

}
