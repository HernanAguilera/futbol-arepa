<?php

class ColegiosController extends BaseController {

	/**
	 * Colegio Repository
	 *
	 * @var Colegio
	 */
	protected $colegio;
	protected $estado;

	public function __construct(Colegio $colegio, Estado $estado)
	{
		$this->colegio = $colegio;
		$this->estado = $estado;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$colegios = $this->colegio->orderBy('nombre')->get();

		return View::make('colegios.index', compact('colegios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$estados = $this->estado->orderBy('nombre')->lists('nombre', 'id');

		return View::make('colegios.create', compact('estados'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Colegio::$rules);

		if ($validation->passes())
		{
			$this->colegio->create($input);

			return Redirect::route('colegios.index');
		}

		return Redirect::route('colegios.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$colegio = $this->colegio->findOrFail($id);
		$arbitros = $colegio->arbitros()->get();

		return View::make('colegios.show', compact('colegio', 'arbitros'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$colegio = $this->colegio->find($id);

		if (is_null($colegio))
		{
			return Redirect::route('colegios.index');
		}
		$estados = $this->estado->orderBy('nombre')->lists('nombre', 'id');

		return View::make('colegios.edit', compact('colegio', 'estados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Colegio::$rules);

		if ($validation->passes())
		{
			$colegio = $this->colegio->find($id);
			$colegio->update($input);

			return Redirect::route('colegios.show', $id);
		}

		return Redirect::route('colegios.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->colegio->find($id)->delete();

		return Redirect::route('colegios.index');
	}

}
