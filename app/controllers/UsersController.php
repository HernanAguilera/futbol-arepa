<?php

class UsersController extends BaseController{

	/**
	 * User Repository
	 *
	 * @var User
	 */
	protected $user;
	protected $rol;

	public function __construct(User $user, Rol $rol)
	{
		$this->user = $user;
		$this->rol = $rol;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->user->paginate();

		return View::make('users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$rols = $this->rol->lists('title', 'id');
		return View::make('users.create', compact('rols'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$rules = array(
			'username'			=> 'required',
			'email'				=> 'required|email',
			'rol_id'			=> 'required|exists:rols,id',
			'password'			=> 'required|min:6',
			'password-repeat'	=> 'same:password'
		);

		$validation = Validator::make($input, $rules);

		if ($validation->passes())
		{
			$propiedades = array(
				'username' 	=> $input['username'],
				'email' 	=> $input['email'],
				'rol_id' 	=> $input['rol_id'],
				'password'	=> Hash::make($input['password'])
			);
			Log::info($propiedades);
			$this->user->create($input);

			return Redirect::route('admin.users.index');
		}

		return Redirect::route('admin.users.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = $this->user->findOrFail($id);

		return View::make('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->user->find($id);

		if (is_null($user))
		{
			return Redirect::route('admin.users.index');
		}

		$rols = $this->rol->lists('title', 'id');
		return View::make('users.edit', compact('user', 'rols'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');

		// Lista de campos requeridos
		$rules = array(
			'username'			=> 'required',
			'email'				=> 'required|email',
			'rol'				=> 'required|exists:rols,id',
			'password'			=> 'same:password-repeat|min:6',
			'password-repeat'	=> 'same:password'
		);

		$validation = Validator::make($input, $rules);

		if ($validation->passes())
		{
			$usuario = $this->user->find($id);
			$input['email'] = mb_strtolower(trim($input['email']));
			
			if($usuario->username != $input['username']) 		$usuario->username = $input['username'];
			if($usuario->email != $input['email']) 		$usuario->email = $input['email'];
			if($usuario->rol_id != $input['rol']) 		$usuario->rol_id = $input['rol'];
			if(!empty($input['password'])) 				$usuario->password = Hash::make($input['password']);
			
			$usuario->save();

			return Redirect::route('admin.users.show', $id);
		}

		return Redirect::route('admin.users.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->user->find($id)->delete();

		return Redirect::route('admin.users.index');
	}


}