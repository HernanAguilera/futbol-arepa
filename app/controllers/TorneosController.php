<?php

class TorneosController extends BaseController {

	/**
	 * Torneo Repository
	 *
	 * @var Torneo
	 */
	protected $torneo;

	public function __construct(Torneo $torneo, 
								Temporada $temporada, 
								Categoria $categoria,
								Club $club)
	{
		$this->torneo = $torneo;
		$this->temporada = $temporada;
		$this->categoria = $categoria;
		$this->club = $club;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$torneos = $this->torneo->all();
		$temporadas = $this->temporada->lists('nombre', 'id');
		$categorias = $this->categoria->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');

		return View::make('torneos.index', 
						  compact('torneos', 
						  		  'temporadas',
						  		  'categorias', 
						  		  'clubes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$temporadas = $this->temporada->lists('nombre', 'id');
		$categorias = $this->categoria->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');

		return View::make('torneos.create', 
						  compact('temporadas', 
						  		  'categorias',
						  		  'clubes'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Torneo::$rules);

		if ($validation->passes())
		{
			if(isset($input['clubes'])){
				$clubes = $input['clubes'];
				unset($input['clubes']);
			}

			$input['inicio'] = self::to_model_date($input['inicio']);
			$input['fin'] = self::to_model_date($input['fin']);

			$torneo = $this->torneo->create($input);

			if(isset($clubes)) $torneo->clubes()->sync($clubes);

			return Redirect::route('torneos.index');
		}

		return Redirect::route('torneos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$torneo = $this->torneo->findOrFail($id);
		$clubes = $torneo->clubes()->orderBy('nombre')->get();
		$fechas = $torneo->fechas()->get();
		$tabla = $torneo->posiciones();
		$this->change_to_thumb($clubes, 'escudo');

		foreach ($tabla as $key => $grupo) {
			$this->change_to_thumb($grupo, 'escudo');
		}

		return View::make('torneos.show', 
						  compact('torneo', 
						  		  'clubes',
						  		  'fechas',
						  		  'tabla'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$torneo = $this->torneo->find($id);

		if (is_null($torneo))
		{
			return Redirect::route('torneos.index');
		}
		$temporadas = $this->temporada->lists('nombre', 'id');
		$categorias = $this->categoria->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');
		$torneo->clubes = $torneo->clubes()->lists('id');

		$torneo->inicio = self::to_view_date($torneo->inicio);
		$torneo->fin = self::to_view_date($torneo->fin);

		return View::make('torneos.edit', 
						  compact('torneo', 
						  		  'temporadas', 
						  		  'categorias',
						  		  'clubes'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Torneo::$rules);

		if ($validation->passes())
		{
			if(isset($input['clubes'])){
				$clubes = $input['clubes'];
				unset($input['clubes']);
			}

			$input['inicio'] = self::to_model_date($input['inicio']);
			$input['fin'] = self::to_model_date($input['fin']);

			$torneo = $this->torneo->find($id);
			$torneo->update($input);

			if(isset($clubes)) 
				$torneo->clubes()->sync($clubes);
			else
				$torneo->clubes()->sync(array());

			return Redirect::route('torneos.show', $id);
		}

		return Redirect::route('torneos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->torneo->find($id)->delete();

		return Redirect::route('torneos.index');
	}

}
