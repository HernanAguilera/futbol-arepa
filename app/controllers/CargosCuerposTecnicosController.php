<?php

class CargosCuerposTecnicosController extends BaseController {

	/**
	 * CargoCuerpoTecnico Repository
	 *
	 * @var CargoCuerpoTecnico
	 */
	protected $cargos_cuerpos_tecnico;

	public function __construct(CargoCuerpoTecnico $cargo)
	{
		$this->cargo = $cargo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cargos = $this->cargo->all();

		return View::make('cargos_cuerpos_tecnicos.index', compact('cargos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('cargos_cuerpos_tecnicos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, CargoCuerpoTecnico::$rules);

		if ($validation->passes())
		{
			$this->cargo->create($input);

			return Redirect::route('cargos-cuerpos-tecnicos.index');
		}

		return Redirect::route('cargos-cuerpos-tecnicos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$cargo = $this->cargo->findOrFail($id);

		return View::make('cargos_cuerpos_tecnicos.show', compact('cargo'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cargo = $this->cargo->find($id);

		if (is_null($cargo))
		{
			return Redirect::route('cargos-cuerpos-tecnicos.index');
		}

		return View::make('cargos_cuerpos_tecnicos.edit', compact('cargo'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, CargoCuerpoTecnico::$rules);

		if ($validation->passes())
		{
			$cargo = $this->cargo->find($id);
			$cargo->update($input);

			return Redirect::route('cargos-cuerpos-tecnicos.show', $id);
		}

		return Redirect::route('cargos-cuerpos-tecnicos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->cargo->find($id)->delete();

		return Redirect::route('cargos-cuerpos-tecnicos.index');
	}

}
