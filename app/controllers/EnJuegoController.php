<?php

class EnJuegoController extends \BaseController {

	protected $partido;
	protected $estadio;
	protected $club;
	protected $tipos;
	protected $futbolista;

	public function __construct(Partido $partido,
								Estadio $estadio,
								Club $club,
								TipoDeGol $tipo,
								Futbolista $futbolista){
		$this->partido = $partido;
		$this->estadio = $estadio;
		$this->club = $club;
		$this->tipo = $tipo;
		$this->futbolista = $futbolista;
	}
	
	public function partido($id){

		$detalles = $this->partido->detalles($id);
		
		$detalles['partido']->fecha_calendario = $this->to_view_datetime($detalles['partido']->fecha_calendario);

		return View::make('enjuego.partido', $detalles);
	}


}
