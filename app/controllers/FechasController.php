<?php

class FechasController extends BaseController {

	/**
	 * Fecha Repository
	 *
	 * @var Fecha
	 */
	protected $fecha;
	protected $torneo;
	protected $grupo;

	public function __construct(Fecha $fecha, Torneo $torneo, Grupo $grupo)
	{
		$this->fecha = $fecha;
		$this->torneo = $torneo;
		$this->grupo = $grupo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$fechas = $this->fecha->orderBy('torneo_id')
								->get();

		return View::make('fechas.index', compact('fechas', 'torneos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$torneos = $this->torneo->lists('nombre', 'id');

		return View::make('fechas.create', compact('torneos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Fecha::$rules);

		if ($validation->passes())
		{
			if(!isset($input['visible'])) $input['visible'] = 0;
			if(!isset($input['activa'])) $input['activa'] = 0;

			if(isset($input['actual'])){
				$this->fecha->where('actual', '1')
							->where('torneo_id', $input['torneo_id'])
							->update(array('actual' => '0'));
			}
			else
				$input['actual'] = 0;

			if(isset($input['proxima'])){
				$this->fecha->where('proxima', '1')
							->where('torneo_id', $input['torneo_id'])
							->update(array('proxima' => '0'));
			}
			else
				$input['proxima'] = 0;

			$grupos = FALSE;
			if(isset($input['grupos'])){
				unset($input['grupos']);
				$grupos = TRUE;
			}

			$fecha = $this->fecha->create($input);

			if($grupos) $fecha->grupos()->sync($fecha->torneo()->first()->grupos()->lists('id'));

			return Redirect::route('fechas.index');
		}

		return Redirect::route('fechas.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$fecha = $this->fecha->findOrFail($id);
		$torneos = $this->torneo->lists('nombre', 'id');

		return View::make('fechas.show', compact('fecha', 'torneos'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$fecha = $this->fecha->find($id);

		if (is_null($fecha))
		{
			return Redirect::route('fechas.index');
		}
		$torneos = $this->torneo->lists('nombre', 'id');

		$grupos = $fecha->grupos()->get()->count()>0;

		return View::make('fechas.edit', compact('fecha', 'torneos', 'grupos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Fecha::$rules);

		if ($validation->passes())
		{
			if(!isset($input['visible'])) $input['visible'] = 0;
			if(!isset($input['activa'])) $input['activa'] = 0;

			if(isset($input['actual']))
				$this->fecha->where('actual', '1')
							->where('torneo_id', $input['torneo_id'])
							->update(array('actual' => '0'));
			else
				$input['actual'] = '0';

			if(isset($input['proxima']))
				$this->fecha->where('proxima', '1')
							->where('torneo_id', $input['torneo_id'])
							->update(array('proxima' => '0'));
			else
				$input['proxima'] = '0';

			$grupos = FALSE;
			if(isset($input['grupos'])){
				unset($input['grupos']);
				$grupos = TRUE;
			}
			
			$fecha = $this->fecha->find($id);
			$fecha->update($input);

			if($grupos)
				if($grupos) $fecha->grupos()->sync($fecha->torneo()->first()->grupos()->lists('id'));
			else
				$fecha->grupos()->sync(array());

			$fecha->actualizarCache();

			return Redirect::route('fechas.show', $id);
		}

		return Redirect::route('fechas.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->fecha->find($id)->delete();

		return Redirect::route('fechas.index');
	}


	public function busquedaPorIdTorneo($torneo_id){
		$fechas = $this->fecha->where('torneo_id', $torneo_id)->select('id', 'jornada')->get();
		return Response::json(array('fechas' => $fechas));
	}

}
