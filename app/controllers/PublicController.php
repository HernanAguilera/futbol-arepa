<?php 

	/**
	* Class PublicController
	*/
	class PublicController extends \BaseController
	{
		protected $torneo;

		function __construct(Torneo $torneo)
		{
			$this->torneo = $torneo;
		}


		public function index(){

			$titleHeader = 'Fútbol Arepa';

			$ultimosResultados = Cache::rememberForever('ultimos_partidos_all', function(){
				return Torneo::ultimosResultadosAll();
			}); 
			$proximosPartidos = Cache::rememberForever('proximos_partidos_all', function(){
				return Torneo::proximosPartidosAll();
			});

			return View::make('public.landing', 
							  compact('ultimosResultados',
							  		  'proximosPartidos',
							  		  'titleHeader')
						);
		}

		/**
		 *		PRIMERA DIVISIÓN
		 */

		public function primera(){
			return $this->torneoCache(Config::get('public_views.primera')
								,'public.primera-division.tabla'
								,'Primera División');
		}

		public function primeraFechas(){
			return $this->torneoFechasCache(Config::get('public_views.primera')
								,'public.primera-division.fechas'
								,'Primera División');

			
		}

		public function primeraFecha($fecha_id){
			return $this->torneoFechaCache(Config::get('public_views.primera')
								,'public.primera-division.fechas'
								,$fecha_id
								,'Primera División');
		}

		/**
		 *		SEGUNDA DIVISIÓN
		 */

		public function segunda(){
			return $this->torneoCache(Config::get('public_views.segunda')
								,'public.segunda-division.tabla'
								,'Segunda División');
			
		}

		public function segundaFechas(){
			return $this->torneoFechasCache(Config::get('public_views.segunda')
								,'public.segunda-division.fechas'
								,'Segunda División');
		}

		public function segundaFecha($fecha_id){
			return $this->torneoFechaCache(Config::get('public_views.segunda')
								,'public.segunda-division.fechas'
								,$fecha_id
								,'Segunda División');
		}

		/**
		 *		TERCERA DIVISIÓN
		 */

		public function tercera(){
			return $this->torneoCache(Config::get('public_views.tercera')
								,'public.tercera-division.tabla'
								,'Tercera División');
		}

		public function terceraFechas(){
			return $this->torneoFechasCache(Config::get('public_views.tercera')
								,'public.tercera-division.fechas'
								,'Tercera División');
		}

		public function terceraFecha($fecha_id){
			return $this->torneoFechaCache(Config::get('public_views.tercera')
								,'public.tercera-division.fechas'
								,$fecha_id
								,'Tercera División');
		}

		/**
		 *		COPA VENEZUELA
		 */

		public function copaFechas(){
			return $this->torneoFechasCache(Config::get('public_views.copa')
								,'public.copa.fechas'
								,'Copa Venezuela');

		}

		public function copaFecha($fecha_id){
			return $this->torneoFechaCache(Config::get('public_views.copa')
								,'public.copa.fechas'
								,$fecha_id
								,'Copa Venezuela');
		}

		/**
		 *		CONTACTO
		 */

		public function contacto(){

			$input = Input::all();

			$rules = array (
				'nombre' => 'required',
				'correo' => 'required|email',
				'asunto' => 'required',
				'mensaje' => 'required|min:25'
			);

			if(!Session::has('username'))
				$rules['recaptcha_response_field'] = 'required|recaptcha';

			$validator = Validator::make ($input, $rules);

			if($validator->passes()){

				$titleHeader = 'Contacto';

				$input['datetime'] = date("F j, Y, g:i a");
				$input['userIpAddress'] = Request::getClientIp();
				Mail::send('emails.contacto', $input, function($message) use ($input)
					{
						//email 'From' field: Get users email add and name
						$message->from($input['correo'] , $input['nombre']);
						//email 'To' field: cahnge this to emails that you want to be notified.
						$message->to('hernan3407@gmail.com', 'Hernán Aguilera')->subject($input['asunto']);

					});

				return View::make('public.contacto', compact('titleHeader'));

			}else{
				return Redirect::back()->withErrors($validator);
			}

		}


		/**
		 *
		 */

		private function torneoCache($torneo_id, $template, $titleHeader){
			$titleHeader = 'Clasificación - '.$titleHeader;

			$torneo =  Cache::rememberForever('torneo'.$torneo_id, function() use ($torneo_id){
				return $this->torneo->findOrFail($torneo_id);
			});
			// tabla(torneo_id)
			$tabla = Cache::rememberForever('tabla'.$torneo_id, function() use ($torneo){
				return $torneo->posiciones();
			});
			// ultimos_resultados(torneo_id)
			$fecha = Cache::rememberForever('ultimos_resultados'.$torneo_id, function() use ($torneo){
				return $torneo->ultimosResultados();
			});
			// goleadores(torneo_id)
			$goleadores = Cache::rememberForever('goleadores'.$torneo_id, function() use ($torneo){
				return $torneo->goleadores();
			});
			return View::make($template, 
							  compact('tabla',
							  		  'fecha',
							  		  'goleadores',
							  		  'titleHeader'));
		}

		private function torneoFechasCache($torneo_id, $template, $titleHeader){
			$torneo =  Cache::rememberForever('torneo'.$torneo_id, function() use ($torneo_id){
				return $this->torneo->findOrFail($torneo_id);
			});
			// fecha_actual(torneo_id)
			$fechaActual = Cache::rememberForever('fecha_actual'.$torneo_id, function() use ($torneo){
				return $torneo->fechas()->where('actual','1')->first();
			});
			$titleHeader = $fechaActual->jornada.' - '.$titleHeader;

			// fechas(torneo_id)
			$fechas = Cache::rememberForever('fechas'.$torneo_id, function() use ($torneo){
				return $torneo->fechas()->where('visible', TRUE)->lists('abreviatura', 'id');
			});
			// ultimos_resultados(torneo_id)
			$fecha = Cache::rememberForever('ultimos_resultados'.$torneo_id, function() use ($torneo){
				return $torneo->ultimosResultados();
			});
			// goleadores(torneo_id)
			$goleadores = Cache::rememberForever('goleadores'.$torneo_id, function() use ($torneo){
				return $torneo->goleadores();
			});

			$fechaActual_id = $fechaActual->id;
			$fechaActiva_id = $fechaActual_id;
			return View::make($template, 
							  compact('fecha', 
							  		  'fechas', 
							  		  'fechaActiva_id',
							  		  'fechaActual_id',
							  		  'goleadores',
							  		  'titleHeader'));
		}

		private function torneoFechaCache($torneo_id, $template, $fecha_id, $titleHeader){
			$torneo =  Cache::rememberForever('torneo'.$torneo_id, function() use ($torneo_id){
				return $this->torneo->findOrFail($torneo_id);
			});

			$fechaActiva = $torneo->fechas()->find($fecha_id);

			if(empty($fechaActiva))
				App::abort(404);
			$titleHeader = $fechaActiva->jornada.' - '.$titleHeader;

			// fechas(torneo_id)
			$fechas = Cache::rememberForever('fechas'.$torneo_id, function() use ($torneo){
				return $torneo->fechas()->where('visible', TRUE)->lists('abreviatura', 'id');
			});
			$fechaActiva_id = $fecha_id;
			// fecha_actual(torneo_id)
			$fechaActual = Cache::rememberForever('fecha_actual'.$torneo_id, function() use ($torneo){
				return $torneo->fechas()->where('actual','1')->first();
			});
			// fecha(torneo_id)
			$fecha = Cache::rememberForever('fecha'.$fecha_id, function() use ($torneo, $fecha_id){
				return $torneo->resumenFecha($torneo->fechas()->find($fecha_id));
			});
			// goleadores(torneo_id)
			$goleadores = Cache::rememberForever('goleadores'.$torneo_id, function() use ($torneo){
				return $torneo->goleadores();
			});

			$fechaActual_id = $fechaActual->id;
			return View::make($template, 
							  compact('fecha', 
							  		  'fechas', 
							  		  'fechaActiva_id',
							  		  'fechaActual_id',
							  		  'goleadores',
							  		  'titleHeader'));
		}


	}