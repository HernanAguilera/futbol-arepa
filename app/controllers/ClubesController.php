<?php

class ClubesController extends BaseController {

	/**
	 * Club Repository
	 *
	 * @var Club
	 */
	protected $club;
	protected $torneo;
	protected $estado;
	protected $estadio;
	protected $grupo;

	public function __construct(Club $club, 
								Torneo $torneo,
								Estado $estado,
								Estadio $estadio,
								Grupo $grupo)
	{
		$this->club = $club;
		$this->torneo = $torneo;
		$this->estado = $estado;
		$this->estadio = $estadio;
		$this->grupo = $grupo;

		// CONFIGURACION DE PATHS PARA ESCUDOS DE CLUBES
		$this->path_for_publicate_escudo = '/files/clubes/escudos/';
		$this->path_for_storage_escudo = public_path().$this->path_for_publicate_escudo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clubes = $this->club->orderBy('nombre')->paginate(10);
		$torneos = $this->torneo->lists('nombre', 'id');
		$estados = $this->estado->lists('nombre', 'id');
		$estadios = $this->estadio->lists('nombre', 'id');

		$this->addFormats($clubes, 'escudo');

		return View::make('clubes.index', 
						  compact('clubes', 
						  		  'torneos',
						  		  'estados',
						  		  'estadios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$torneos = $this->torneo->lists('nombre', 'id');
		$estados = $this->estado->lists('nombre', 'id');
		$estadios = $this->estadio->lists('nombre', 'id');
		$grupos = $this->grupo->lists('nombre', 'id');

		return View::make('clubes.create', 
						compact('torneos', 
								'estados', 
								'estadios', 
								'grupos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Club::$rules);

		if ($validation->passes())
		{
			if(isset($input['torneos'])){
				$torneos = $input['torneos'];
				unset($input['torneos']);
			}

			if(isset($input['grupos'])){
				$grupos = $input['grupos'];
				unset($input['grupos']);
			}

			$this->upload_file2($input, 'escudo', $this->path_for_storage_escudo, $this->path_for_publicate_escudo);
			// if(Input::hasFile('escudo')){
			// 	$input['escudo']->move($this->path_for_storage_escudo, $input['escudo']->getClientOriginalName());
			// 	$input['escudo'] = $this->path_for_publicate_escudo.$input['escudo']->getClientOriginalName();
			// }else
			// 	unset($input['escudo']);

			$input['fundacion'] = self::to_model_date($input['fundacion']);

			$club = $this->club->create($input);

			if(isset($torneos)) $club->torneos()->sync($torneos);
			if(isset($grupos)) $club->grupos()->sync($grupos);

			return Redirect::route('clubes.index');
		}

		return Redirect::route('clubes.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$club = $this->club->findOrFail($id);
		

		$torneos_del_club = array(0 => '--- Torneos ---') + $club->torneos()->lists('nombre', 'id');

		return View::make('clubes.show', 
						  compact('club', 
						  		  
						  		  'estados',
						  		  'torneos_del_club'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$club = $this->club->find($id);

		if (is_null($club))
		{
			return Redirect::route('clubes.index');
		}
		$torneos = $this->torneo->lists('nombre', 'id');
		$estados = $this->estado->lists('nombre', 'id');
		$estadios = $this->estadio->lists('nombre', 'id');
		$grupos = $this->grupo->lists('nombre', 'id');

		$club->fundacion = self::to_view_date($club->fundacion);

		$club->torneos = $club->torneos()->lists('id');
		$club->grupos = $club->grupos()->lists('id');

		return View::make('clubes.edit', 
						  compact('club', 
						  		  'torneos',
						  		  'estados',
						  		  'estadios',
						  		  'grupos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Club::$rules);

		if ($validation->passes())
		{
			if(isset($input['torneos'])){
				$torneos = $input['torneos'];
				unset($input['torneos']);
			}

			if(isset($input['grupos'])){
				$grupos = $input['grupos'];
				unset($input['grupos']);
			}

			$this->upload_file2($input, 'escudo', $this->path_for_storage_escudo, $this->path_for_publicate_escudo);
			// if(Input::hasFile('escudo')){
			// 	$input['escudo']->move($this->path_for_storage_escudo, $input['escudo']->getClientOriginalName());
			// 	$input['escudo'] = $this->path_for_publicate_escudo.$input['escudo']->getClientOriginalName();
			// }else
			// 	unset($input['escudo']);

			$input['fundacion'] = self::to_model_date($input['fundacion']);

			$club = $this->club->find($id);
			$club->update($input);

			if(isset($torneos)) 
				$club->torneos()->sync($torneos);
			else
				$club->torneos()->sync(array());

			if(isset($grupos)) 
				$club->grupos()->sync($grupos);
			else
				$club->grupos()->sync(array());

			return Redirect::route('clubes.show', $id);
		}

		return Redirect::route('clubes.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->club->find($id)->delete();

		return Redirect::route('clubes.index');
	}


	public function generateAllFormatFile(){
		$clubes = $this->club->all();

		foreach ($clubes as $key => $club) {
			$this->generateFormats(basename($club->escudo), $this->path_for_storage_escudo);
		}
	}

}
