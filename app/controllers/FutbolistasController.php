<?php

class FutbolistasController extends BaseController {

	/**
	 * Futbolista Repository
	 *
	 * @var Futbolista
	 */
	protected $futbolista;
	protected $pais;
	protected $posicion;
	protected $ciudad;
	protected $torneo;
	protected $club;

	public function __construct(Futbolista $futbolista, 
								Pais $pais, 
								Posicion $posicion,
								Ciudad $ciudad,
								Torneo $torneo,
								Club $club
								)
	{
		// INYECCION DE DEPENDENCIAS
		$this->futbolista = $futbolista;	
		$this->pais = $pais;
		$this->posicion = $posicion;
		$this->ciudad = $ciudad;
		$this->torneo = $torneo;
		$this->club = $club;

		// CONFIGURACION DE PATHS PARA IMAGENES DE FUTBOLISTAS
		$this->path_for_publicate_avatar = '/files/futbolistas/avatar/';
		$this->path_for_storage_avatar = public_path().$this->path_for_publicate_avatar;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Request::ajax()){
			return Response::json($this->futbolista->all());
		}

		$futbolistas = $this->futbolista->orderBy('id', 'des')->paginate(10);
		$paises = $this->pais->lists('nombre', 'id');
		$posiciones = $this->posicion->lists('abreviatura', 'id');
		$ciudades = $this->ciudad->lists('nombre', 'id');
		$torneos = $this->torneo->lists('nombre', 'id');

		return View::make('futbolistas.index', 
						  compact('futbolistas', 
						  		  'paises', 
						  		  'posiciones',
						  		  'ciudades',
						  		  'torneos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$paises = $this->pais->lists('nombre', 'id');
		$posiciones = $this->posicion->lists('abreviatura', 'id');
		$ciudades = $this->ciudad->lists('nombre', 'id');
		$ciudades = array(0 => '---     Ciudades    ---') + $ciudades;
		$torneos = $this->torneo->lists('nombre', 'id');

		return View::make('futbolistas.create', 
						  compact('paises', 
						  		  'posiciones', 
						  		  'ciudades',
						  		  'torneos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Futbolista::$rules);

		if ($validation->passes())
		{
			// se obtiene las propiedas del futbolistas que se presentan como colecciones de dato
			// luego se elimina  del arreglo input para no generar error en el metodo create
			if(isset($input['nacionalidad'])){
				$nacionalidades = $input['nacionalidad'];
				unset($input['nacionalidad']);
			} 
			if(isset($input['posicion'])){
				$posiciones = $input['posicion'];
				unset($input['posicion']);
			}

			if(isset($input['ciudad_id']) && $input['ciudad_id'] == 0)
				unset($input['ciudad_id']);

			// se comprueba si ha sido enviado algun archivo de ser asi se movera a la carpeta correspondiente
			if(Input::hasFile('avatar')){
				$input['avatar']->move($this->path_for_storage_avatar, $input['avatar']->getClientOriginalName());
				Image::make($this->path_for_storage_avatar.$input['avatar']->getClientOriginalName())
						->widen(80)
						->save($this->path_for_storage_avatar.'thumbnail/'.$input['avatar']->getClientOriginalName());
				$input['avatar'] = $this->path_for_publicate_avatar.$input['avatar']->getClientOriginalName();
			}else
				unset($input['avatar']);

			// se crea el nuevo futbolista y el objeto resultante se almacena en la variable $futbolista
			$futbolista = $this->futbolista->create($input);

			// se sincronizan las propiedas colectoras en la tabla intermedia
			if(isset($nacionalidades)) $futbolista->nacionalidades()->sync($nacionalidades);
			if(isset($posiciones)) $futbolista->posiciones()->sync($posiciones);

			return Redirect::route('futbolistas.index');
		}

		return Redirect::route('futbolistas.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// se obtiene el futbolista a mostrar
		$futbolista = $this->futbolista->findOrFail($id);
		
		// se obtiene la lista de caracteristicas
		$clubes = $this->club->lists('nombre', 'id');
		$torneos = $futbolista->torneos()->get();
		$ciudad_nacimiento = $futbolista->ciudad_nacimiento()->first();

		$futbolista->ciudad_nacimiento = is_null($ciudad_nacimiento) ?
			'' :
			$ciudad_nacimiento->nombre ;

		$futbolista->avatar = is_null($futbolista->avatar) ?
			Config::get('images.AVATAR_DEFAULT') :
			$futbolista->avatar;
		
		// se retorna la vista
		return View::make('futbolistas.show', 
						  compact('futbolista', 
						  		  'paises', 
						  		  'posiciones',
						  		  'ciudades',
						  		  'torneos',
						  		  'clubes'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// se obtiene el futbolista a modificar
		$futbolista = $this->futbolista->find($id);
		
		// se comprueba que el futbolista exita
		if (is_null($futbolista))
		{	// si es futbolista no existe se redirige a la vista principal
			return Redirect::route('futbolistas.index');
		}
		
		// estas son las listas de posibles opciones a escoger por el usuario
		$paises = $this->pais->lists('nombre', 'id');
		$posiciones = $this->posicion->lists('abreviatura', 'id');
		$ciudades = $this->ciudad->lists('nombre', 'id');
		$ciudades = array(0 => '---     Ciudades    ---') + $ciudades;
		$torneos = $this->torneo->lists('nombre', 'id');
		
		/**
		 *		Este paso no hace otra cosa que simplicar la escritura del programa
		 *	para que no existe necesidad de hacer referencia a la estructura interna
		 *	del los modelos dentro de la vista.
		 */
		$futbolista->nacionalidad = $futbolista->nacionalidades()->lists('id');
		$futbolista->posicion = $futbolista->posiciones()->lists('id');
		$futbolista->torneo = $futbolista->torneos()->lists('id');

		return View::make('futbolistas.edit', 
						  compact('futbolista', 
						  		  'paises', 
						  		  'posiciones',
						  		  'ciudades',
						  		  'torneos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Futbolista::$rules);

		if ($validation->passes())
		{
			// se comprueba si ha sido enviado algun archivo de ser asi se movera a la carpeta correspondiente
			if(Input::hasFile('avatar')){
				$input['avatar']->move($this->path_for_storage_avatar, $input['avatar']->getClientOriginalName());
				Image::make($this->path_for_storage_avatar.$input['avatar']->getClientOriginalName())
						->widen(80)
						->save($this->path_for_storage_avatar.'thumbnail/'.$input['avatar']->getClientOriginalName())
						;
				$input['avatar'] = $this->path_for_publicate_avatar.$input['avatar']->getClientOriginalName();
			}else
				unset($input['avatar']);

			// se obtiene el futbolista a modificar
			$futbolista = $this->futbolista->find($id);

			// se obtine las propiedades colectoras
			// luego se elimina las propiedades colectoras para no generar errores en el metodo update
			if(isset($input['nacionalidad'])){
				$nacionalidades = $input['nacionalidad'];
				unset($input['nacionalidad']);
			} 
			if(isset($input['posicion'])){
				$posiciones = $input['posicion'];
				unset($input['posicion']);
			}

			if(isset($input['ciudad_id']) && $input['ciudad_id'] == 0)
				unset($input['ciudad_id']);
			
			// se actualiza la informacion del futbolista
			$futbolista->update($input);

			// se sincronizan las pripiedades colectoras en sus respectivas tablas intermedias
			if(isset($nacionalidades)) 
				$futbolista->nacionalidades()->sync($nacionalidades);
			else
				$futbolista->nacionalidades()->sync(array());
			
			if(isset($posiciones)) 
				$futbolista->posiciones()->sync($posiciones);
			else
				$futbolista->posiciones()->sync(array());

			return Redirect::route('futbolistas.show', $id);
		}

		return Redirect::route('futbolistas.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->futbolista->find($id)->delete();

		return Redirect::route('futbolistas.index');
	}

	public function busqueda($query){

		$algo = $this->futbolista->where('nombre', 'like', "%$query%")
									->orWhere('apellido', 'like', "%$query%")
									->select('id', 'nombre', 'apellido', 'avatar')->get();
		
		foreach ($algo as $key => $obj) {
			$obj->tokens = array($obj->nombre, $obj->apellido);
			$obj->value = $obj->nombre.' '.$obj->apellido;
		}

		return Response::json($algo);
	}
}
