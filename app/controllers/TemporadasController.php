<?php

class TemporadasController extends BaseController {

	/**
	 * Temporada Repository
	 *
	 * @var Temporada
	 */
	protected $temporada;
	protected $pais;

	public function __construct(Temporada $temporada, Pais $pais)
	{
		$this->temporada = $temporada;
		$this->pais = $pais;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$temporadas = $this->temporada->all();
		$paises = $this->pais->orderBy('nombre')->lists('nombre', 'id');

		return View::make('temporadas.index', compact('temporadas', 'paises'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$paises = $this->pais->lists('nombre', 'id');

		return View::make('temporadas.create', compact('paises'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Temporada::$rules);

		if ($validation->passes())
		{
			$input['inicio'] = self::to_model_date($input['inicio']);
			$input['fin'] = self::to_model_date($input['fin']);

			if($input['actual']=='si')
				$this->temporada->where('actual', 'si')
								->where('pais_id', $input['pais_id'])
								->update(array('actual' => 'no'));

			$this->temporada->create($input);

			return Redirect::route('temporadas.index');
		}

		return Redirect::route('temporadas.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$temporada = $this->temporada->findOrFail($id);
		$paises = $this->pais->lists('nombre', 'id');

		return View::make('temporadas.show', compact('temporada', 'paises'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$temporada = $this->temporada->find($id);

		if (is_null($temporada))
		{
			return Redirect::route('temporadas.index');
		}
		$paises = $this->pais->lists('nombre', 'id');
		
		$temporada->inicio = self::to_view_date($temporada->inicio);
		$temporada->fin = self::to_view_date($temporada->fin);

		return View::make('temporadas.edit', compact('temporada', 'paises'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Temporada::$rules);

		if ($validation->passes())
		{
			$input['inicio'] = self::to_model_date($input['inicio']);
			$input['fin'] = self::to_model_date($input['fin']);

			if($input['actual']=='si')
				$this->temporada->where('actual', 'si')
								->where('pais_id', $input['pais_id'])
								->update(array('actual' => 'no'));

			$temporada = $this->temporada->find($id);
			$temporada->update($input);

			return Redirect::route('temporadas.show', $id);
		}

		return Redirect::route('temporadas.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->temporada->find($id)->delete();

		return Redirect::route('temporadas.index');
	}

}
