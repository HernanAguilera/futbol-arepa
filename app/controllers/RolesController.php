<?php

class RolesController extends \BaseController {

	/**
	 * Display a listing of rols
	 *
	 * @return Response
	 */
	public function index()
	{
		$rols = Rol::all();

		return View::make('roles.index', compact('rols'));
	}

	/**
	 * Show the form for creating a new rol
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('roles.create');
	}

	/**
	 * Store a newly created rol in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Rol::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Rol::create($data);

		return Redirect::route('admin.roles.index');
	}

	/**
	 * Display the specified rol.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rol = Rol::findOrFail($id);

		return View::make('roles.show', compact('rol'));
	}

	/**
	 * Show the form for editing the specified rol.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rol = Rol::find($id);

		return View::make('roles.edit', compact('rol'));
	}

	/**
	 * Update the specified rol in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rol = Rol::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Rol::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$rol->update($data);

		return Redirect::route('admin.roles.index');
	}

	/**
	 * Remove the specified rol from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Rol::destroy($id);

		return Redirect::route('admin.roles.index');
	}

}
