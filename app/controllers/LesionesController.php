<?php

class LesionesController extends BaseController {

	/**
	 * Lesion Repository
	 *
	 * @var Lesion
	 */
	protected $lesion;

	public function __construct(Lesion $lesion)
	{
		$this->lesion = $lesion;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$lesiones = $this->lesion->all();

		return View::make('lesiones.index', compact('lesiones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('lesiones.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Lesion::$rules);

		if ($validation->passes())
		{
			$this->lesion->create($input);

			return Redirect::route('lesiones.index');
		}

		return Redirect::route('lesiones.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$lesion = $this->lesion->findOrFail($id);

		return View::make('lesiones.show', compact('lesion'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$lesion = $this->lesion->find($id);

		if (is_null($lesion))
		{
			return Redirect::route('lesiones.index');
		}

		return View::make('lesiones.edit', compact('lesion'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Lesion::$rules);

		if ($validation->passes())
		{
			$lesion = $this->lesion->find($id);
			$lesion->update($input);

			return Redirect::route('lesiones.show', $id);
		}

		return Redirect::route('lesiones.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->lesion->find($id)->delete();

		return Redirect::route('lesiones.index');
	}

}
