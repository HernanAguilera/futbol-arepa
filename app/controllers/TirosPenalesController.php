<?php

class TirosPenalesController extends BaseController {

	/**
	 * TiroPenal Repository
	 *
	 * @var TiroPenal
	 */
	protected $penal;

	public function __construct(TiroPenal $penal)
	{
		$this->penal = $penal;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$penales = $this->penal->all();

		return View::make('tiros_penales.index', 
						  compact('penales'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('tiros_penales.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if(!isset($input['anotado'])) $input['anotado'] = FALSE;
		
		$validation = Validator::make($input, TiroPenal::$rules);

		if ($validation->passes())
		{
			$this->penal->create($input);

			return Redirect::route('tiros-penales.index');
		}

		return Redirect::route('tiros-penales.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$penal = $this->penal->findOrFail($id);

		return View::make('tiros_penales.show', 
						  compact('penal'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$penal = $this->penal->find($id);

		if (is_null($penal))
		{
			return Redirect::route('tiros-penales.index');
		}

		return View::make('tiros_penales.edit', 
						  compact('penal'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');

		if(!isset($input['anotado'])) $input['anotado'] = FALSE;

		$validation = Validator::make($input, TiroPenal::$rules);

		if ($validation->passes())
		{
			$penal = $this->penal->find($id);
			$penal->update($input);

			return Redirect::route('tiros-penales.show', $id);
		}

		return Redirect::route('tiros-penales.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->penal->find($id)->delete();

		return Redirect::route('tiros-penales.index');
	}

}
