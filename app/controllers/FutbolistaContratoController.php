<?php

class FutbolistaContratoController extends \BaseController {

	protected $futbolista;
	protected $tempodara;
	protected $torneo;
	protected $club;

	public function __construct(Futbolista $futbolista,
								Temporada $temporada,
								Torneo $torneo,
								Club $club){
		$this->futbolista = $futbolista;
		$this->temporada = $temporada;
		$this->torneo = $torneo;
		$this->club = $club;
	}

	public function create($id){
		$futbolista = $this->futbolista->find($id);
		$temporadas = $this->temporada->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');
		$torneos = $this->torneo->lists('nombre', 'id');
		return View::make('contrato.futbolistas.create', 
						  compact('futbolista',
						  		  'temporadas',
						  		  'torneos',
						  		  'clubes'));
	}

	public function store(){
		$input = Input::all();

		$futbolista = $this->futbolista->find($input['futbolista']);
		$futbolista->torneos()->detach($input['torneo']);
		$futbolista->torneos()->attach($input['torneo'], array('club_id' => $input['club'], 'numero' => $input['numero']));

		if(Request::ajax())
			return Response::json(array('mensaje'=>'datos almacenados'));
		else
			return Redirect::to("/futbolistas/$futbolista->id");
	}


	public function edit($futbolista_id, $torneo_id){

		$temporadas = $this->temporada->lists('nombre', 'id');
		$torneos = $this->torneo->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');

		$futbolista = $this->futbolista->find($futbolista_id);
		$torneo = $this->torneo->find($torneo_id);
		$temporada_id = $torneo->temporada()->first()->temporada_id;
		$club_id = $futbolista->torneos->find($torneo_id)->pivot->club_id;
		$numero = $futbolista->torneos->find($torneo_id)->pivot->numero;
		
		return View::make('contrato.futbolistas.edit', 
						  compact('futbolista',
						  		  'temporadas',
						  		  'torneos',
						  		  'clubes',
						  		  'temporada_id',
						  		  'torneo_id',
						  		  'club_id',
						  		  'numero'
						  		  ));
	}

	public function update($id){
		$input = Input::all();
		
		$futbolista = $this->futbolista->find($input['futbolista']);
		Log::info('input', array('torneo anterior' => $input['torneo_anterior'], 'nuevo torneo' => $input['torneo']));
		$futbolista->torneos()->detach($input['torneo_anterior']);
		$futbolista->torneos()->attach($input['torneo'], array('club_id' => $input['club'], 'numero' => $input['numero']));
		return Redirect::to("/futbolistas/$futbolista->id");
	}


	public function getFutbolistaByTorneoAndClub($torneo_id, $club_id){

		$futbolistas = Futbolista::getFutbolistaByTorneoAndClub($torneo_id, $club_id);

		return Response::json(array('futbolistas' => $futbolistas));
	}

}
