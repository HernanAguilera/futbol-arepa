<?php

class GolesController extends BaseController {

	/**
	 * Gol Repository
	 *
	 * @var Gol
	 */
	protected $gol;
	protected $partido;
	protected $club;
	protected $futbolista;
	protected $tipo;

	public function __construct(Gol $gol,
								Partido $partido,
								Club $club,
								Futbolista $futbolista,
								TipoDeGol $tipo)
	{
		$this->gol = $gol;
		$this->partido = $partido;
		$this->club = $club;
		$this->futbolista = $futbolista;
		$this->tipo = $tipo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$goles = $this->gol->orderBy('id', 'des')->paginate(15);
		$clubes = $this->club->lists('nombre', 'id');
		$futbolistas = $this->futbolista->lists('nombre', 'id');
		$partidos_c = $this->partido->get();

		$partidos = array();
		foreach ($partidos_c as $key => $partido) {
			$partido[$key] = '';
			foreach ($partido->clubes as $club) {
				$partido[$key] .= $club->nombre.' ';
			}
		}

		return View::make('goles.index', 
						  compact('goles',
						  		  'clubes',
						  		  'futbolistas',
						  		  'partidos'
						  		  ));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$clubes = $this->club->lists('nombre', 'id');
		$tipos = $this->tipo->lists('descripcion', 'id');
		$futbolistas_c = $this->futbolista->select('nombre', 'apellido', 'id')->get();

		$futbolistas = array();
		foreach ($futbolistas_c as $key => $futbolista) {
			$futbolistas[$futbolista->id] = $futbolista->nombre.' '.$futbolista->apellido;
		}

		$partidos_c = $this->partido->all();

		$partidos = array();
		foreach ($partidos_c as $key => $partido) {
			$partidos[$key] = '';
			foreach ($partido->clubes as $club) {
				$partidos[$key] .= $club->nombre.' - ';
			}
			$partidos[$key] .= $partido->fecha_id;
		}

		return View::make('goles.create', 
						  compact('goles',
						  		  'clubes',
						  		  'futbolistas',
						  		  'partidos',
						  		  'tipos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Gol::$rules);

		if ($validation->passes())
		{

			$tweet = false;
			if(isset($input['tweet'])){
				unset($input['tweet']);
				$tweet = true;
			}

			$gol = $this->gol->create($input);

			if($tweet){
				Input::merge(array('gol_id' => $gol->id));
				$request = Request::create('tweet/partido/gol', 
										   'POST', 
											array('gol_id' => $gol->id));
				Route::dispatch($request);
			}

			return Redirect::route('goles.index');
		}

		return Redirect::route('goles.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$gol = $this->gol->findOrFail($id);

		return View::make('goles.show', compact('gol'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$gol = $this->gol->find($id);

		if (is_null($gol))
		{
			return Redirect::route('goles.index');
		}
		$tipos = $this->tipo->lists('descripcion', 'id');

		return View::make('goles.edit', compact('gol', 'tipos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Gol::$rules);

		if ($validation->passes())
		{	
			$gol = $this->gol->find($id);
			$gol->update($input);

			return Redirect::route('goles.show', $id);
		}

		return Redirect::route('goles.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->gol->find($id)->delete();

		return Redirect::route('goles.index');
	}

}
