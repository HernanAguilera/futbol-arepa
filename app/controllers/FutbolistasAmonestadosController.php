<?php

class FutbolistasAmonestadosController extends BaseController {

	/**
	 * FutbolistaAmonestado Repository
	 *
	 * @var FutbolistaAmonestado
	 */
	protected $amonestado;
	protected $futbolista;
	protected $club;
	protected $partido;

	public function __construct(FutbolistaAmonestado $amonestado,
								Futbolista $futbolista,
								Club $club,
								Partido $partido)
	{
		$this->amonestado = $amonestado;
		$this->futbolista = $futbolista;
		$this->club = $club;
		$this->partido = $partido;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$amonestados = $this->amonestado->all();

		return View::make('futbolistas_amonestados.index', compact('amonestados'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$futbolistas_c = $this->futbolista->select('id', 'nombre', 'apellido')->get();
		$clubes = $this->club->lists('nombre', 'id');
		$partidos_c = $this->partido->get();

		$futbolistas = array();
		$partidos = array();
		foreach ($futbolistas_c as $key => $futbolista) {
			$futbolistas[$futbolista->id] = $futbolista->nombre.' '.$futbolista->apellido;
		}
		foreach ($partidos_c as $key => $partido) {
			$partidos[$partido->id] = json_encode($partido->clubes());
		}

		return View::make('futbolistas_amonestados.create', compact('futbolistaa', 'clubes', 'partidos'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, FutbolistaAmonestado::$rules);

		if ($validation->passes())
		{
			$this->amonestado->create($input);

			return Redirect::route('futbolistas-amonestados.index');
		}

		return Redirect::route('futbolistas-amonestados.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$amonestado = $this->amonestado->findOrFail($id);

		return View::make('futbolistas_amonestados.show', compact('amonestado'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$amonestado = $this->amonestado->find($id);

		if (is_null($amonestado))
		{
			return Redirect::route('futbolistas-amonestados.index');
		}

		return View::make('futbolistas_amonestados.edit', compact('amonestado'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, FutbolistaAmonestado::$rules);

		if ($validation->passes())
		{
			$amonestado = $this->amonestado->find($id);
			$amonestado->update($input);

			return Redirect::route('futbolistas-amonestados.show', $id);
		}

		return Redirect::route('futbolistas-amonestados.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->amonestado->find($id)->delete();

		return Redirect::route('futbolistas-amonestados.index');
	}

}
