<?php

class GruposController extends BaseController {

	/**
	 * grupo Repository
	 *
	 * @var grupo
	 */
	protected $grupo;
	protected $torneo;
	protected $club;

	public function __construct(Grupo $grupo, 
								Torneo $torneo,
								Club $club)
	{
		$this->grupo = $grupo;
		$this->torneo = $torneo;
		$this->club = $club;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$grupos = $this->grupo->all();

		return View::make('grupos.index', compact('grupos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$torneos = $this->torneo->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');

		return View::make('grupos.create', compact('torneos', 'clubes'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, grupo::$rules);

		if ($validation->passes())
		{
			if(isset($input['clubes'])){
				$clubes = $input['clubes'];
				unset($input['clubes']);
			}

			$this->grupo->create($input);

			if(isset($clubes)) $grupo->clubes()->sync($clubes);

			return Redirect::route('grupos.index');
		}

		return Redirect::route('grupos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$grupo = $this->grupo->findOrFail($id);

		return View::make('grupos.show', compact('grupo'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$grupo = $this->grupo->find($id);

		if (is_null($grupo))
		{
			return Redirect::route('grupos.index');
		}
		$torneos = $this->torneo->lists('nombre', 'id');
		$grupo->clubes = $grupo->clubes()->lists('id');
		$clubes = $this->club->lists('nombre', 'id');

		return View::make('grupos.edit', compact('grupo', 'torneos', 'clubes'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, grupo::$rules);

		if ($validation->passes())
		{
			if(isset($input['clubes'])){
				$clubes = $input['clubes'];
				unset($input['clubes']);
			}

			$grupo = $this->grupo->find($id);
			$grupo->update($input);

			if(isset($clubes)) $grupo->clubes()->sync($clubes);

			return Redirect::route('grupos.show', $id);
		}

		return Redirect::route('grupos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->grupo->find($id)->delete();

		return Redirect::route('grupos.index');
	}

}
