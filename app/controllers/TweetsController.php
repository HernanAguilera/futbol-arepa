<?php 

/**
* 
*/
class TweetsController extends \BaseController
{
	
	public function __construct()
	{
		
	}

	private function enviarEstado($mensaje){

		//return Twitter::getUserTimeline(array('screen_name' => 'hernanaguilera', 'count' => 1, 'format' => 'array'));
		return Twitter::postTweet(array('status' => $mensaje, 'format' => 'json'));
	}

	public function statusPartido(){
		$input = Input::all();
		$partido_id = $input['partido_id'];
		$partido = Partido::find($partido_id);
		$detalles = $partido->detalles($partido_id);

		$tiempo = 0;
		switch ($input['tiempo']) {
					case '2T':
						$tiempo = 45;
						break;
					case '1TEX':
						$tiempo = 90;
						break;
					case '2TEX':
						$tiempo = 105;
						break;
					case 'I':
						$tiempo = 'Inicia el encuentro';
						break;
					case 'MT':
						$tiempo = 'Descanso';
						break;
					case 'I2':
						$tiempo = 'Inicia el 2do tiempo';
						break;
					case 'F':
						$tiempo = 'Final';
						break;
					case 'A':
						$tiempo = 'Ayer';
						break;
				}

		$torneo['categoria'] = $detalles['torneo']->categoria()->first()->nombre;
		$nombreArray = explode('(', $detalles['torneo']->nombre);
		$torneo['nombre'] = $nombreArray[0];

		$fecha['nombre'] = $detalles['fecha']->jornada;
		
		$local['twitter'] 		= $detalles['clubes']['local']->twitter;
		if(!(strpos($local['twitter'], '#') !== FALSE)) 
			$local['twitter'] = '@'.$local['twitter'];

		$local['goles']			= $detalles['goles']['local']->cantidad;

		$anotaciones['local'] = $this->lista_goles($detalles['goles']['local']->goles);
		
		$visitante['twitter'] 	= $detalles['clubes']['visitante']->twitter;
		if(!(strpos($visitante['twitter'], '#') !== FALSE))
			$visitante['twitter'] = '@'.$visitante['twitter'];

		$visitante['goles']		= $detalles['goles']['visitante']->cantidad;
		
		$anotaciones['visitante'] = $this->lista_goles($detalles['goles']['visitante']->goles);

		/**
		 *	Construccion del mensaje
		 */
		if(empty($detalles['grupo'])){
			$mensaje_goles  = '#'.str_replace(' ', '', $torneo['nombre']).' #'.str_replace(' ', '', $fecha['nombre']);
			$mensaje  = '#'.str_replace(' ', '', $torneo['nombre']).' #'.str_replace(' ', '', $fecha['nombre']);
		}else{
			$mensaje_goles  = '#Grupo'.str_replace(' ', '', $detalles['grupo']).' #'.str_replace(' ', '', $fecha['nombre']);
			$mensaje  = '#Grupo'.str_replace(' ', '', $detalles['grupo']).' #'.str_replace(' ', '', $fecha['nombre']);
		}
		if(is_numeric($tiempo)){
			$mensaje_goles .= ' | '.($input['minuto']+$tiempo).'\'';
			$mensaje .= ' | '.($input['minuto']+$tiempo).'\'';
		}else{
			$mensaje_goles .= ' | '.$tiempo;
			$mensaje .= ' | '.$tiempo;
		}
		$mensaje_goles .= ' | '.$local['twitter'].' '.$local['goles'].' '.'-'.' '.$visitante['goles'].' '.$visitante['twitter'];
		$mensaje .= ' | '.$local['twitter'].' '.$local['goles'].' '.'-'.' '.$visitante['goles'].' '.$visitante['twitter'];
		
		if(!empty($anotaciones['local']) && !empty($anotaciones['visitante']))
			$mensaje_goles .= ' | '.$anotaciones['local'].'; '.$anotaciones['visitante'];
		elseif(empty($anotaciones['local']) && !empty($anotaciones['visitante']))
			$mensaje_goles .= ' | '.$anotaciones['visitante'];
		elseif(!empty($anotaciones['local']) && empty($anotaciones['visitante']))
			$mensaje_goles .= ' | '.$anotaciones['local'];
		
		if($detalles['torneo']->tipo != 'copa'){
			$mensaje_goles .= ' | #'.str_replace(' ', '', $torneo['categoria']);
			$mensaje .= ' | #'.str_replace(' ', '', $torneo['categoria']);
		}

		if(strlen($mensaje_goles)>140)
			$this->enviarEstado($mensaje);
		else
			$this->enviarEstado($mensaje_goles);

		// if(strlen($mensaje_goles)>140)
		// 	Log::info($mensaje);
		// else
		// 	Log::info($mensaje_goles);

		
	}

	public function gol(){
		$input = Input::all();		
		$gol_id = $input['gol_id'];
		$gol = Gol::find($gol_id);
		$torneo = $gol->partido->fecha->torneo;

		$tiempo = 0;
		switch ($input['tiempo']) {
					case '2T':
						$tiempo = 45;
						break;
					case '1TEX':
						$tiempo = 90;
						break;
					case '2TEX':
						$tiempo = 105;
						break;
		}

		if($torneo->tipo=='copa')
			$mensaje = '#'.str_replace(' ', '', $gol->partido->fecha->torneo->nombre);
		else
			$mensaje = '#'.str_replace(' ', '', $torneo->categoria->nombre);
		
		$mensaje .= ' | Gooooool para ';

		$twitter = $gol->club->twitter;
		if(!(strpos($twitter, '#') !== FALSE)) 
			$twitter = '@'.$twitter;
		$mensaje .= $twitter;


		if(!empty($gol->futbolista_id))
			$mensaje .= ' | '.$gol->futbolista->nombre.' '.$gol->futbolista->apellido;
		if($gol->minuto != 0)
			$mensaje .= ' al minuto '.($gol->minuto+$tiempo);

		// Log::info($mensaje);

		$this->enviarEstado($mensaje);
	}


	public function lista_goles($anotadores){
		$anotaciones_tmp = array();
		
		foreach ($anotadores as $anotador) {
			if(!empty($anotador->futbolista)){
				$anotador_tmp = futbolista::find($anotador->futbolista_id)->apellido;
				foreach ($anotador->anotaciones as $gol) {
					$anotador_tmp .= ' '.$gol->minuto.'\'';
				}
				$anotaciones_tmp[] = $anotador_tmp;
			}
		}

		$anotaciones = implode(', ', $anotaciones_tmp);

		return $anotaciones;
	}
}