<?php

class PartidosController extends BaseController {

	/**
	 * Partido Repository
	 *
	 * @var Partido
	 */
	protected $partido;
	protected $fecha;
	protected $estadio;
	protected $club;
	protected $arbitro;
	protected $rol_arbitro;
	protected $torneo;
	protected $estado_partido;

	public function __construct(Partido $partido,
								Fecha $fecha,
								Estadio $estadio,
								Club $club,
								Arbitro $arbitro,
								RolArbitro $rol_arbitro,
								Torneo $torneo,
								EstadoPartido $estado_partido)
	{
		$this->partido = $partido;
		$this->fecha = $fecha;
		$this->estadio = $estadio;
		$this->club = $club;
		$this->arbitro = $arbitro;
		$this->torneo = $torneo;
		$this->estado_partido = $estado_partido;

		$this->rol_arbitro = $rol_arbitro;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$torneos = array(0 => '------- Torneos --------') + $this->torneo->lists('nombre', 'id');

		return View::make('partidos.index', 
						  compact(
						  		  'torneos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$torneos = $this->torneo->get();
		$fechas = array();
		foreach ($torneos as $key => $torneo) {
			$fechas[$torneo->nombre] = $torneo->fechas()->lists('jornada', 'id');
		}


		$estadios = array(0 => '---  Estadios  ---') + $this->estadio->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');
		$estados = $this->estado_partido->lists('nombre', 'id');
		

		$roles = $this->rol_arbitro->select('id', 'nombre')->get();
		$arbitros = array('---   Arbitros   ---');
		$colegios = Colegio::orderby('nombre')->get();
		foreach ($colegios as $key => $colegio) {
			$c_arbitros = $colegio->arbitros()
									->select('id', 'nombre', 'apellido')
									->orderby('nombre')
									->orderby('apellido')
									->get();
			if($c_arbitros->count()){
				$grupo_arbitros = array();
				foreach ($c_arbitros as $key => $c_arbitro) {
					$grupo_arbitros[$c_arbitro->id] = $c_arbitro->nombre.' '.$c_arbitro->apellido;
				}
				$arbitros[$colegio->nombre] = $grupo_arbitros;
			}

		}

		$clubes = array(0 => '---   Clubes   ---') + $clubes;
		

		return View::make('partidos.create', 
						  compact('fechas', 
						  		  'torneos',
						  		  'estadios',
						  		  'clubes',
						  		  'arbitros',
						  		  'roles',
						  		  'estados'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Partido::$rules);

		if ($validation->passes())
		{
			# SINO SE ESCOJE ESTADIO SE TOMA POR DEFECTO EL DEL CLUB LOCAL
			if($input['estadio_id']==0) $input['estadio_id'] = Club::find($input['local'])->estadio_id;

			# PROCESO DE VALIDACION DE CLUBES PARTICIPANTES EN EL PARTIDO
			$clubes = $this->validateClubes($input);

			#  PROCESO DE VALIDACIÓN DE ARBITROS
			$arbitros = $this->validateArbitros($input);


			#	FORMATEAR FECHA
			$input['fecha_calendario'] = $this->to_model_datetime($input['fecha_calendario']);

			#	ELIMINAR CAMPO TORNEO
			if(isset($input['torneo'])) unset($input['torneo']);

			#	CREAR PARTIDO
			$partido = $this->partido->create($input);

			if(count($clubes)>1){
				$partido->clubes()->attach($clubes['local'], array('condicion' => 'local'));
				$partido->clubes()->attach($clubes['visitante'], array('condicion' => 'visitante'));
			}

			foreach ($arbitros as $key => $arbitro) {
				$partido->arbitros()->attach($arbitro, array('rol_id' => $key));
			}

			# ACTUALIZAR CACHE
			$partido->fecha->actualizarCache();

			return Redirect::route('partidos.index');
		}

		return Redirect::route('partidos.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$detalles = $this->partido->detalles($id);

		$detalles['partido']->fecha_calendario = $this->to_view_datetime($detalles['partido']->fecha_calendario);	

		return View::make('partidos.show', $detalles);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$partido = $this->partido->find($id);

		if (is_null($partido))
		{
			return Redirect::route('partidos.index');
		}

		$torneos = $this->torneo->get();
		$fechas = array();
		foreach ($torneos as $key => $torneo) {
			$fechas[$torneo->nombre] = $torneo->fechas()->lists('jornada', 'id');
		}

		$partido->fecha_calendario = $this->to_view_datetime($partido->fecha_calendario);
		$estadios = $this->estadio->lists('nombre', 'id');
		$clubes = $this->club->lists('nombre', 'id');
		$estados = $this->estado_partido->lists('nombre', 'id');
		
		$roles = $this->rol_arbitro->select('id', 'nombre')->get();

		$arbitros = array('---   Arbitros   ---');
		$colegios = Colegio::orderby('nombre')->get();
		foreach ($colegios as $key => $colegio) {
			$c_arbitros = $colegio->arbitros()
									->select('id', 'nombre', 'apellido')
									->orderby('nombre')
									->orderby('apellido')
									->get();
			if($c_arbitros->count()){
				$grupo_arbitros = array();
				foreach ($c_arbitros as $key => $c_arbitro) {
					$grupo_arbitros[$c_arbitro->id] = $c_arbitro->nombre.' '.$c_arbitro->apellido;
				}
				$arbitros[$colegio->nombre] = $grupo_arbitros;
			}

		}

		$clubes = array(0 => '---   Clubes   ---') + $clubes;
		# OBTENER CLUBES PARTICIPANTES
		$partido->clubes = $this->getClubes($partido);
		# OBTENER ARBITROS DEL PARTIDO
		$partido->arbitros = $this->getArbitros($partido);

		return View::make('partidos.edit', 
						  compact('partido',
						  		  'torneos',
						  		  'fechas',
						  		  'estadios',
						  		  'clubes',
						  		  'arbitros',
						  		  'roles',
						  		  'estados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Partido::$rules);

		if ($validation->passes())
		{
			# PROCESO DE VALIDACION DE CLUBES PARTICIPANTES EN EL PARTIDO
			$clubes = $this->validateClubes($input);

			#  PROCESO DE VALIDACIÓN DE ARBITROS
			$arbitros = $this->validateArbitros($input);

			#	ELIMINAR CAMPO TORNEO
			if(isset($input['torneo'])) unset($input['torneo']);

			#	FORMATEAR FECHA
			$input['fecha_calendario'] = $this->to_model_datetime($input['fecha_calendario']);

			#	BUSCA EL PARTIDO POR EL ID
			$partido = $this->partido->find($id);
			#	ACTUALIZA EL PARTIDO
			$partido->update($input);

			$partido->clubes()->detach();

			if(count($clubes)>1){
				$partido->clubes()->attach($clubes['local'], array('condicion' => 'local'));
				$partido->clubes()->attach($clubes['visitante'], array('condicion' => 'visitante'));
			}

			$partido->arbitros()->detach();

			foreach ($arbitros as $key => $arbitro) {
				$partido->arbitros()->attach($arbitro, array('rol_id' => $key));
			}

			# ACTUALIZAR CACHE
			$partido->fecha->actualizarCache();

			return Redirect::route('partidos.show', $id);
		}

		return Redirect::route('partidos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$partido = $this->partido->find($id);

		$fecha = $partido->fecha()->first();
		
		$partido->delete();

		$fecha->actualizarCache();

		return Redirect::route('partidos.index');
	}


	public function busqueda($fecha_id){
		
		$partidos = $this->partido
						->where('fecha_id', $fecha_id)
						->orderby('fecha_calendario')
						->get();
		foreach ($partidos as $key => $partido) {
			/**
			 *		*$clubes_c* es la coleccion de clubes con todos los campos del registro
			 *	como no queremos solo algunos haremos seleccion de los mismo en el
			 *	que aparece a continuacion
			 */
			$clubes_c = $partido->clubes()->select('nombre')->get();

			$clubes = array(); // parte del retorno de la funcion
			foreach ($clubes_c as $key => $club) {
				$club_temp = new stdClass();
				$club_temp->nombre = $club->nombre;
				$clubes[] = $club_temp;
			}
			$partido->clubes = $clubes;
			$partido->fecha = $partido->fecha()->first()->jornada;
			$partido->estadio = $partido->estadio()->first()->nombre;
			$partido->estado = $partido->estado()->get()->count() > 0 ? $partido->estado()->first()->nombre : '';
		}

		return Response::json(array('partidos' => $partidos));
	}

	/****************************************************************
	 ************* SECCION DE UTILIDADES DEL CONTROLADOR ************
	 ****************************************************************/

	/**
	 *	valida si se han enviado los clubes de forma correcta
	 *
	 *	@param array input
	 *	@return array clubes
	 */

	private function validateClubes(&$input){
		$club = array();
		// se comprueba que ambos clubes que disputaran el partido hayan sido enviados
		if(isset($input['local']) && isset($input['visitante']) 
			&& $input['local'] != 0 && $input['visitante'] != 0){
			// se comprueba que local y visitante no sean el mismo club
			if($input['local'] != $input['visitante']){
				$club['local'] = $input['local'];
				$club['visitante'] = $input['visitante'];
			}
			// se eliminan las variables de clubes del arreglo input
			unset($input['local']);
			unset($input['visitante']);
		}else 
		// se comprueba que solo haya sido definido alguno de los dos clubes
		if (isset($input['local']) || isset($input['visitante'])) {
		// de ser asi se elimina pues es necesario dos clubes para poder disputarse un partido
			if(isset($input['local'])) unset($input['local']);
			if(isset($input['visitante'])) unset($input['visitante']);
		}
		return $club;
	}

	/**
	 *
	 */

	private function validateArbitros(&$input){
		$arbitros = array();
		foreach ($input as $key => $campo) {
			if(self::startsWith($key, 'rol_')){
				if($campo != 0){
					Log::info('el campo es diferente de cero => '.$campo);
					$new_key = str_replace('rol_', '', $key);
					$arbitros[$new_key] = $campo;
				}
				unset($input[$key]);
			}
		}

		$repetido = self::repeat_values($arbitros);
		if($repetido){
			foreach ($arbitros as $key => $arbitro) {
				unset($arbitro);
			}
		}
		
		return $arbitros;
	}

	private function getClubes($partido){
		$clubes_pivot = $partido->clubes()->get();
		$clubes_participantes = array();
		foreach ($clubes_pivot as $club) {
			$clubes_participantes[$club->pivot->condicion] = $club->pivot->club_id;
		}

		return $clubes_participantes;
	}

	private function getArbitros($partido){
		$arbitros_pivot = $partido->arbitros()->get();
		$arbitros_participantes = array();
		foreach ($arbitros_pivot as $arbitro) {
			$arbitros_participantes[$arbitro->pivot->rol_id] = $arbitro->pivot->arbitro_id;
		}

		return $arbitros_participantes;
	}

	private function actualizarCache($fecha){

		switch ($fecha->torneo_id) {
			case Config::get('public_view.primera'):
				if($fecha->actual)
					Cache::forever('primera_actual', Torneo::find($fecha->torneo_id)->ultimosResultados());
				if($fecha->proxima)
					Cache::forever('primera_proxima', Torneo::find($fecha->torneo_id)->proximaFecha());
				break;
			case Config::get('public_view.segunda'):
				if($fecha->actual)
					Cache::forever('segunda_actual', Torneo::find($fecha->torneo_id)->ultimosResultados());
				if($fecha->proxima)
					Cache::forever('segunda_proxima', Torneo::find($fecha->torneo_id)->proximaFecha());
				break;
			case Config::get('public_view.copa'):
				if($fecha->actual)
					Cache::forever('copa_actual', Torneo::find($fecha->torneo_id)->ultimosResultados());
				if($fecha->proxima)
					Cache::forever('copa_proxima', Torneo::find($fecha->torneo_id)->proximaFecha());
				break;
			case Config::get('public_view.internacional'):
				if($fecha->actual)
					Cache::forever('internacional_actual', Torneo::find($fecha->torneo_id)->ultimosResultados());
				if($fecha->proxima)
					Cache::forever('internacional_proxima', Torneo::find($fecha->torneo_id)->proximaFecha());
				break;
		}
	}

}
