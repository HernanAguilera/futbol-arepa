<?php

class EstadiosController extends BaseController {

	/**
	 * Estadio Repository
	 *
	 * @var Estadio
	 */
	protected $estadio;

	public function __construct(Estadio $estadio, Ciudad $ciudad)
	{
		$this->estadio = $estadio;
		$this->ciudad = $ciudad;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$estadios = $this->estadio->orderBy('nombre')->paginate(10);
		$ciudades = $this->ciudad->lists('nombre', 'id');

		return View::make('estadios.index', compact('estadios', 'ciudades'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$ciudades = $this->ciudad->lists('nombre', 'id');

		return View::make('estadios.create', compact('ciudades'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Estadio::$rules);

		if ($validation->passes())
		{
			$this->estadio->create($input);

			return Redirect::route('estadios.index');
		}

		return Redirect::route('estadios.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$estadio = $this->estadio->findOrFail($id);
		$clubes = $estadio->clubes()->get();

		return View::make('estadios.show', compact('estadio', 'clubes'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$estadio = $this->estadio->find($id);

		if (is_null($estadio))
		{
			return Redirect::route('estadios.index');
		}
		$ciudades = $this->ciudad->lists('nombre', 'id');
		return View::make('estadios.edit', compact('estadio', 'ciudades'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Estadio::$rules);

		if ($validation->passes())
		{
			$estadio = $this->estadio->find($id);
			$estadio->update($input);

			return Redirect::route('estadios.show', $id);
		}

		return Redirect::route('estadios.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->estadio->find($id)->delete();

		return Redirect::route('estadios.index');
	}

}
