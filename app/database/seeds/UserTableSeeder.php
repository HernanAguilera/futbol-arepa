<?php

class UserTableSeeder extends Seeder {
 
    public function run()
    {
        $users = [
            ['username' => 'admin', 'password' => Hash::make('123456'), 'email' => 'contacto@hernanaguilera.com.ve', 'rol_id' => 1],
        ];
 
        DB::table('users')->insert($users);
    }
 
}