<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	    //$this->call('RolsTableSeeder');
		//$this->call('UserTableSeeder');
		$this->call('ArbitrosTableSeeder');
		$this->call('TemporadasTableSeeder');
		$this->call('TorneosTableSeeder');
		$this->call('PaisTableSeeder');
		$this->call('EstadosTableSeeder');
		$this->call('CiudadsTableSeeder');
		$this->call('EstadiosTableSeeder');
		$this->call('FechasTableSeeder');
		$this->call('PosicionsTableSeeder');
		$this->call('FutbolistaTableSeeder');
		$this->call('Galeria_de_estadiosTableSeeder');
		$this->call('ClubsTableSeeder');
		$this->call('CategoriaTableSeeder');
		$this->call('LesionsTableSeeder');
		$this->call('Cargos_cuerpo_tecnicosTableSeeder');
		$this->call('Integrante_cuerpo_tecnicosTableSeeder');
		$this->call('PartidosTableSeeder');
		$this->call('Roles_arbitrosTableSeeder');
		$this->call('ConvocadosTableSeeder');
		$this->call('ColegiosTableSeeder');
		$this->call('GolsTableSeeder');
		$this->call('Tipo_de_golsTableSeeder');
		$this->call('Futbolistas_amonestadosTableSeeder');
		$this->call('GruposTableSeeder');
		$this->call('Tipos_de_partidosTableSeeder');
		$this->call('Estados_de_partidosTableSeeder');
		$this->call('Tiros_penalesTableSeeder');
	}

}
