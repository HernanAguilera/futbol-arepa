<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnActualToTemporadas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('temporadas', function(Blueprint $table) {
			$table->enum('actual', array('si', 'no'))
				  ->after('activa')
				  ->default('no')
				  ->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('temporadas', function(Blueprint $table) {
			$table->dropColumn('actual');
		});
	}

}
