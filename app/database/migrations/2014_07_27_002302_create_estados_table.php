<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estados', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->string('abreviatura', 8);
			$table->enum('visible', array('si', 'no'))->default('si');
			$table->integer('pais_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('pais_id')
				->references('id')
				->on('paises')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estados');
	}

}
