<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArbitrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('arbitros', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 50);
			$table->string('apellido', 50);
			$table->date('fecha_nacimiento')->nullable();
			$table->string('avatar', 128)->nullable();
			$table->integer('estado_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('estado_id')
				->references('id')
				->on('estados')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('arbitros');
	}

}
