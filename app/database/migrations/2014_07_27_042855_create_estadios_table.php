<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('estadios', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->integer('capacidad')->nullable();
			$table->integer('ciudad_id', FALSE, TRUE);
			$table->text('ubicacion')->nullable();
			$table->timestamps();
			$table->foreign('ciudad_id')
				->references('id')
				->on('ciudades')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('estadios');
	}

}
