<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTirosPenalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tiros_penales', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('partido_id', FALSE, TRUE);
			$table->integer('futbolista_id', FALSE, TRUE)->nullable();
			$table->integer('club_id', FALSE, TRUE);
			$table->boolean('anotado')->default(TRUE);
			$table->tinyInteger('orden');
			$table->timestamps();
			$table->foreign('partido_id')
				->references('id')
				->on('partidos')
				->onDelete('cascade');
			$table->foreign('futbolista_id')
				->references('id')
				->on('futbolistas')
				->onDelete('set null');
			$table->foreign('club_id')
				->references('id')
				->on('clubes')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tiros_penales');
	}

}
