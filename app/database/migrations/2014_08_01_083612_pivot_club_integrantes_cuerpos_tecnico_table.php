<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotClubIntegrantesCuerposTecnicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dirige', function(Blueprint $table) {
			$table->integer('club_id')->unsigned()->index();
			$table->integer('integrante_id')->unsigned()->index();
			$table->integer('cargo_id')->unsigned();
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->foreign('integrante_id')->references('id')->on('integrantes_cuerpos_tecnicos')->onDelete('cascade');
			$table->foreign('cargo_id')->references('id')->on('cargos_cuerpos_tecnicos')->onDelete('cascade');
			$table->primary(array('club_id', 'integrante_id'));

		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_integrantes_cuerpos_tecnico');
	}

}
