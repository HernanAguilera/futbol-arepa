<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotFutbolistaPaisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('futbolista_pais', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('pais_id')->unsigned()->index();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'pais_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('futbolista_pais');
	}

}
