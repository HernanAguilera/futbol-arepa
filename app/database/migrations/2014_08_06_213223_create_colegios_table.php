<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColegiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('colegios', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->date('fecha_fundacion')->nullable();
			$table->integer('estado_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('estado_id')
				->references('id')
				->on('estados')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('colegios');
	}

}
