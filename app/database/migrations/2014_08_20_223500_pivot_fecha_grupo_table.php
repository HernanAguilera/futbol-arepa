<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotFechaGrupoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fecha_grupo', function(Blueprint $table) {
			$table->integer('fecha_id')->unsigned()->index();
			$table->integer('grupo_id')->unsigned()->index();
			$table->foreign('fecha_id')->references('id')->on('fechas')->onDelete('cascade');
			$table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');
			$table->primary(array('fecha_id', 'grupo_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fecha_grupo');
	}

}
