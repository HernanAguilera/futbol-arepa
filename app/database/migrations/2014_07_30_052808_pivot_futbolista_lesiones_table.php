<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotFutbolistaLesionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('futbolista_lesiones', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('lesion_id')->unsigned()->index();
			$table->date('ocurrio_fecha')->nullable();
			$table->string('ocurrio_lugar', 256)->nullable();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('lesion_id')->references('id')->on('lesiones')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'lesion_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('futbolista_lesiones');
	}

}
