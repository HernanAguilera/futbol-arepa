<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFutbolistasAmonestadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('futbolistas_amonestados', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('futbolista_id', FALSE, TRUE)->nullable();
			$table->integer('club_id', FALSE, TRUE);
			$table->integer('partido_id', FALSE, TRUE);
			$table->enum('tipo', array('amarilla', 'roja'))->default('amarilla');
			$table->tinyInteger('minuto')->unsigned();
			$table->enum('tiempo', array('1T', '2T', '1TEX', '2TEX'));
			$table->timestamps();
			$table->foreign('futbolista_id')
				->references('id')
				->on('futbolistas')
				->onDelete('set null');
			$table->foreign('club_id')
				->references('id')
				->on('clubes')
				->onDelete('cascade');
			$table->foreign('partido_id')
				->references('id')
				->on('partidos')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('futbolistas_amonestados');
	}

}
