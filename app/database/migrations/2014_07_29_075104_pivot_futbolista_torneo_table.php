<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotFutbolistaTorneoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('futbolista_numero_en_torneo', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('torneo_id')->unsigned()->index();
			$table->integer('club_id')->unsigned()->index();
			$table->tinyInteger('numero')->unsigned()->nullable();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('torneo_id')->references('id')->on('torneos')->onDelete('cascade');
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'torneo_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('futbolista_numero_en_torneo');
	}

}
