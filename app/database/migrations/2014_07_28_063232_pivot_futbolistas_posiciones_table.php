<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotFutbolistasPosicionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('futbolista_posicion', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('posicion_id')->unsigned()->index();
			$table->boolean('principal')->nullable();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('posicion_id')->references('id')->on('posiciones')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'posicion_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('futbolista_posicion');
	}

}
