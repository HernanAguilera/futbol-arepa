<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClubesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clubes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->date('fundacion')->nullable();
			$table->string('escudo', 256)->nullable();
			$table->enum('activo', array('si', 'no'))->default('si');
			$table->integer('estado_id', FALSE, TRUE)->nullable();
			$table->integer('estadio_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('estado_id')
				->references('id')
				->on('estados')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('estadio_id')
				->references('id')
				->on('estadios')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clubes');
	}

}
