<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIntegrantesCuerposTecnicosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('integrantes_cuerpos_tecnicos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 80);
			$table->string('apellido', 80);
			$table->date('fecha_nacimiento')->nullable();
			$table->string('avatar', 256)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('integrantes_cuerpos_tecnicos');
	}

}
