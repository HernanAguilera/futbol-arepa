<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotClubeFutbolistaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('club_futbolista', function(Blueprint $table) {
			$table->integer('club_id')->unsigned()->index();
			$table->integer('futbolista_id')->unsigned()->index();
			$table->date('inicio_contrato')->nullable();
			$table->date('fin_contrato')->nullable();
			$table->enum('tipo_contrato', array('prestamo', 'pertenencia'))->default('pertenencia');
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->primary(array('club_id', 'futbolista_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_futbolista');
	}

}
