<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paises', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->string('gentilicio', 50)->nullable();
			$table->string('abreviatura', 8)->nullable();
			$table->string('bandera', 256)->nullable();
			$table->enum('visible', array('si', 'no'))->default('si');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paises');
	}

}
