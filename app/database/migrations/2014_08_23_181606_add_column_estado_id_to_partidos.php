<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnEstadoIdToPartidos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('partidos', function(Blueprint $table) {
			$table->integer('estado_id', FALSE, TRUE)
					->nullable()
					->after('fecha_calendario');
			$table->foreign('estado_id')
				->references('id')
				->on('estados_de_partidos')
				->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('partidos', function(Blueprint $table) {
			$table->dropForeign('partidos_estado_id_foreign');
			$table->dropColumn('estado_id');
		});
	}

}
