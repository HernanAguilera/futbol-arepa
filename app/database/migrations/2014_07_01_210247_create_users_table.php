<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email',100);
			$table->string('username',20);
			$table->string('password',128);
			$table->string('remember_token',100)->nullable();
			$table->integer('rol_id')->unsigned();
			$table->timestamps();
			$table->foreign('rol_id')
				  ->references('id')
				  ->on('rols')
				  ->onUpdate('cascade')
				  ->onDelete('cascade')
				  ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
