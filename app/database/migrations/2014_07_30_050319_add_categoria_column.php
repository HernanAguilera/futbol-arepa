<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCategoriaColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('torneos', function(Blueprint $table) {
			$table->integer('categoria_id', FALSE, TRUE);
			$table->foreign('categoria_id')
				->references('id')
				->on('categorias')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('torneos', function(Blueprint $table) {
			$table->dropForeign('torneos_categoria_id_foreign');
			$table->dropColumn('categoria_id');
		});
	}

}
