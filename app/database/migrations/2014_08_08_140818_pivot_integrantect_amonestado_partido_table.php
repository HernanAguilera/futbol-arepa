<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotIntegrantectAmonestadoPartidoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('integrantect_amonestado', function(Blueprint $table) {
			$table->integer('integrantect_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->integer('club_id', FALSE, TRUE);
			$table->foreign('integrantect_id')->references('id')->on('integrantes_cuerpos_tecnicos')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->primary(array('integrantect_id', 'partido_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('integrantect_amonestado');
	}

}
