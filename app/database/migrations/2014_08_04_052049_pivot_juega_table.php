<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotJuegaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('juega', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->integer('min_ingreso', FALSE, TRUE);
			$table->integer('min_sustituido', FALSE, TRUE)->nullable();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'partido_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('juega');
	}

}
