<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFutbolistasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('futbolistas', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 80);
			$table->string('apellido', 80);
			$table->date('fecha_nacimiento')->nullable();
			$table->integer('ciudad_id', FALSE, TRUE)->nullable();
			$table->integer('altura')->nullable();
			$table->string('avatar', 256)->nullable();
			$table->timestamps();
			$table->foreign('ciudad_id')
				->references('id')
				->on('ciudades')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('futbolistas');
	}

}
