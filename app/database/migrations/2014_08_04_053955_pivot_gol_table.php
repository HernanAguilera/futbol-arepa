<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotGolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gol', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->integer('minuto', FALSE, TRUE);
			$table->enum('propiapuerta', array('si', 'no'))->default('no')->nullable();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'partido_id', 'minuto'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gol');
	}

}
