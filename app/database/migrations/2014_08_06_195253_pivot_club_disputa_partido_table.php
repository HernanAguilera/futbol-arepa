<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotClubDisputaPartidoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('club_disputa_partido', function(Blueprint $table) {
			$table->integer('club_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->enum('condicion', array('local', 'visitante'));
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->primary(array('club_id', 'partido_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_disputa_partido');
	}

}
