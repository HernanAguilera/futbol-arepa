<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('minuto');
			$table->integer('partido_id', FALSE, TRUE);
			$table->integer('club_id', FALSE, TRUE);
			$table->integer('futbolista_id', FALSE, TRUE)->nullable();
			$table->boolean('propiapuerta')->default(FALSE);
			$table->timestamps();
			$table->foreign('partido_id')
				->references('id')
				->on('partidos')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('club_id')
				->references('id')
				->on('clubes')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('futbolista_id')
				->references('id')
				->on('futbolistas')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goles');
	}

}
