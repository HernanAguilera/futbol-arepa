<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnProximaToFechas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fechas', function(Blueprint $table) {
			$table->boolean('proxima')
				  ->after('actual')
				  ->nullable()
				  ->default(FALSE);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fechas', function(Blueprint $table) {
			$table->dropColumn('proxima');
		});
	}

}
