<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotAmonestadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('amonestado', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->integer('minuto', FALSE, TRUE);
			$table->enum('tipo', array('amarilla', 'roja'));
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'partido_id', 'tipo'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('amonestado');
	}

}
