<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGruposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grupos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 32);
			$table->integer('torneo_id', FALSE, TRUE)->nullable();
			$table->timestamps();
			$table->foreign('torneo_id')
					->references('id')
					->on('torneos')
					->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grupos');
	}

}
