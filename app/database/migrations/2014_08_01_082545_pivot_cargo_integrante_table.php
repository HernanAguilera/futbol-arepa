<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotCargoIntegranteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cargo_integrante', function(Blueprint $table) {
			$table->integer('cargo_id')->unsigned()->index();
			$table->integer('integrante_id')->unsigned()->index();
			$table->foreign('cargo_id')->references('id')->on('cargos_cuerpos_tecnicos')->onDelete('cascade');
			$table->foreign('integrante_id')->references('id')->on('integrantes_cuerpos_tecnicos')->onDelete('cascade');
			$table->primary(array('cargo_id', 'integrante_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cargo_integrante');
	}

}
