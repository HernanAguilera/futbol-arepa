<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesArbitrosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles_de_arbitros', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 80);
			$table->text('descripcion')->nullable();
			$table->date('fecha_implementado')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles_de_arbitros');
	}

}
