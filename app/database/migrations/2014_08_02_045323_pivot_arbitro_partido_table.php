<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotArbitroPartidoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('arbitro_partido', function(Blueprint $table) {
			$table->integer('arbitro_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->integer('rol_id', FALSE, TRUE);
			$table->foreign('arbitro_id')->references('id')->on('arbitros')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->foreign('rol_id')->references('id')->on('roles_de_arbitros')->onDelete('cascade');
			$table->primary(array('arbitro_id', 'partido_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('arbitro_partido');
	}

}
