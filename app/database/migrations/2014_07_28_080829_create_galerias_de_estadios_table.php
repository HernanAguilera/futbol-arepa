<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGaleriasDeEstadiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('galerias_de_estadios', function(Blueprint $table) {
			$table->increments('id');
			$table->string('imagen', 258);
			$table->integer('estadio_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('estadio_id')
				->references('id')
				->on('estadios')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('galerias_de_estadios');
	}

}
