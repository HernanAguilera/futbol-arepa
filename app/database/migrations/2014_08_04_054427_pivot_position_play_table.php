<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotPositionPlayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('positionplay', function(Blueprint $table) {
			$table->integer('futbolista_id')->unsigned()->index();
			$table->integer('partido_id')->unsigned()->index();
			$table->integer('posicion_id')->unsigned()->index();
			$table->integer('minuto')->unsigned()->index();
			$table->foreign('futbolista_id')->references('id')->on('futbolistas')->onDelete('cascade');
			$table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
			$table->foreign('posicion_id')->references('id')->on('posiciones')->onDelete('cascade');
			$table->primary(array('futbolista_id', 'partido_id', 'posicion_id', 'minuto'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('positionplay');
	}

}
