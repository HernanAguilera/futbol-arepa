<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCiudadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ciudades', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->string('abreviatura', 8)->nullable();
			$table->enum('visible', array('si', 'no'))->default('si');
			$table->integer('estado_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('estado_id')
				->references('id')
				->on('estados')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ciudades');
	}

}
