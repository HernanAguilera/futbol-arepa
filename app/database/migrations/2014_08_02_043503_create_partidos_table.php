<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('partidos', function(Blueprint $table) {
			$table->increments('id');
			$table->date('fecha_calendario')->nullable();
			$table->text('nota')->nullable();
			$table->string('video', 256)->nullable();
			$table->integer('fecha_id', FALSE, TRUE);
			$table->integer('estadio_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('fecha_id')
				->references('id')
				->on('fechas')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('estadio_id')
				->references('id')
				->on('estadios')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('partidos');
	}

}
