<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTorneosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('torneos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->date('inicio')->nullable();
			$table->date('fin')->nullable();
			$table->text('descripcion')->nullable();
			$table->integer('temporada_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('temporada_id')
				->references('id')
				->on('temporadas')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('torneos');
	}

}
