<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotClubParticipaEnGrupoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('club_participa_en_grupo', function(Blueprint $table) {
			$table->integer('club_id')->unsigned()->index();
			$table->integer('grupo_id')->unsigned()->index();
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');
			$table->primary(array('club_id', 'grupo_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_participa_en_grupo');
	}

}
