<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemporadasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temporadas', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 128);
			$table->date('inicio')->nullable();
			$table->date('fin')->nullable();
			$table->enum('activa', array('si', 'no'))->default('si');
			$table->integer('pais_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('pais_id')
				->references('id')
				->on('paises')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temporadas');
	}

}
