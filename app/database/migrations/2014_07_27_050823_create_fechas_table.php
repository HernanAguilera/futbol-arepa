<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFechasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fechas', function(Blueprint $table) {
			$table->increments('id');
			$table->string('jornada', 128);
			$table->integer('torneo_id', FALSE, TRUE);
			$table->timestamps();
			$table->foreign('torneo_id')
				->references('id')
				->on('torneos')
				->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fechas');
	}

}
