<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotClubTorneoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('club_participa_en_torneo', function(Blueprint $table) {
			$table->integer('club_id')->unsigned()->index();
			$table->integer('torneo_id')->unsigned()->index();
			$table->foreign('club_id')->references('id')->on('clubes')->onDelete('cascade');
			$table->foreign('torneo_id')->references('id')->on('torneos')->onDelete('cascade');
			$table->primary(array('club_id', 'torneo_id'));
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('club_participa_en_torneo');
	}

}
