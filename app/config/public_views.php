<?php 

return array(


	/**
	 * liga de Primera División a mostrar a los visitantes de la web
	 */

	'primera' => '1',


	/**
	 * liga de Segunda División a mostrar a los visitantes de la web
	 */

	'segunda' => '2',


	/**
	 * liga de Tercera División a mostrar a los visitantes de la web
	 */

	'tercera' => '5',
	

	/**
	 * Copa a mostrar a los visitantes de la web
	 */

	'copa' => '3',


	/**
	 * Copa Internacional a mostrar a los visitantes de la web
	 */

	'internacional' => '4'

	);