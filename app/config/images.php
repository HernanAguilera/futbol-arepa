<?php 

return array(


	/**
	 *	DEFAULTS
	 */

	'AVATAR_DEFAULT' => '/imgs/avatar-desconocido.png',


	/**
	 *	Formatos de imagen
	 */

	'formats' => array(
		'icon'	=> array('height' => 20 ,'width' => 'auto'),
		'thumb' => array('height' => 64 ,'width' => 'auto'),
		'medium'=> array('height' => 'auto' ,'width' => 300),
		'large' => array('height' => 'auto' ,'width' => 600),
	),

	);

 ?>