<?php 

return array(

	array(
		'title' => 'Inicio',
		'url'	=> '/admin',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Roles',
		'url'	=> '/admin/roles',
		'rol'	=> array('SuperAdmin')

	),
	array(
		'title' => 'Usuarios',
		'url'	=> '/admin/users',
		'rol'	=> array('SuperAdmin')
	),
	array(
		'title' => 'Paises',
		'url'	=> '/paises',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Estados',
		'url'	=> '/estados',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Ciudades',
		'url'	=> '/ciudades',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => '',
		'url'	=> '#',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Partidos',
		'url'	=> '/partidos',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Goles',
		'url'	=> '/goles',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Amonestados',
		'url'	=> '/futbolistas-amonestados',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Colegios',
		'url'	=> '/colegios',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Arbitros',
		'url'	=> '/arbitros',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Temporadas',
		'url'	=> '/temporadas',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Categorias',
		'url'	=> '/categorias',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Torneos',
		'url'	=> '/torneos',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Grupos',
		'url'	=> '/grupos',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Fechas',
		'url'	=> '/fechas',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Clubes',
		'url'	=> '/clubes',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Futbolistas',
		'url'	=> '/futbolistas',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Integrantes de cuerpos técnicos',
		'url'	=> '/integrantes-cuerpos-tecnicos',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Estadios',
		'url'	=> '/estadios',
		'rol'	=> array('SuperAdmin', 'Admin', 'Colaborador')
	),
	array(
		'title' => 'Galerias',
		'url'	=> '/galerias/estadios',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	// array(
	// 	'title' => 'Inicio',
	// 	'url'	=> '/admin',
	// 	'rol'	=> array('SuperAdmin', 'Colaborador')
	// ),
	array(
		'title' => 'Cargos cuerpos técnicos',
		'url'	=> '/cargos-cuerpos-tecnicos',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Lesiones',
		'url'	=> '/lesiones',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Posiciones',
		'url'	=> '/posiciones',
		'rol'	=> array('SuperAdmin', 'Admin')
	),
	array(
		'title' => 'Roles de arbitros',
		'url'	=> '/roles-arbitros',
		'rol'	=> array('SuperAdmin', 'Admin',)
	),

);