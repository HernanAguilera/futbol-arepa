<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LfDXPkSAAAAAH5thezePItNHaiMQtcpRGOy5XCe',
	'private_key'	=> '6LfDXPkSAAAAABHizhy-i-EuZ08P_nn4l3wdFz5q',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> '',

	'options' => array(
        'theme' => 'white',
        'lang' => 'es',
    ),
	
);