<?php

use Mockery as m;
use Way\Tests\Factory;

class FutbolistasAmonestadosTest extends TestCase {

	public function __construct()
	{
		$this->mock = m::mock('Eloquent', 'Futbolistas_amonestado');
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp()
	{
		parent::setUp();

		$this->attributes = Factory::futbolistas_amonestado(['id' => 1]);
		$this->app->instance('Futbolistas_amonestado', $this->mock);
	}

	public function tearDown()
	{
		m::close();
	}

	public function testIndex()
	{
		$this->mock->shouldReceive('all')->once()->andReturn($this->collection);
		$this->call('GET', 'futbolistas_amonestados');

		$this->assertViewHas('futbolistas_amonestados');
	}

	public function testCreate()
	{
		$this->call('GET', 'futbolistas_amonestados/create');

		$this->assertResponseOk();
	}

	public function testStore()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(true);
		$this->call('POST', 'futbolistas_amonestados');

		$this->assertRedirectedToRoute('futbolistas_amonestados.index');
	}

	public function testStoreFails()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(false);
		$this->call('POST', 'futbolistas_amonestados');

		$this->assertRedirectedToRoute('futbolistas_amonestados.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow()
	{
		$this->mock->shouldReceive('findOrFail')
				   ->with(1)
				   ->once()
				   ->andReturn($this->attributes);

		$this->call('GET', 'futbolistas_amonestados/1');

		$this->assertViewHas('futbolistas_amonestado');
	}

	public function testEdit()
	{
		$this->collection->id = 1;
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->once()
				   ->andReturn($this->collection);

		$this->call('GET', 'futbolistas_amonestados/1/edit');

		$this->assertViewHas('futbolistas_amonestado');
	}

	public function testUpdate()
	{
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->andReturn(m::mock(['update' => true]));

		$this->validate(true);
		$this->call('PATCH', 'futbolistas_amonestados/1');

		$this->assertRedirectedTo('futbolistas_amonestados/1');
	}

	public function testUpdateFails()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['update' => true]));
		$this->validate(false);
		$this->call('PATCH', 'futbolistas_amonestados/1');

		$this->assertRedirectedTo('futbolistas_amonestados/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testDestroy()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'futbolistas_amonestados/1');
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(['passes' => $bool]));
	}
}
