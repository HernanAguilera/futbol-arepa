<?php

use Mockery as m;
use Way\Tests\Factory;

class TipoDeGolsTest extends TestCase {

	public function __construct()
	{
		$this->mock = m::mock('Eloquent', 'Tipo_de_gol');
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp()
	{
		parent::setUp();

		$this->attributes = Factory::tipo_de_gol(['id' => 1]);
		$this->app->instance('Tipo_de_gol', $this->mock);
	}

	public function tearDown()
	{
		m::close();
	}

	public function testIndex()
	{
		$this->mock->shouldReceive('all')->once()->andReturn($this->collection);
		$this->call('GET', 'tipo_de_gols');

		$this->assertViewHas('tipo_de_gols');
	}

	public function testCreate()
	{
		$this->call('GET', 'tipo_de_gols/create');

		$this->assertResponseOk();
	}

	public function testStore()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(true);
		$this->call('POST', 'tipo_de_gols');

		$this->assertRedirectedToRoute('tipo_de_gols.index');
	}

	public function testStoreFails()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(false);
		$this->call('POST', 'tipo_de_gols');

		$this->assertRedirectedToRoute('tipo_de_gols.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow()
	{
		$this->mock->shouldReceive('findOrFail')
				   ->with(1)
				   ->once()
				   ->andReturn($this->attributes);

		$this->call('GET', 'tipo_de_gols/1');

		$this->assertViewHas('tipo_de_gol');
	}

	public function testEdit()
	{
		$this->collection->id = 1;
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->once()
				   ->andReturn($this->collection);

		$this->call('GET', 'tipo_de_gols/1/edit');

		$this->assertViewHas('tipo_de_gol');
	}

	public function testUpdate()
	{
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->andReturn(m::mock(['update' => true]));

		$this->validate(true);
		$this->call('PATCH', 'tipo_de_gols/1');

		$this->assertRedirectedTo('tipo_de_gols/1');
	}

	public function testUpdateFails()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['update' => true]));
		$this->validate(false);
		$this->call('PATCH', 'tipo_de_gols/1');

		$this->assertRedirectedTo('tipo_de_gols/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testDestroy()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'tipo_de_gols/1');
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(['passes' => $bool]));
	}
}
