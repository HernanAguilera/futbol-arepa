<?php

use Mockery as m;
use Way\Tests\Factory;

class IntegranteCuerpoTecnicosTest extends TestCase {

	public function __construct()
	{
		$this->mock = m::mock('Eloquent', 'Integrante_cuerpo_tecnico');
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp()
	{
		parent::setUp();

		$this->attributes = Factory::integrante_cuerpo_tecnico(['id' => 1]);
		$this->app->instance('Integrante_cuerpo_tecnico', $this->mock);
	}

	public function tearDown()
	{
		m::close();
	}

	public function testIndex()
	{
		$this->mock->shouldReceive('all')->once()->andReturn($this->collection);
		$this->call('GET', 'integrante_cuerpo_tecnicos');

		$this->assertViewHas('integrante_cuerpo_tecnicos');
	}

	public function testCreate()
	{
		$this->call('GET', 'integrante_cuerpo_tecnicos/create');

		$this->assertResponseOk();
	}

	public function testStore()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(true);
		$this->call('POST', 'integrante_cuerpo_tecnicos');

		$this->assertRedirectedToRoute('integrante_cuerpo_tecnicos.index');
	}

	public function testStoreFails()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(false);
		$this->call('POST', 'integrante_cuerpo_tecnicos');

		$this->assertRedirectedToRoute('integrante_cuerpo_tecnicos.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow()
	{
		$this->mock->shouldReceive('findOrFail')
				   ->with(1)
				   ->once()
				   ->andReturn($this->attributes);

		$this->call('GET', 'integrante_cuerpo_tecnicos/1');

		$this->assertViewHas('integrante_cuerpo_tecnico');
	}

	public function testEdit()
	{
		$this->collection->id = 1;
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->once()
				   ->andReturn($this->collection);

		$this->call('GET', 'integrante_cuerpo_tecnicos/1/edit');

		$this->assertViewHas('integrante_cuerpo_tecnico');
	}

	public function testUpdate()
	{
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->andReturn(m::mock(['update' => true]));

		$this->validate(true);
		$this->call('PATCH', 'integrante_cuerpo_tecnicos/1');

		$this->assertRedirectedTo('integrante_cuerpo_tecnicos/1');
	}

	public function testUpdateFails()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['update' => true]));
		$this->validate(false);
		$this->call('PATCH', 'integrante_cuerpo_tecnicos/1');

		$this->assertRedirectedTo('integrante_cuerpo_tecnicos/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testDestroy()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'integrante_cuerpo_tecnicos/1');
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(['passes' => $bool]));
	}
}
