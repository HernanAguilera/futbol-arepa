<?php

use Mockery as m;
use Way\Tests\Factory;

class CargosCuerpoTecnicosTest extends TestCase {

	public function __construct()
	{
		$this->mock = m::mock('Eloquent', 'Cargos_cuerpo_tecnico');
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp()
	{
		parent::setUp();

		$this->attributes = Factory::cargos_cuerpo_tecnico(['id' => 1]);
		$this->app->instance('Cargos_cuerpo_tecnico', $this->mock);
	}

	public function tearDown()
	{
		m::close();
	}

	public function testIndex()
	{
		$this->mock->shouldReceive('all')->once()->andReturn($this->collection);
		$this->call('GET', 'cargos_cuerpo_tecnicos');

		$this->assertViewHas('cargos_cuerpo_tecnicos');
	}

	public function testCreate()
	{
		$this->call('GET', 'cargos_cuerpo_tecnicos/create');

		$this->assertResponseOk();
	}

	public function testStore()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(true);
		$this->call('POST', 'cargos_cuerpo_tecnicos');

		$this->assertRedirectedToRoute('cargos_cuerpo_tecnicos.index');
	}

	public function testStoreFails()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(false);
		$this->call('POST', 'cargos_cuerpo_tecnicos');

		$this->assertRedirectedToRoute('cargos_cuerpo_tecnicos.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow()
	{
		$this->mock->shouldReceive('findOrFail')
				   ->with(1)
				   ->once()
				   ->andReturn($this->attributes);

		$this->call('GET', 'cargos_cuerpo_tecnicos/1');

		$this->assertViewHas('cargos_cuerpo_tecnico');
	}

	public function testEdit()
	{
		$this->collection->id = 1;
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->once()
				   ->andReturn($this->collection);

		$this->call('GET', 'cargos_cuerpo_tecnicos/1/edit');

		$this->assertViewHas('cargos_cuerpo_tecnico');
	}

	public function testUpdate()
	{
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->andReturn(m::mock(['update' => true]));

		$this->validate(true);
		$this->call('PATCH', 'cargos_cuerpo_tecnicos/1');

		$this->assertRedirectedTo('cargos_cuerpo_tecnicos/1');
	}

	public function testUpdateFails()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['update' => true]));
		$this->validate(false);
		$this->call('PATCH', 'cargos_cuerpo_tecnicos/1');

		$this->assertRedirectedTo('cargos_cuerpo_tecnicos/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testDestroy()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'cargos_cuerpo_tecnicos/1');
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(['passes' => $bool]));
	}
}
