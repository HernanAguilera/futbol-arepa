<?php

class Futbolista extends Eloquent {

	protected $table = 'futbolistas';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'apellido' => 'required',
	);

	public static function getFutbolistaByTorneoAndClub($torneo_id, $club_id, $lists = FALSE){
		$torneo = Torneo::find($torneo_id);
		$futbolistas_c = $torneo->futbolistas()->get();
		$futbolistas = array();
		if($lists){
			foreach ($futbolistas_c as $key => $futbolista) {
				if($futbolista->pivot->club_id==$club_id)
					$futbolistas[$futbolista->id] = $futbolista->nombre.' '.$futbolista->apellido;
			}
		}else{
			foreach ($futbolistas_c as $key => $futbolista) {
				if($futbolista->pivot->club_id==$club_id)
					$futbolistas[] = $futbolista;
			}
		}
		return $futbolistas;
	}

	public function ciudad_nacimiento(){
		return $this->belongsTo('Ciudad', 'ciudad_id');
	}

	public function nacionalidades(){
		return $this->belongsToMany('Pais', 'futbolista_pais', 'futbolista_id', 'pais_id');
	}

	public function posiciones(){
		return $this->belongsToMany('Posicion', 'futbolista_posicion', 'futbolista_id', 'posicion_id');
	}

	public function torneos(){
		return $this->belongsToMany('Torneo', 'futbolista_numero_en_torneo', 'futbolista_id', 'torneo_id')->withPivot('club_id', 'numero');
	}

	public function clubes(){
		return $this->belongsToMany('Club', 'club_futbolista', 'futbolista_id', 'club_id');
	}

	public function goles(){
		return $this->hasMany('Gol');
	}

	public function penales(){
		return $this->hasMany('TiroPenal');
	}

	public function futbolistas_amonestados(){
		return $this->hasMany('FutbolistaAmonestado');
	}
}
