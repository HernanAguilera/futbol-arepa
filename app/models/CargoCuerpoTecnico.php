<?php

class CargoCuerpoTecnico extends Eloquent {

	protected $table = 'cargos_cuerpos_tecnicos';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'descripcion' => 'required'
	);
}
