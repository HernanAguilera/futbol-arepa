<?php

class Torneo extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
	);

	public static function ultimosResultadosAll(){
		$partidos = Partido::where('estado_id', '3')
							->orderBy('fecha_calendario', 'des')
							->take(35)->get();
		$grupos = array();
		foreach ($partidos as $key => $partido) {
			list($dia, $hora) = explode(' ', $partido->fecha_calendario);
			$grupos[$dia][$partido->fecha->torneo->nombre][] = $partido->resumen();
		}
		
		return $grupos;
	}

	public static function proximosPartidosAll(){
		$partidos = Partido::where('estado_id', '1')
							->where('fecha_calendario', '>', date('Y-m-d H:i:s'))
							->orderBy('fecha_calendario', 'asc')
							->take(35)->get();
		$grupos = array();
		foreach ($partidos as $key => $partido) {
			list($dia, $hora) = explode(' ', $partido->fecha_calendario);
			$grupos[$dia][$partido->fecha->torneo->nombre][] = $partido->resumen();
		}
		
		return $grupos;
	}

	public function ultimosResultados(){
		$fechaActual = $this->fechas()->where('actual', TRUE)->first();
		return $this->resumenFecha($fechaActual);
	}

	public function proximaFecha(){
		$proximaFecha = $this->fechas()->where('proxima', TRUE)->first();
		return $this->resumenFecha($proximaFecha);
	}

	public function resumenFecha($fecha){
		$partidos = isset($fecha) ? $fecha->partidos()->get() : array();
		$grupos_c = isset($fecha) ? $fecha->grupos()->lists('id') : array();

		$grupos = array();
		foreach ($partidos as $key => $partido) {
			$clubes = $partido->clubes()->get();
			$grupo1 = $clubes[0]->grupos()->where('torneo_id', $fecha->torneo_id)->first();
			$grupo2 = $clubes[1]->grupos()->where('torneo_id', $fecha->torneo_id)->first();
			if(is_object($grupo1) && is_object($grupo2) && $grupo1->id == $grupo2->id)
				$grupos[$grupo1->nombre][] = $partido->resumen();
			else
				$grupos['default'][] = $partido->resumen();
		}
		return $grupos;
	}

	public function goleadores($limit = 10){
		$goleadores = Gol::
						   join('partidos', 'partido_id', '=', 'partidos.id')
						 ->join('fechas', 'partidos.fecha_id', '=', 'fechas.id')
						 ->join('torneos', 'fechas.torneo_id', '=', 'torneos.id')
						 ->join('futbolistas', 'futbolista_id', '=', 'futbolistas.id')
						 ->join('clubes', 'club_id', '=', 'clubes.id')
						 ->where('torneos.id', $this->id)
						 ->where('tipo_id', '!=', '3')
						 ->select(DB::raw('count(*) as anotaciones, futbolistas.nombre, futbolistas.apellido, clubes.id as club'))
						 ->groupBy('futbolista_id')
						 ->orderBy('anotaciones', 'des')
						 ->take($limit)
						 ->get();

		foreach ($goleadores as &$goleador) {
			$club = Club::find($goleador->club);
			$club->getFormats();
			$goleador->escudo = $club->escudo_icon;
		}

		return $goleadores;
	}

	public function posiciones(){
		$grupos_c = $this->grupos()->get(); // grupos colection

		$tabla = array(); // retorno de la función
		if($grupos_c->count()>0){
			foreach ($grupos_c as $grupo) {
				$clubes_c = $grupo->clubes()->get();
				$fechas_ids = $grupo->fechas()->lists('id');
				$tabla[$grupo->nombre] = self::construir_tabla($clubes_c, $fechas_ids);
			}
		}else{
			$clubes_c = $this->clubes()->get();
			$fechas_ids = $this->fechas()->lists('id');
			$tabla[$this->nombre] = self::construir_tabla($clubes_c, $fechas_ids);
		}
		
		return $tabla;
	}


	/**
	 *	CLASE PARA CONTRUIR LAS FILAS DE LA TABLA (REDORDS DE LOS CLUBES)
	 */
	private static function construir_tabla($clubes_c, $fechas_ids){

		// Si aun no hay fechas definidas para el torneo
		if(count($fechas_ids)==0)
			$fechas_ids = array(0);

		$grupo = array();
		foreach ($clubes_c as $club_c) {
			$club = array();
			$club_c->getFormats();
			$club['id'] = $club_c->id;
			$club['nombre'] = $club_c->nombre;
			$club['escudo'] = $club_c->escudo_icon;
			$partidos = $club_c->partidos()->where('estado_id', 3)->whereIn('fecha_id', $fechas_ids)->get();
			$club['PJ'] = $partidos->count();
			$club['PG'] = 0;
			$club['PE'] = 0;
			$club['PP'] = 0;
			$club['GF'] = 0;
			$club['GC'] = 0;
			
			foreach ($partidos as $key => $partido) {
				$GF = $partido->goles()->where('club_id', $club_c->id)->get()->count();
				$GC = $partido->goles()->where('club_id', '!=' ,$club_c->id)->get()->count();
				if($GF>$GC)
					$club['PG']++;
				elseif($GF==$GC)
					$club['PE']++;
				else
					$club['PP']++;

				$club['GF'] += $GF;
				$club['GC'] += $GC;
			}

			$club['Dif'] = $club['GF'] - $club['GC'];
			$club['Pts'] = $club['PG']*3 + $club['PE'];

			$grupo[] = $club;
		}

		return self::ordenar($grupo);
	}


	/**
	 *		ORDENAR LOS REGISTROS DE LA TABLA DE POSICIONES POR PUNTOS, 
	 *	DIFERENCIA DE GOLES Y GOLES A FAVOR
	 */
	private static function ordenar($clubes){

		//	ORDEN ALFABERICO
		for ($i=0; $i < count($clubes); $i++) { 
			for ($j=$i+1; $j < count($clubes); $j++) { 
				if($clubes[$i]['nombre']>$clubes[$j]['nombre']){
					$club_temp = $clubes[$i];
					$clubes[$i] = $clubes[$j];
					$clubes[$j] = $club_temp;
				}
			}
		}

		// ORDENAR POR PUNTOS
		for ($i=0; $i < count($clubes); $i++) { 
			for ($j=$i+1; $j < count($clubes); $j++) { 
				if($clubes[$i]['Pts']<$clubes[$j]['Pts']){
					$club_temp = $clubes[$i];
					$clubes[$i] = $clubes[$j];
					$clubes[$j] = $club_temp;
				}
			}
		}

		// AGRUPAR POR PUNTOS
		$puntos = array();
		foreach ($clubes as $club) {
			if(!isset($puntos[$club['Pts']]))
				$puntos[$club['Pts']] = array($club);
			else
				array_push($puntos[$club['Pts']], $club);
		}


		// ORDENAR POR DIFERENCIA DE GOLES
		foreach ($puntos as &$subpuntos) {
			for ($i=0; $i < count($subpuntos); $i++) { 
				for ($j=$i+1; $j < count($subpuntos); $j++) { 
					if($subpuntos[$i]['Dif']<$subpuntos[$j]['Dif']){
						$pts_temp = $subpuntos[$i];
						$subpuntos[$i] = $subpuntos[$j];
						$subpuntos[$j] = $pts_temp;
					}
				}
			}
		}

		// AGRUPAR POR GOLES A FAVOR
		$diferencias = array();
		foreach ($puntos as $key => &$subpuntos) {
			$subdiferencias = array();
			foreach ($subpuntos as $club) {
				if(!isset($subdiferencias[$club['Dif']]))
					$subdiferencias[$club['Dif']] = array($club);
				else
					array_push($subdiferencias[$club['Dif']], $club);
			}
			$diferencias[$key] = $subdiferencias;
		}

		$puntos = $diferencias;

		// ORDENAR POR GOLES A FAVOR
		foreach ($puntos as &$diferencia) {
			foreach ($diferencia as &$subdiferencias) {
				for ($i=0; $i < count($subdiferencias); $i++) { 
					for ($j=$i+1; $j < count($subdiferencias); $j++) { 
						if($subdiferencias[$i]['GF']<$subdiferencias[$j]['GF']){
							$pts_temp = $subdiferencias[$i];
							$subdiferencias[$i] = $subdiferencias[$j];
							$subdiferencias[$j] = $pts_temp;
						}
					}
				}
			}
		}

		$clubes = array();
		foreach ($puntos as &$diferencias) {
			foreach ($diferencias as &$subdiferencias) {
				$clubes = array_merge($clubes, $subdiferencias);
			}
		}

		return $clubes;
	}


	/**
	 *	DEFINICION DE RELACIONES
	 */

	public function grupos(){
		return $this->hasMany('Grupo');
	}

	public function fechas(){
		return $this->hasMany('Fecha');
	}

	public function categoria(){
		return $this->belongsTo('Categoria');
	}

	public function temporada(){
		return $this->belongsTo('Temporada');
	}

	public function futbolistas(){
		return $this->belongsToMany('Futbolista', 'futbolista_numero_en_torneo', 'torneo_id', 'futbolista_id')->withPivot('club_id', 'numero');
	}

	public function clubes(){
		return $this->belongsToMany('Club', 'club_participa_en_torneo', 'torneo_id', 'club_id');
	}
}
