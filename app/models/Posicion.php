<?php

class Posicion extends Eloquent {

	protected $table = 'posiciones';

	protected $guarded = array();

	public static $rules = array(
		'descripcion' => 'required',
		'abreviatura' => 'required'
	);

	public function futbolistas(){
		return $this->belongsToMany('Futbolista', 'futbolista_posicion', 'futbolista_id', 'posicion_id');
	}
}
