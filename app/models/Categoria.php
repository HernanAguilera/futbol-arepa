<?php

class Categoria extends Eloquent {

	protected $table = 'categorias';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'abreviatura' => 'required',
		'pais_id' => 'required'
	);

	public function pais(){
		return $this->BelongsTo('Pais');
	}

	public function torneos(){
		return $this->hasMany('Torneo');
	}
}
