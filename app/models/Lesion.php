<?php

class Lesion extends Eloquent {

	protected $table = 'lesiones';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
	);
}
