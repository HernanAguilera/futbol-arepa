<?php

class Arbitro extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'apellido' => 'required',
	);

	protected function colegio(){
		return $this->belongsTo('Colegio');
	}
}
