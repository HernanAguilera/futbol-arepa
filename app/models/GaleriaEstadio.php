<?php

class GaleriaEstadio extends Eloquent {

	protected $table = 'galerias_de_estadios';

	protected $guarded = array();

	public static $rules = array(
		'imagen' => 'required',
		'estadio_id' => 'required'
	);
}
