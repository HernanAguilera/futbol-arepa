<?php

class Ciudad extends Eloquent {

	protected $table = 'ciudades';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'estado_id' => 'required'
	);

	public function lists($value, $key = NULL){
		if(is_null($key))
			return $this->groupBy('nombre')->lists($value);
		return $this->groupBy('nombre')->lists($value, $key);
	}

	public function estado(){
		return $this->belognsTo('Estado');
	}

	public function estadios(){
		return $this->hasMany('Estadio');
	}

	public function futbolistas_oriundos(){
		return $this->hasMany('Futbolista');
	}
}
