<?php

class TiroPenal extends Eloquent {

	protected $table = 'tiros_penales';

	protected $guarded = array();

	public static $rules = array(
		'partido_id' => 'required',
		'club_id' => 'required',
		'anotado' => 'required',
		'orden' => 'required'
	);

	public function partido(){
		return $this->belongsTo('Partido');
	}

	public function futbolista(){
		return $this->belongsTo('Futbolista');
	}

	public function club(){
		return $this->belongsTo('Club');
	}
}
