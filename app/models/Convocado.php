<?php

class Convocado extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'futbolista_id' => 'required',
		'partido_id' => 'required'
	);
}
