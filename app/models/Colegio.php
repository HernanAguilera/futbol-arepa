<?php

class Colegio extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'estado_id' => 'required'
	);

	public function estado(){
		return $this->belongsTo('Estado');
	}

	public function arbitros(){
		return $this->hasMany('Arbitro');
	}
}
