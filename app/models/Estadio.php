<?php

class Estadio extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
	);

	public function lists($value, $key = NULL){
		if(is_null($key))
			return $this->groupBy($value)->lists($value);
		return $this->groupBy($value)->lists($value, $key);
	}

	public function ciudad(){
		return $this->belongsTo('Ciudad');
	}

	public function clubes(){
		return $this->hasMany('Club');
	}

	public function partidos(){
		return $this->hasMany('Partido');
	}
}
