<?php

class Club extends Eloquent {

	protected $table = 'clubes';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required'
	);

	public function getFormats(){
		if(!empty($this->escudo)){
			foreach (Config::get('images.formats') as $formatName => $format) {
				$file_format = dirname($this->escudo).'/'.$formatName.'/'.basename($this->escudo);
				if(is_file(public_path().$file_format))
					$this['escudo_'.$formatName] = $file_format;
				else
					$this['escudo_'.$formatName] = '';
			}
		}
	}

	public function lists($value, $key = NULL){
		if(is_null($key))
			return $this->groupBy($value)->lists($value);
		return $this->groupBy($value)->lists($value, $key);
	}

	public function goles(){
		return $this->hasMany('Gol');
	}

	public function penales(){
		return $this->hasMany('TiroPenal');
	}

	public function futbolistas_amonestados(){
		return $this->hasMany('FutbolistaAmonestado');
	}

	public function estadio(){
		return $this->belongsTo('Estadio');
	}

	public function estado(){
		return $this->belongsTo('Estado');
	}

	public function torneos(){
		return $this->belongsToMany('Torneo', 'club_participa_en_torneo', 'club_id', 'torneo_id');
	}

	public function futbolistas(){
		return $this->belongsToMany('Futbolista', 'club_futbolista', 'club_id', 'futbolista_id');
	}

	public function partidos(){
		return $this->belongsToMany('Partido', 'club_disputa_partido', 'club_id', 'partido_id')->withPivot('condicion');
	}

	public function grupos(){
		return $this->belongsToMany('Grupo', 'club_participa_en_grupo', 'club_id', 'grupo_id');
	}
}
