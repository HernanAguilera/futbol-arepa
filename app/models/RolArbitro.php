<?php

class RolArbitro extends Eloquent {

	protected $table = "roles_de_arbitros";

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
	);
}
