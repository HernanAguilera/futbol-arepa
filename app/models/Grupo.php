<?php

class Grupo extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'torneo_id' => 'required'
	);

	public function torneo(){
		return $this->BelongsTo('Torneo');
	}

	public function clubes(){
		return $this->belongsToMany('Club', 'club_participa_en_grupo', 'grupo_id', 'club_id');
	}

	public function fechas(){
		return $this->belongsToMany('Fecha');
	}

}