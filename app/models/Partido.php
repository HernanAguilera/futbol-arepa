<?php

class Partido extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'fecha_id' => 'required',
		'estadio_id' => 'required'
	);

	public function resumen(){

		$clubes_pivot = $this->clubes()->get();

		$clubes = array();
		$goles = array();
		$penales = array();

		foreach ($clubes_pivot as $club) {	
			$clubes[$club->pivot->condicion] = Club::find($club->pivot->club_id);
			$clubes[$club->pivot->condicion]->getFormats();
			$goles[$club->pivot->condicion] = $this->getGoles($this->id, $club->pivot->club_id);
			$penales[$club->pivot->condicion] = TiroPenal::where('partido_id', $this->id)
														 ->where('club_id', $club->pivot->club_id)
														 ->where('anotado', '1')
														 ->orderBy('orden')
														 ->get()->count();
		}
		$resumen = new Stdclass();
		$resumen->clubes = $clubes;
		$resumen->goles = $goles;
		$resumen->penales = $penales;
		$resumen->horario = $this->fecha_calendario;
		$resumen->reprogramado = $this->reprogramado;
		$resumen->suspendido = $this->estado()->first()->id == 4 ? TRUE : FALSE;
		$resumen->enjuego = $this->estado()->first()->id == 2 ? TRUE : FALSE;
		$resumen->finalizado = $this->estado()->first()->id == 3 ? TRUE : FALSE;
		$resumen->estadio = $this->estadio->nombre;
		$resumen->nota = $this->nota;
		$resumen->video = $this->video;

		return $resumen;
	}

	public function detalles($id){

		$partido 	= $this->find($id);

		$tipos 		= TipoDeGol::lists('descripcion', 'id');
		$estadio 	= $partido->estadio()->first();
		$fecha 		= $partido->fecha()->first();
		$torneo 	= $fecha->torneo()->first();

		$clubes_pivot = $partido->clubes()->get();

		$clubes = array();
		$futbolistas = array();
		$goles = array();
		$amonestados = array();
		$penales = array();

		foreach ($clubes_pivot as $club) {	
			$clubes[$club->pivot->condicion] = Club::find($club->pivot->club_id);
			$clubes[$club->pivot->condicion]->getFormats();
			$futbolistas[$club->pivot->condicion] = array(0 => '  --- Desconocido ---   ') + Futbolista::getFutbolistaByTorneoAndClub($torneo->id, $club->id, TRUE);

			$goles[$club->pivot->condicion] = $this->getGoles($id, $club->pivot->club_id);

			$amonestados[$club->pivot->condicion] = FutbolistaAmonestado::where('partido_id', $id)
																		->where('club_id', $club->pivot->club_id)
																		->orderBy('tipo')
																		->orderBy('tiempo', 'asc')
																		->orderBy('minuto', 'asc')
																		->get();

			$penales[$club->pivot->condicion] = TiroPenal::where('partido_id', $id)
														 ->where('club_id', $club->pivot->club_id)
														 ->orderBy('orden')
														 ->get();
			$penales[$club->pivot->condicion]->anotados = TiroPenal::where('partido_id', $id)
														 ->where('club_id', $club->pivot->club_id)
														 ->where('anotado', '1')
														 ->orderBy('orden')
														 ->get()->count();
		}

		// grupos
		$grupo = '';
		$clubes_g = $partido->clubes()->get();
		$grupo1 = $clubes_g[0]->grupos()->where('torneo_id', $partido->fecha->torneo_id)->first();
		$grupo2 = $clubes_g[1]->grupos()->where('torneo_id', $partido->fecha->torneo_id)->first();
		if(is_object($grupo1) && is_object($grupo2) && $grupo1->id == $grupo2->id)
			$grupo = $grupo1->nombre;

		return compact('partido',
	  				   'estadio',
	  				   'fecha',
	  				   'torneo',
	  				   'clubes',
	  				   'tipos',
	  				   'futbolistas',
	  				   'goles',
	  				   'amonestados',
	  				   'penales',
	  				   'grupo'
	  			);
	}

	/**
	 *	{
	 *		goles: [ numeric
	 *					{
	 *						futbolista_id:integer, 
	 *						anotaciones: [
	 *										{ 
	 *											id: integer
	 *											tiempo: (1T, 2T, 1TEX, 2TEX), 
	 *											minuto: integer, 
	 *											tipo: (normal, penal, autogol) 
	 *										},
	 *										{ 
	 *											id: integer
	 *											tiempo: (1T, 2T, 1TEX, 2TEX), 
	 *											minuto: integer, 
	 *											tipo: (normal, penal, autogol) 
	 *										},...
	 *									]
	 *						contrario: boolean
	 *					},...
	 *			],
	 *		cantidad: integer
	 *	}
	 */

	private function getGoles($partido_id, $club_id){
			$goles_c = Gol::where('partido_id', $partido_id)
						->where('club_id', $club_id)
						->orderBy('tiempo', 'asc')
						->orderBy('minuto', 'asc')
						->get();

		$anotador  = new stdClass();
		$anotador->cantidad = $goles_c->count();
		$anotador->goles = array();
		$anotador->anotacion_nula = FALSE;

		$goles_agrupados = array();
		$anotados_en_contra = array();

		foreach ($goles_c as $key => $gol) {
			$anotacion = new stdClass();
			$anotacion->id = $gol->id;
			$anotacion->tiempo = $gol->tiempo;
			if($gol->minuto<=0 || empty($gol->futbolista_id))
				$anotador->anotacion_nula = TRUE;
			
			switch ($gol->tiempo) {
				case '2T':
					$anotacion->minuto = ($gol->minuto<=45) ? ($gol->minuto + 45) : (string)(90).'+'.(string)($gol->minuto - 45);
					break;
				case '1TEX':
					$anotacion->minuto = ($gol->minuto<=45) ? ($gol->minuto + 90) : (string)(105).'+'.(string)($gol->minuto - 45);
					break;
				case '2TEX':
					$anotacion->minuto = ($gol->minuto<=45) ? ($gol->minuto + 105) : (string)(105).'+'.(string)($gol->minuto - 45);
					break;
				
				default:
					$anotacion->minuto = ($gol->minuto<=45) ? $gol->minuto : (string)(45).'+'.(string)($gol->minuto - 45);
					break;
			}

			if($gol->tipo()->first()->descripcion=='Penal')
				$anotacion->tipo = '(P)';
			elseif($gol->tipo()->first()->descripcion=='Autogol')
				$anotacion->tipo = '(AG)';
			else
				$anotacion->tipo = '';

			if(isset($goles_agrupados[$gol->futbolista_id])){
				array_push($goles_agrupados[$gol->futbolista_id], $anotacion);
			}else{
				$goles_agrupados[$gol->futbolista_id] = array($anotacion);
				$anotados_en_contra[$gol->futbolista_id] = ($anotacion->tipo == '(AG)');
			}
		}

		foreach ($goles_agrupados as $key => $grupo_goles) {
					$gol_temp = new stdClass();
					$futbolistas_obj = Futbolista::find($key);
					$gol_temp->futbolista = $futbolistas_obj ? "$futbolistas_obj->nombre $futbolistas_obj->apellido" : '';
					$gol_temp->futbolista_id = $key;
					$gol_temp->anotaciones = $grupo_goles;
					$gol_temp->contrario = $anotados_en_contra[$key];
					array_push($anotador->goles, $gol_temp);
				}
		return $anotador;
	}


	/***********************************************************************
	 ****************************** RELACIONES *****************************
	 ***********************************************************************/

	public function estado(){
		return $this->belongsTo('EstadoPartido');
	}

	public function estadio(){
		return $this->belongsTo('Estadio');
	}

	public function fecha(){
		return $this->belongsTo('Fecha');
	}

	public function goles(){
		return $this->hasMany('Gol');
	}

	public function penales(){
		return $this->hasMany('TiroPenal');
	}

	public function futbolistas_amonestados(){
		return $this->hasMany('FutbolistaAmonestado');
	}

	public function clubes(){
		return $this->belongsToMany('Club', 'club_disputa_partido', 'partido_id', 'club_id')->withPivot('condicion');
	}

	public function arbitros(){
		return $this->belongsToMany('Arbitro', 'arbitro_partido', 'partido_id', 'arbitro_id')->withPivot('rol_id');
	}

	public function ct_suspendidos(){
		return $this->belongsToMany('integrantes_cuerpos_tecnicos', 'integrantect_amonestado', 'partido_id', 'integrantect_id')->withPivot('club_id');
	}
}
