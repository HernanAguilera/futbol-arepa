<?php

class Gol extends Eloquent {

	protected $table = 'goles';

	protected $guarded = array();

	public static $rules = array(
		'minuto' => 'required',
		'partido_id' => 'required',
		'club_id' => 'required',
	);

	public function partido(){
		return $this->belongsTo('Partido');
	}

	public function club(){
		return $this->belongsTo('Club');
	}

	public function futbolista(){
		return $this->belongsTo('Futbolista');
	}

	public function tipo(){
		return $this->belongsTo('TipoDeGol');
	}
}
