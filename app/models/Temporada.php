<?php

class Temporada extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'inicio' => 'required',
		'fin' => 'required'
	);

	public static function actual($pais_id){
		
		if(!is_integer($pais_id))
			return 0;

		$actual = Fecha::where('actual', 'si')
						->where('pais_id', $pais_id)
						->get();
		
		if($actual->count()>0)
			return $actual->id;

		return 0;
	}

	public function torneos(){
		return $this->hasMany('Torneo');
	}
}
