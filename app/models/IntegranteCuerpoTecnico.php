<?php

class IntegranteCuerpoTecnico extends Eloquent {

	protected $table = 'integrantes_cuerpos_tecnicos';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'apellido' => 'required',
	);
}
