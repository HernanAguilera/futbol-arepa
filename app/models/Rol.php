<?php

class Rol extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = array('title');

	public function users(){
		return $this->hasMany('User');
	}

}