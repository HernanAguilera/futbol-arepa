<?php

class FutbolistaAmonestado extends Eloquent {

	protected $table = 'futbolistas_amonestados';

	protected $guarded = array();

	public static $rules = array(
		'futbolista_id' => 'required',
		'partido_id' => 'required',
		'tipo' => 'required',
		'minuto' => 'required',
		'tiempo' => 'required'
	);

	public function partido(){
		return $this->belongsTo('Partido');
	}

	public function club(){
		return $this->belongsTo('Club');
	}

	public function futbolista(){
		return $this->belongsTo('Futbolista');
	}
}
