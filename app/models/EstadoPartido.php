<?php

class EstadoPartido extends Eloquent {

	protected $table = 'estados_de_partidos';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'abreviatura' => 'required'
	);

	public function partidos(){
		return $this->hasMany('Partido');
	}
}
