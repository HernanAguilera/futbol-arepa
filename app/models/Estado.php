<?php

class Estado extends Eloquent {

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'pais_id' => 'required'
	);

	public function lists($value, $key = NULL){
		if(is_null($key))
			return $this->groupBy($value)->lists($value);
		return $this->groupBy($value)->lists($value, $key);
	}

	public function pais(){
		return $this->belongsTo('Pais');
	}

	public function ciudades(){
		return $this->hasMany('Ciudad');
	}

	public function colegio(){
		return $this->hasOne('Colegio');
	}

	public function clubes(){
		return $this->hasMany('Club');
	}
}
