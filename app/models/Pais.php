<?php

class Pais extends Eloquent {

	protected $table = 'paises';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
	);

	public function lists($value, $key = NULL){
		if(is_null($key))
			return $this->groupBy('nombre')->lists($value);
		return $this->groupBy('nombre')->lists($value, $key);
	}

	public function estados(){
		return $this->hasMany('Estado');
	}

	public function futbolistas(){
		return $this->belongsToMany('Futbolista', 'futbolista_pais', 'futbolista_id', 'pais_id');
	}

	public function categorias(){
		return $this->hasMany('Categoria');
	}
}
