<?php

class Fecha extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'jornada' => 'required',
		'torneo_id' => 'required'
	);

	public static function actual($torneo_id){
		
		if(!is_integer($torneo_id))
			return 0;

		$actual = Fecha::where('actual', '1')
						->where('torneo_id', $torneo_id)
						->get();
		
		if($actual->count()>0)
			return $actual->id;

		return 0;
	}

	public function actualizarCache(){
		Cache::forever('tabla'.$this->torneo_id, Torneo::find($this->torneo_id)->posiciones());
		Cache::forever('goleadores'.$this->torneo_id, Torneo::find($this->torneo_id)->goleadores());
		Cache::forever('fechas'.$this->torneo_id, Torneo::find($this->torneo_id)->fechas()->where('visible', TRUE)->lists('abreviatura', 'id'));
		Cache::forever('fecha'.$this->id, Torneo::find($this->torneo_id)->resumenFecha($this));
		// Cache::forever('fecha'.$this->id, Torneo::resumenFecha($this));
		Cache::forever('ultimos_partidos_all', Torneo::ultimosResultadosAll());
		Cache::forever('proximos_partidos_all', Torneo::proximosPartidosAll());

		if($this->actual){
			Cache::forever('fecha_actual'.$this->torneo_id, Torneo::find($this->torneo_id)->fechas()->where('actual','1')->first());
			Cache::forever('ultimos_resultados'.$this->torneo_id, Torneo::find($this->torneo_id)->ultimosResultados());
		}
	}

	public function partidos(){
		return $this->hasMany('Partido');
	}

	public function torneo(){
		return $this->belongsTo('Torneo');
	}

	public function grupos(){
		return $this->belongsToMany('Grupo');
	}
}
