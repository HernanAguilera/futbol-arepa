<?php

class TipoDeGol extends Eloquent {

	protected $table = 'tipo_de_gol';

	protected $guarded = array();

	public static $rules = array(
		'descripcion' => 'required'
	);

	public function goles(){
		return $this->hasMany('Gol');
	}
}
