<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'guest'), function(){
	
	Route::get('login', function(){
		return View::make('auth.login');
	});

	Route::post('login', 'AuthController@login');

	Route::controller('password', 'RemindersController');
});



Route::get('logout', array('before' => 'auth', 'uses' => 'AuthController@logout'));

Route::get('admin', array('before' => 'auth', function(){
	return View::make('auth.admin');	
}));


Route::group(array('before' => 'auth'), function(){

	Route::group(array('before' => 'is_admin'), function(){
		Route::resource('admin/roles', 'RolesController');
		Route::resource('admin/users', 'UsersController');
	});

	Route::resource('paises', 'PaisesController');

	Route::resource('estados', 'EstadosController');

	Route::resource('ciudades', 'CiudadesController');

	Route::resource('arbitros', 'ArbitrosController');

	Route::resource('temporadas', 'TemporadasController');

	Route::resource('torneos', 'TorneosController');

	Route::resource('estadios', 'EstadiosController');

	Route::resource('fechas', 'FechasController');
	// return JSON
	Route::get('json/fechas/torneo/{torneo_id}', 'FechasController@busquedaPorIdTorneo');

	Route::resource('posiciones', 'PosicionesController');

	Route::resource('futbolistas', 'FutbolistasController');

	Route::resource('galerias/estadios', 'GaleriasEstadiosController');

	Route::resource('clubes', 'ClubesController');
	Route::get('clubes-image/avatar/all-generate', 'ClubesController@generateAllFormatFile');

	Route::resource('categorias', 'CategoriasController');

	Route::resource('lesiones', 'LesionesController');

	Route::resource('cargos-cuerpos-tecnicos', 'CargosCuerposTecnicosController');

	Route::resource('integrantes-cuerpos-tecnicos', 'IntegrantesCuerposTecnicosController');

	Route::resource('partidos', 'PartidosController');
	// return JSON
	Route::get('json/partidos/fecha/{fecha_id}', 'PartidosController@busqueda');

	Route::resource('roles-arbitros', 'RolesArbitrosController');

	Route::resource('convocados', 'ConvocadosController');

	Route::resource('colegios', 'ColegiosController');
	
	Route::resource('goles', 'GolesController');

	Route::resource('tipo-de-gol', 'TipoDeGolController');

	Route::resource('futbolistas-amonestados', 'FutbolistasAmonestadosController');

	Route::resource('grupos', 'GruposController');

	Route::resource('estados-partidos', 'EstadosPartidosController');

	Route::resource('tiros-penales', 'TirosPenalesController');

	Route::get('en-juego/partido/{id}', 'EnJuegoController@partido');
	Route::post('en-juego/store/{id}', 'EnJuegoController@store');

	Route::get('contrato/futbolista/{id}', 'FutbolistaContratoController@create');
	Route::post('contrato/futbolista/store', 'FutbolistaContratoController@store');
	Route::post('contrato/futbolista/{id}', 'FutbolistaContratoController@update');
	Route::get('contrato/futbolista/{futbolista_id}/torneo/{torneo_id}/edit', 'FutbolistaContratoController@edit');
	// return JSON
	Route::get('contrato/torneo/{torneo_id}/club/{club_id}', 'FutbolistaContratoController@getFutbolistaByTorneoAndClub');

	Route::get('contrato/integrante-cuerpo-tecnico/{id}', 'CuerpoTecnicoContratoController@create');
	Route::post('contrato/integrante-cuerpo-tecnico/store', 'CuerpoTecnicoContratoController@store');
	Route::get('contrato/integrante-cuerpo-tecnico/{integrante_id}/torneo/{torneo_id}', 'CuerpoTecnicoContratoController@edit');

	Route::any('query/futbolistas/{query}', 'FutbolistasController@busqueda');

	// Route::post('/twitter/enviar-estado', 'TweetsController@enviarEstado');
	Route::post('tweet/partido/status', 'TweetsController@statusPartido');
	Route::post('tweet/partido/gol', 'TweetsController@gol');
	Route::post('tweet/partido/ifmt', 'TweetsController@IFMT');


	Route::get('cache/flush', function(){
		Cache::flush();
	});

});

Route::get('contacto', function(){
	return View::make('public.contacto', array('titleHeader' => 'Contacto'));
});
Route::post('contacto', 'PublicController@contacto');

Route::get('/', 'PublicController@index');

Route::get('primera-division',  'PublicController@primera');
Route::get('primera-division/fechas', 'PublicController@primeraFechas');
Route::get('primera-division/fechas/{fecha_id}', 'PublicController@primeraFecha');

Route::get('segunda-division', 'PublicController@segunda');
Route::get('segunda-division/fechas', 'PublicController@segundaFechas');
Route::get('segunda-division/fechas/{fecha_id}', 'PublicController@segundaFecha');

Route::get('tercera-division', 'PublicController@tercera');
Route::get('tercera-division/fechas', 'PublicController@terceraFechas');
Route::get('tercera-division/fechas/{fecha_id}', 'PublicController@terceraFecha');

Route::get('copa-venezuela', 'PublicController@copaFechas');
Route::get('copa-venezuela/fechas/{fecha_id}', 'PublicController@copaFecha');