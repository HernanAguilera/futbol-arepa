
F_AREPA.createNS("F_AREPA.LOGIC");

F_AREPA.LOGIC.amonestar = function(){

	boton_amarilla_local = $('#amarilla-local');
	boton_roja_local = $('#roja-local');

	boton_amarilla_visitante = $('#amarilla-visitante');
	boton_roja_visitante = $('#roja-visitante');

	click_callback = function(evt){
		boton = evt.target

		switch(boton.id) {
		    case 'amarilla-local':
		    	club = $('#local').val();
		    	amonestado_id = $( "#amonestado-local option:selected" ).attr('value');
		        color = 'amarilla';
		        break;
		    case 'roja-local':
		    	club = $('#local').val();
		    	amonestado_id = $( "#amonestado-local option:selected" ).attr('value');
		        color = 'roja';
		        break;
		    case 'amarilla-visitante':
		    	club = $('#visitante').val();
		    	amonestado_id = $( "#amonestado-visitante option:selected" ).attr('value');
		        color = 'amarilla';
		        break;
		    case 'roja-visitante':
		    	club = $('#visitante').val();
		    	amonestado_id = $( "#amonestado-visitante option:selected" ).attr('value');
		        color = 'roja'; 
		        break;
		} 

		direccion = '/futbolistas-amonestados';

		datos = {
			minuto			: $('#minuto').val(),
			tiempo 			: $('#tiempo option:selected').attr('value'),
			partido_id		: F_AREPA.UTILITIES.url(),
			club_id			: club,
			futbolista_id	: amonestado_id,
			tipo			: color,
		}

		F_AREPA.UTILITIES.AJAX.post(direccion, datos);
	}

	boton_amarilla_local.on('click', click_callback);
	boton_roja_local.on('click', click_callback);
	boton_amarilla_visitante.on('click', click_callback);
	boton_roja_visitante.on('click', click_callback);

}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.LOGIC.amonestar);