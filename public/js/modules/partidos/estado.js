F_AREPA.createNS("F_AREPA.LOGIC");

F_AREPA.LOGIC.estado = function(){

	boton_status = $("#status");

	click_callback = function(evt){

		if($("#tweet").is(':checked')) {

			direccion = '/tweet/partido/status';

			datos = {
				partido_id: F_AREPA.UTILITIES.url(),
	            minuto: $('#minuto').val(),
	            tiempo: $('#tiempo option:selected').attr('value'),
			}

			F_AREPA.UTILITIES.AJAX.post(direccion, datos);
		}
	}

	boton_status.on('click', click_callback);

}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.LOGIC.estado);