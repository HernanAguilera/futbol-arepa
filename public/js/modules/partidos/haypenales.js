F_AREPA.createNS("F_AREPA.UTILITIES.PARTIDOS");

F_AREPA.UTILITIES.PARTIDOS.haypenales = function(){

	en_juego = $('#en-juego');
	tanda_penales = $('#tanda-penales');

	tanda_penales.hide()

	$('#haypenales option').on('click', function(evt){
		if(evt.target.value == 1){
			en_juego.hide()
			tanda_penales.show()
		}else{
			en_juego.show()
			tanda_penales.hide()
		}
	})

}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.UTILITIES.PARTIDOS.haypenales);