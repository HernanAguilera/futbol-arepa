F_AREPA.createNS("F_AREPA.LOGIC");

F_AREPA.LOGIC.penales = function(){

	boton_penal_local 		= $("#penal-local");
	boton_penal_visitante 	= $("#penal-visitante");

	click_callback = function(evt){
		boton = evt.target;

		if(boton.id=='penal-local'){

			anotador = $("#anotador-penal-local option:selected").attr('value');
			club = $('#local').val();
			fue_anotado = $("#anotado-local").is(':checked') ? '1' : '0';
			penal_orden = $('#orden-local').val();

		}else{

			anotador = $("#anotador-penal-visitante option:selected").attr('value');
			club = $('#visitante').val();
			fue_anotado = $("#anotado-visitante").is(':checked') ? '1' : '0';
			penal_orden = $('#orden-visitante').val();

		}

		direccion = '/tiros-penales';

		datos = {
			partido_id		: F_AREPA.UTILITIES.url(),
			futbolista_id	: anotador,
			club_id			: club,
			anotado			: fue_anotado,
			orden			: penal_orden,
		};

		if(datos.futbolista_id==0) delete datos.futbolista_id;

		F_AREPA.UTILITIES.AJAX.post(direccion, datos);

	}

	boton_penal_local.on('click', click_callback);
	boton_penal_visitante.on('click', click_callback);
}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.LOGIC.penales);