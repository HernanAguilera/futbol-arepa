
F_AREPA.createNS("F_AREPA.UTILITIES.PARTIDOS");

F_AREPA.UTILITIES.PARTIDOS.selectFilter = function(){

	$('#torneo option').on('click', function(evt){
		
		if(evt.target.value!=0){
			torneo_id = evt.target.value;

			url = '/json/fechas/torneo/'+torneo_id;
			url_template = '/js/modules/fechas/templates/list.options.mst';

			F_AREPA.UTILITIES.AJAX.MUSTACHE.get(url, url_template, '#fecha', 'fechas', cb);

		}
	})

	cb = function(){

		$('#fecha option').on('click', function(evt){
			if(evt.target.value!=0){
				fecha_id = evt.target.value;

				url = '/json/partidos/fecha/'+fecha_id;

				url_template = '/js/modules/partidos/templates/list.mst';

				F_AREPA.UTILITIES.AJAX.MUSTACHE.get(url, url_template, '#partidos', 'partidos');
			}
		})

	}

}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.UTILITIES.PARTIDOS.selectFilter);