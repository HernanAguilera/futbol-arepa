
F_AREPA.createNS("F_AREPA.LOGIC");

F_AREPA.LOGIC.anotarGol = function(){

	boton_gol_local = $('#gol-local');
	boton_gol_visitante = $('#gol-visitante');

	click_callback = function(evt){
		boton = evt.target;

		if(boton.id=='gol-local'){
			tipo_de_gol = $( "#tipo-gol-local option:selected" ).attr('value');
			anotador_id = $( "#anotador-local option:selected" ).attr('value');

			if(tipo_de_gol==3)
				club = $('#visitante').val();
			else
				club = $('#local').val();

		}else{
			tipo_de_gol = $( "#tipo-gol-visitante option:selected" ).attr('value');
			anotador_id = $( "#anotador-visitante option:selected" ).attr('value');

			if(tipo_de_gol==3)
				club = $('#local').val();
			else
				club = $('#visitante').val();

		}

		direccion = '/goles';

		datos = {
			minuto			: $('#minuto').val(),
			tiempo 			: $('#tiempo option:selected').attr('value'),
			partido_id		: F_AREPA.UTILITIES.url(),
			club_id			: club,
			futbolista_id	: anotador_id,
			tipo_id			: tipo_de_gol,
		}

		if(datos.futbolista_id==0) delete datos.futbolista_id;
		if(datos.minuto.length==0) datos.minuto = 0;
		if($("#tweet").is(':checked')) datos.tweet = true;

		F_AREPA.UTILITIES.AJAX.post(direccion, datos);

	}

	boton_gol_local.on('click', click_callback);
	boton_gol_visitante.on('click', click_callback);
}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.LOGIC.anotarGol);