F_AREPA.createNS("F_AREPA.LOGIC");

F_AREPA.LOGIC.periodo = function(){

	boton_periodo = $("#btn-periodo");

	click_callback = function(evt){

		if($("#tweet").is(':checked')) {

			direccion = '/tweet/partido/status';

			datos = {
					partido_id: F_AREPA.UTILITIES.url(),
		            minuto: $('#minuto').val(),
		            tiempo: $('#periodo option:selected').attr('value'),
				}

			F_AREPA.UTILITIES.AJAX.post(direccion, datos);

		}

	}

	boton_periodo.on('click', click_callback);

}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.LOGIC.periodo);