
// funcion que controla los eventos en el select de torneos
var getFubolistasByClubesAndTorneos = function(){

	var evento = function(evt){
		
		// Obteniendo las variables
		club_id = F_AREPA.UTILITIES.url();
		torneo_id = evt.target.value;

		if(torneo_id!=0){
			// Contruyendo la url a consultar
			url = '/contrato/torneo/'+torneo_id+'/club/'+club_id;

			// plantilla
			url_template = '/js/modules/futbolistas/templates/list.mst';

			// AJAX
			F_AREPA.UTILITIES.AJAX.MUSTACHE.get(url, url_template, '#futbolistas', 'futbolistas');
		}
	}

	F_AREPA.UTILITIES.selectSearch('#torneos', evento);
}

F_AREPA.CONFIG.coleccion.agregar(getFubolistasByClubesAndTorneos);