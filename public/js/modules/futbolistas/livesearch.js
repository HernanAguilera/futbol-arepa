
F_AREPA.createNS("F_AREPA.LOGIC");

F_AREPA.LOGIC.futbolistas = function(){

	// esta variable almacena que futbolista ha sido selecionado
	var futbolista_id = 0;

	// funcion que se ejecuta cuando una opcion es seleccionada en livesearch
	var rellenar_campos_modal = function(jq, sug, data){
		futbolista_id = sug.id;
		$('#avatar').attr('src', sug.avatar);
		$('#table-futbolista #nombre').html(sug.nombre);
		$('#table-futbolista #apellido').html(sug.apellido);
		$('#ConfirmacionFutbolista').modal('show');
	}

	// agregar la funcionalidad live search para input#agregar
	F_AREPA.WIDGETS.livesearch('value', '/query/futbolistas/%QUERY', '#agregar', rellenar_campos_modal)

	// agregar evento en el boton de confirmacion del modal
	$('#agregar-futbolista').on('click', function(){
		club_id = F_AREPA.UTILITIES.url();
		torneo_id = $( "#torneos option:selected" ).attr('value');
		var datos = {
			"futbolista" : futbolista_id,
			"club" : club_id,
			"torneo" : torneo_id,
			"numero": 0,
		}
		F_AREPA.UTILITIES.AJAX.post('/contrato/futbolista/store', datos);
		$('#agregar').attr('value', '');
		$('#ConfirmacionFutbolista').modal('hide');
	})
}

F_AREPA.CONFIG.coleccion.agregar(F_AREPA.LOGIC.futbolistas);
