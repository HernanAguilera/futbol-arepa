// create the root namespace and making sure we're not overwriting it
var F_AREPA = F_AREPA || {};

F_AREPA.createNS = function (namespace) {
    var nsparts = namespace.split(".");
    var parent = F_AREPA;
    // we want to be able to include or exclude the root namespace
    // So we strip it if it's in the namespace
    if (nsparts[0] === "F_AREPA") {
        nsparts = nsparts.slice(1);
    }

    // loop through the parts and create
    // a nested namespace if necessary
    for (var i = 0; i < nsparts.length; i++) {
        var partname = nsparts[i];
        // check if the current parent already has
        // the namespace declared, if not create it
        if (typeof parent[partname] === "undefined") {
            parent[partname] = {};
        }
        // get a reference to the deepest element
        // in the hierarchy so far
        parent = parent[partname];
    }
    // the parent is now completely constructed
    // with empty namespaces and can be used.
    return parent;
}


/*
 *  Se crea el namespace para config de la aplicacion
 */

F_AREPA.createNS("F_AREPA.CONFIG");

F_AREPA.CONFIG.funciones = function(){

	var funciones = [];

	this.agregar = function(funcion){
		funciones.push(funcion);
	};

	this.ejecutar = function(){
		for (var i = 0; i < funciones.length; i++) {
			funciones[i]()
		};
	};
}

F_AREPA.CONFIG.coleccion = new F_AREPA.CONFIG.funciones();

/*
 *  Se crea el namespace para utilities
 */
F_AREPA.createNS("F_AREPA.UTILITIES");

F_AREPA.UTILITIES.url = function(n){
    n || ( n = null );
    var url = window.location.href;
    url = url.split('/');
    if(n===null) 
        return url[url.length-1];
    return (n <= url.length-1 && n > 0) ? url[n] : null;
}

F_AREPA.UTILITIES.selectSearch = function(select, trigger){
    $(select +' option').on('click', trigger);
}

/*
 *  Dentro de este se crea el namespace para ajax
 */

F_AREPA.createNS("F_AREPA.UTILITIES.AJAX");

F_AREPA.UTILITIES.AJAX.get = function(direccion, callback){
    callback || ( callback = null );
    ajax = $.ajax({
        url: direccion,
    })

    if(callback === null){
        ajax.done(function(msj){
            console.log(msj);
        })
        return;
    }

    ajax.done(callback);
}


F_AREPA.UTILITIES.AJAX.post = function(direccion, datos, callback){
    callback || ( callback = null );
    ajax = $.ajax({
        type: 'POST',
        data: datos,
        url: direccion,
    })

    if(callback === null){
        ajax.done(function(msj){
            console.log(msj);
        })
        return;
    }

    ajax.done(callback);
    
}

F_AREPA.createNS("F_AREPA.UTILITIES.AJAX.MUSTACHE");

F_AREPA.UTILITIES.AJAX.MUSTACHE.get = function(URLdata, URLtemplate, selector, raiz, callback){
    callback || ( callback = null );
    $.ajax({
        url: URLtemplate,
    }).done(function(template){
        $.ajax({
            url: URLdata,
        })
        .done(function(json){
            console.log('json',json);
            if(json[raiz].length>0){
                rendered = Mustache.render(template, json);
                $(selector).html(rendered);
            }else{
                $(selector).html('');
            }
            if(callback!=null) callback();
        })
    })
}
